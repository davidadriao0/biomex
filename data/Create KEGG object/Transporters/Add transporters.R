# Libraries
library(data.table)
library(clusterProfiler)

# Read human file
humanTransporters <- fread("Human transporters.csv")
humanTransporters <- humanTransporters[[1]]

# Read mouse file
mouseTransporters <- fread("Mouse transporters.csv")
mouseTransporters <- mouseTransporters[[1]]

# Read the KEGG rda object (which needs to be updated)
load("../KEGG.rda")

# Conver the human to SYMBOL
transportersHumanStyle <- bitr(humanTransporters, fromType = "REFSEQ", toType = "SYMBOL", OrgDb = "org.Hs.eg.db")
transportersHumanStyle <- transportersHumanStyle$SYMBOL

# Conver the mouse to SYMBOL
transportersMouseStyle <- bitr(mouseTransporters, fromType = "REFSEQ", toType = "SYMBOL", OrgDb = "org.Mm.eg.db")
transportersMouseStyle <- transportersMouseStyle$SYMBOL

# The zebrafish style is like the human but all lower
transportersZebrafishStyle <- tolower(transportersHumanStyle)

# Convert to ENTREZID for all species
hs <- bitr(transportersHumanStyle, fromType = "SYMBOL", toType = "ENTREZID", OrgDb = "org.Hs.eg.db")
mm <- bitr(transportersMouseStyle, fromType = "SYMBOL", toType = "ENTREZID", OrgDb = "org.Mm.eg.db")
rn <- bitr(transportersMouseStyle, fromType = "SYMBOL", toType = "ENTREZID", OrgDb = "org.Rn.eg.db")
bt <- bitr(transportersHumanStyle, fromType = "SYMBOL", toType = "ENTREZID", OrgDb = "org.Bt.eg.db")
dr <- bitr(transportersZebrafishStyle, fromType = "SYMBOL", toType = "ENTREZID", OrgDb = "org.Dr.eg.db")
ss <- bitr(transportersHumanStyle, fromType = "SYMBOL", toType = "ENTREZID", OrgDb = "org.Ss.eg.db")
mf <- bitr(transportersHumanStyle, fromType = "SYMBOL", toType = "ENTREZID", OrgDb = "org.Mfascicularis.eg.db")

# Add the transporters to the KEGG object
KEGG$Human$Transporters <- hs[[2]]
KEGG$Mouse$Transporters <- mm[[2]]
KEGG$Rat$Transporters <- rn[[2]]
KEGG$Cow$Transporters <- bt[[2]]
KEGG$Zebrafish$Transporters <- dr[[2]]
KEGG$Pig$Transporters <- ss[[2]]
KEGG$`Crab-eating macaque`$Transporters <- mf[[2]]

# Check if the feature were added
KEGG$Human$Transporters
KEGG$Mouse$Transporters
KEGG$Rat$Transporters
KEGG$Cow$Transporters
KEGG$Zebrafish$Transporters
KEGG$Pig$Transporters
KEGG$`Crab-eating macaque`$Transporters

# Save the results to the KEGG object
save(KEGG, file = "KEGG.rda")
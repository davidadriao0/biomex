#!/bin/bash

# If you are not root, do not run the script
if [ "$EUID" -ne 0 ]
	then echo "Please run as root (sudo ./biomex)"
	exit
fi

# Parse parameters
usage()
{
	printf "Usage: biomex [options]\n\n"
	printf "Options:\n"
	printf "\t-p PORT, --port PORT\n"
	printf "\t\tPort to use to connect to the docker container.\n"
	printf "\t-d DISPLAY, --display DISPLAY\n"
	printf "\t\tSpecify the active display.\n"
	printf "\t--disable_access_control\n"
	printf "\t\tDisable the X11 access control.\n"
	printf "\t--use_internal_browser\n"
	printf "\t\tUse the Docker internal browser.\n"
	printf "\t-h, --help\n"
	printf "\t\tShow this help message and exit.\n"
}

# Default parameters
PORT=3838
USE_INTERNAL_BROWSER=false
ENABLE_FOR_ALL=false
USER_DISPLAY=$DISPLAY

# Get the parameters selected by the user
while [ "$1" != "" ]; do
	PARAM=`echo $1 | awk -F' ' '{print $1}'`
	VALUE=`echo $2 | awk -F' ' '{print $1}'`
	case $PARAM in
		-h | --help)
			usage
			exit
			;;
		-p | --port)
			if [[ $VALUE =~ ^-?[0-9]+$ ]]; then
				PORT=$VALUE
				shift 2
			else
				echo "The port must be an integer"
				echo ""
				usage
				exit 1
			fi
			;;
		-d | --DISPLAY)
			USER_DISPLAY=$VALUE
			shift 2
			;;
		--disable_access_control)
			ENABLE_FOR_ALL=true
			shift 1
			;;
		--use_internal_browser)
			USE_INTERNAL_BROWSER=true
			shift 1
			;;
		*)
			echo "Error: unknown parameter \"$PARAM\""
			exit 1
			;;
	esac
done

# Finilize parameters
PORT_PARAMETER="--port 3838"
BROWSER_PARAMETER=""

if [ $PORT != 3838 ]; then PORT_PARAMETER="--port $PORT"; fi
if [ $USE_INTERNAL_BROWSER == true ]; then BROWSER_PARAMETER="--browser"; fi

# Create the biomex directory in $HOME
mkdir -p $HOME/.biomex

# Give X11 access to Docker
if [ $ENABLE_FOR_ALL != true ]; then
	xhost +si:localuser:root &>/dev/null
else
	xhost + &>/dev/null
fi

# If the internal browser is not used, set the X11 permission a print some text
if [ $USE_INTERNAL_BROWSER == false ]; then
	# Write text to console
	clear
	printf "The BIOMEX session is running."
	printf "\nYou can open BIOMEX by copying and pasting this address in your browser: http://0.0.0.0:$PORT"
	printf "\nClosing the browser window will terminate the current BIOMEX session."
	printf "\n\nYou can see the application log in $HOME/.biomex/biomex.log.\n"
fi
		
# Run the container and redirect output to the biomex.log file
docker run \
	--publish $PORT:80 \
	--env DISPLAY=$USER_DISPLAY \
	--env BIOMEX_DOCKER_HOME=$HOME \
	--env PORT_PARAMETER="$PORT_PARAMETER" \
	--env BROWSER_PARAMETER="$BROWSER_PARAMETER" \
	--env XAUTHORITY=$XAUTHORITY \
	--expose $PORT \
	--volume /home:/home \
	--volume $HOME:/MyHome \
	--volume /tmp/.X11-unix:/tmp/.X11-unix \
	--volume $XAUTHORITY:$XAUTHORITY \
	--cap-add SYS_ADMIN \
	--device /dev/fuse \
	--security-opt apparmor:unconfined \
	biomex 2>&1 | sudo tee $HOME/.biomex/biomex.log >/dev/null

# Remove X11 access to Docker
if [ $ENABLE_FOR_ALL != true ]; then
	xhost -si:localuser:root &>/dev/null
else
	xhost - &>/dev/null
fi

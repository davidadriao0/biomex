# Since the design of experiment settings are always the same, we set it here and reuse it multiple times later.
designOfExperimentObservationSettings <- list(
    observationVariable = "",
    observationGroup = "",
    observationExclude = FALSE,
    observationToExclude = ""
)

designOfExperimentFeatureSettings <- list(
    featureSelected = "All",
    featureSelectedCustomPath = NULL,
    hvMeanLowThreshold = 0.1,
    hvMeanHighThreshold = 3,
    hvDispersionThreshold = 0.5,
    hvNumberOfBins = 20,
    hvPerformOnAllObservations = FALSE
)

designOfExperimentSetSettings <- list(
    setSelected = "KEGG sets",
    setSelectedCustomPath = NULL,
    keggSelected = "Metabolism"
)

designOfExperimentFilteringSettings <- list(
  featureAverageExpressionThreshold = 0
)

designOfExperimentSettings <- Reduce(append, list(designOfExperimentObservationSettings, 
                                                  designOfExperimentFeatureSettings, 
                                                  designOfExperimentSetSettings, 
                                                  designOfExperimentFilteringSettings))

# Global variables
global <- reactiveValues(
  # General variables. ====
  workingDirectory = NULL, # Set the global working directory
  applicationDataDirectory = NULL,
  sessionID = NULL, # Unique ID for the www folder session
  mainDirectory = NULL, # The main output directory
  progressStyle = "notification",
  switchingExperiment = FALSE,
  loadStoredResults = FALSE,
  performedStartup = FALSE,
  isCaseStudy = NULL, # Not saved
  loadLogic = FALSE, # Not saved
  showBIOMEX = FALSE, # Not saved
  switchingBehaviour = "Skip", # Not saved
  previousExperiment = NULL, # Not saved
  recentExperimentsTable = NULL, # Not saved
  performActionsAfterProcessing = FALSE, # Not saved
  online = FALSE,
  reservedVariables = c("AutomatedClustering", "ManualClustering", "State", "Pseudotime"),
  cores = NULL, # Not saved, the true number of cores is calculated on startup 
  
  # Settings
  compress = FALSE,
  doNotUseWebGL = FALSE,
  disableSwitchExperimentModal = FALSE,
  plotLimit = 100,
  tableLimit = 100,
  
  # Prepare data (common) ====
  csvDataLocation = NULL,  # Not saved
  csvMetadataLocation = NULL,  # Not saved
  csvDataAnnotation = NULL, # Not saved
  combinedAnnotation = NULL,
  experiments = NULL,
  selectedExperiment = NULL,
  performedSelectDataCSV = FALSE,
  performedSelectMetadataCSV = FALSE,
  
  # Data matrices ====
  dataMatrixOriginal = NULL,
  dataMatrixNormalized = NULL,
  dataMatrixRegression = NULL,
  
  # Prepare data (processing) ====
  metadataMatrix = NULL,
  metadataMatrixOriginal = NULL,
  experimentInformation = NULL,
  mappingTable = NULL,
  processingInformation = NULL,
  performedProcessing = FALSE,
  
  # Data ====
  # These values are not saved
  csvDataLocationAdd = NULL,
  csvDataAnnotationAdd = NULL,
  csvMetadataLocationAdd = NULL,
  combinedAnnotationAdd = NULL,
  performedSelectDataCSVAdd = FALSE,
  performedSelectMetadataCSVAdd = FALSE,
  
  # Metadata ====
  addMetadataMatrix = NULL,
  addMetadataFilePath = NULL,
  modifyMetadataMatrix = NULL,
  additionalMetadataMatrix = NULL,
  defineColorsMetadataMatrix = NULL,
  
  # Data pretreatment ====
  pretreatmentSettings = list(
    # Common
    observationSubset = "All",
    observationSubsetChoice = "",
    normalizationType = "Standard",
    skip = FALSE,
    # Transcriptomics
    featureAverageExpressionThreshold = 0.01,
    featureExpressionThreshold = 1,
    observationPercentageThreshold = 10,
    mitochondrialPercentageThreshold = 10,
    batchType = "None",
    batchVariable = "",
    batchK = 20,
    batchManualOrdering = FALSE,
    batchOrdering = "",
    batchInputCosineTransformation = TRUE,
    batchOutputCosineTransformation = FALSE,
    batchFeaturesToUse = "All",
    batchHvMeanLowThreshold = 0.1,
    batchHvMeanHighThreshold = 3,
    batchHvDispersionThreshold = 0.5,
    batchHvNumberOfBins = 20,
    # Metabolomics and Proteomics
    noiseSelectedObservations = "All",
    missingValuePercentageThreshold = 20,
    coefficientVariationThreshold = 0,
    normalizationReferenceFeature = "None",
    regressionDegree = "None",
    regressionColumn = "",
    injectionOrderColumn = "",
    regressionSpan = 0.75,
    regressionReferenceObservations = NULL,
    regressionInjectionOrder = NULL,
    imputationMethod = "None"
  ),
  pretreatmentFinalSettings = NULL,
  pretreatmentInformation = NULL,
  performedPretreatment = FALSE,
  pretreatmentTrigger = NULL,
  
  # Feature engineering ====
  feDoeSettings = designOfExperimentSettings,
  feDoeFinalSettings = list(),
  feDoeResults = NULL,
  feDoePerformed = FALSE,
  feDoeTrigger = NULL,
  
  feSettings = list(
    method = "None",
    svaMinimumSetSize = 5,
    svaKernel = "Gaussian",
    selectedCustomPath = NULL
  ),
  feFinalSettings = list(),
  feResults = NULL,
  feStoredResults = NULL,
  feTrigger = NULL,
  
  # Dimensionality reduction ====
  drDoeSettings = designOfExperimentSettings,
  drDoeFinalSettings = list(),
  drDoeResults = NULL,
  drDoePerformed = FALSE,
  drDoeTrigger = NULL,
  
  drSettings = list(
    dataMatrixAsInput = "Normal",
    scaling = "Auto",
    method = "PCA",
    dimensions = 2,
    pcaAccurateMode = FALSE,
    tsnePcaDimensions = 15,
    tsnePerplexity = 60,
    tsneLearningRate = 200,
    tsneIterations = 1000,
    tsneRandomSeed = 1234,
    tsneCores = 1,
    umapPcaDimensions = 15,
    umapNeighbours = 15,
    umapMinimumDistance = 0.1,
    umapAlpha = 1,
    umapEpochs = 200,
    umapRandomSeed = 1234
  ),

  drFinalSettings = list(),
  drResults = NULL,
  drStoredResults = NULL,
  drPlot = NULL,
  
  # Clustering ====
  clusteringDoeSettings = designOfExperimentSettings,
  clusteringDoeFinalSettings = list(),
  clusteringDoeResults = NULL,
  clusteringDoePerformed = FALSE,
  clusteringDoeTrigger = NULL,
  
  clusteringSettings = list(
    clusteringInput = "Dimensionality reduction current result",
    clusteringDimension = 2,
    originalFeatures = "",
    engineeredFeatures = "",
    scaling = "Auto",
    pcaAccurateMode = FALSE,
    clusteringType = "Hierarchical clustering",
    numberOfClusters = 10,
    distance = "Euclidean",
    agglomeration = "Complete",
    useSquaredDistance = FALSE,
    seuratNeighbour = 10,
    seuratResolution = 1,
    randomSeed = 1234
  ),
  clusteringFinalSettings = list(),
  clusteringResults = NULL,
  clusteringPlot = NULL,
  
  # Heatmap ====
  heatmapDoeSettings = designOfExperimentSettings,
  heatmapDoeFinalSettings = list(),
  heatmapDoeResults = NULL,
  heatmapDoePerformed = FALSE,
  heatmapDoeTrigger = NULL,
  
  heatmapSettings = list(
    dataMatrixAsInput = "Normal grouped",
    forcePositive = FALSE,
    scaling = "Auto",
    type = "Correlation",
    setsSelected = "",
    featuresSelected = "",
    observationsSelected = "",
    clusteringLocation = "None",
    clusteringType = "Manual",
    distance = "Euclidean",
    agglomeration = "Complete",
    useSquaredDistance = FALSE,
    pvalueThreshold = 0.05,
    bootstrapNumber = 1000
  ),
  heatmapFinalSettings = list(),
  heatmapResults = NULL,
  heatmapStoredResults = NULL,
  heatmapPlot = NULL,
  
  # Graph ====
  graphDoeSettings = designOfExperimentSettings,
  graphDoeFinalSettings = list(),
  graphDoeResults = NULL,
  graphDoePerformed = FALSE,
  graphDoeTrigger = NULL,
  
  graphSettings = list(    
    observationVariable = "",
    observationGroup = ""
  ),
  graphFinalSettings = list(),
  graphResults = NULL,
  graphData = NULL,
  graphPlot = NULL,
    
  # Metadata graph ====
  metadataGraphDoeSettings = designOfExperimentSettings,
  metadataGraphDoeFinalSettings = list(),
  metadataGraphDoeResults = NULL,
  metadataGraphDoePerformed = FALSE,
  metadataGraphDoeTrigger = NULL,
  
  metadataGraphSettings = list(
    observationVariable = "",
    observationGroup = "",
    normalizationFactor = "Not selected"
  ),
  metadataGraphFinalSettings = list(),
  metadataGraphResults = NULL,
  metadataGraphPlot = NULL,
  metadataGraphPlotData = NULL,
  
  # Brute force ====
  bfSettings = list(
    analysis = "Dimensionality reduction",
    drColorCoding = "Original feature",
    drDotSize = 5,
    testRun = FALSE
  ),
  bfFinalSettings = list(),
  bfResults = NULL,
  bfAdditionalResults = NULL,
  bfStoredResults = NULL,
  bfShowSettings = designOfExperimentFeatureSettings,
  bfTable = NULL,
  
  # Differential analysis ====
  daDoeSettings = designOfExperimentSettings,
  daDoeFinalSettings = list(),
  daDoeResults = NULL,
  daDoePerformed = FALSE,
  daDoeTrigger = NULL,
  
  daSettings = list(
    dataMatrixAsInput = "Normal",
    observationVariable = "",
    referenceGroup = "",
    experimentalGroup = "",
    covariates = "",
    type = "Limma"
  ),
  daFinalSettings = list(),
  daResults = NULL,
  daStoredResults = NULL,
  daPlot = NULL,
  
  # Competitive set enrichment analysis ====
  cseaDoeSettings = designOfExperimentSettings,
  cseaDoeFinalSettings = list(),
  cseaDoeResults = NULL,
  cseaDoePerformed = FALSE,
  cseaDoeTrigger = NULL,
  
  cseaSettings = list(
    featuresToUse = "All",
    minimumSetSize = 5,
    permutations = 1000,
    randomSeed = 1234
  ),
  cseaFinalSettings = list(),
  cseaResults = NULL,
  cseaStoredResults = NULL,
  cseaPlot = NULL,
  cseaEnrichmentPlot = NULL,
  cseaSetContent = NULL,
  
  # Self contained set enrichment analysis ====
  sseaDoeSettings = designOfExperimentSettings,
  sseaDoeFinalSettings = list(),
  sseaDoeResults = NULL,
  sseaDoePerformed = FALSE,
  sseaDoeTrigger = NULL,
  
  sseaSettings = list(
    featuresToUse = "All",
    minimumSetSize = 5,
    rotations = 1000,
    randomSeed = 1234
  ),
  sseaFinalSettings = list(),
  sseaResults = NULL,
  sseaStoredResults = NULL,
  sseaSetContent = NULL,
  
  # Pathway mapping ====
  pmSettings = list(
    featuresToUse = "All",
    range = 1,
    bins = 10,
    keggSelected = "Metabolism"
  ),
  pmFinalSettings = list(),
  pmResults = NULL,
  pmStoredResults = NULL,
  
  # Survival analysis ====
  saDoeSettings = designOfExperimentSettings,
  saDoeFinalSettings = list(),
  saDoeResults = NULL,
  saDoePerformed = FALSE,
  saDoeTrigger = NULL,
  
  saSettings = list(
    eventVariable = "Not selected",
    eventTarget = "dead",
    timeVariable = "Not selected",
    quantiles = c(0.25, 0.75),
    excludeMedium = FALSE,
    divideTime = FALSE,
    stratificationVariable = "Not selected"
  ),
  saFinalSettings = list(),
  saResults = NULL,
  saPlot = NULL,
  
  # Marker set analysis ====
  msaDoeSettings = designOfExperimentSettings,
  msaDoeFinalSettings = list(),
  msaDoeResults = NULL,
  msaDoePerformed = FALSE,
  msaDoeTrigger = NULL,
  
  msaSettings = list(
    dataMatrixAsInput = "Normal",
    type = "Unique (upregulated)",
    metricUpper = 100,
    metricLower = -100,
    invertRank = FALSE,
    observationVariable = "",
    observationGroup = "",
    covariates = ""
  ),
  msaFinalSettings = list(),
  msaResults = NULL,
  msaStoredResults = NULL,
  msaTable = NULL,
  
  # Pseudotime analysis ====
  paDoeSettings = designOfExperimentSettings,
  paDoeFinalSettings = list(),
  paDoeResults = NULL,
  paDoePerformed = FALSE,
  paDoeTrigger = NULL,
  
  paSettings = list(
    algorithm = "Monocle",
    pcaDimensions = 3,
    k = 4,
    stretchFactor = 0,
    randomSeed = 1234
  ),
  paFinalSettings = list(),
  paResults = NULL,
  paStoredResults = NULL,
  paPlot = NULL,
  paExpressionPlot = NULL,
  
  # Cluster prediction ====
  cpDoeSettings = designOfExperimentSettings,
  cpDoeFinalSettings = list(),
  cpDoeResults = NULL,
  cpDoePerformed = FALSE,
  cpDoeTrigger = NULL,
  
  cpSettings = list(
    observationVariable = "",
    similarityThreshold = 0,
    featureIdentifier = "Original",
    dataToPredictPath = NULL,
    metadataToPredictPath = NULL
  ),
  cpFinalSettings = list(),
  cpResults = NULL,
  cpStoredResults = NULL,
  cpPlot = NULL,
  
  # Trajectory analysis ====
  taDoeSettings = designOfExperimentSettings,
  taDoeFinalSettings = list(),
  taDoeResults = NULL,
  taDoePerformed = FALSE,
  taDoeTrigger = NULL,
  
  taSettings = list(
    group = "",
    fittingType = "Ellipse",
    fittingLoops = 4,
    pseudotimeCalculationMethod = "Complex",
    invertPseudotime = FALSE,
    startingObservation = ""
  ),
  
  taFinalSettings = list(),
  taResults = NULL,
  taPlot = NULL,
  taExpressionPlot = NULL,
  triggerTrajectoryAnalysis = NULL,
  trajectoryData = NULL,
  trajectoryLines = NULL,
  trajectoryLinesThreshold = NULL,
  
  # Machine learning ====
  mlDoeSettings = designOfExperimentSettings,
  mlDoeFinalSettings = list(),
  mlDoeResults = NULL,
  mlDoePerformed = FALSE,
  mlDoeTrigger = NULL,
  
  mlSettings = list(
    dataMatrixAsInput = "Normal",
    trainingDataPercentage = 75,
    dataSplitRandomSeed = 1234,
    scaling = "Auto",
    featureSelectionType = "None",
    featureSelectionMinimum = 50,
    featureSelectionMaximum = 500,
    featureSelectionStep = 50,
    featureSelectionCrossValidationFolds = 10,
    featureSelectionCrossValidationRepeats = 10,
    model = "Random forest",
    trainingCrossValidationFolds = 10,
    trainingCrossValidationRepeats = 10,
    variableToPredict = NULL,
    featureIdentifier = "Original",
    dataToPredictPath = NULL,
    metadataToPredictPath = NULL
  ),
  mlFinalSettings = list(),
  mlResults = NULL,
  mlStoredResults = NULL,
  mlPlot = NULL,
  
  # Meta analysis ====
  metaAnalysisData = NULL,
  maSettings = list(
    featuresToUpper = FALSE,
    metricUpper = 100,
    metricLower = -100,
    pvalueUpper = 1,
    pvalueLower = 0,
    dataSelected = ""
  ),
  maFinalSettings = list(),
  maResults = NULL,
  maStoredResults = NULL,
  maPlot = NULL,
  maSelectionTable = NULL,
  
  # Cluster similarity ====
  clusterSimilarityData = NULL,
  csSettings = list(
    numberOfFeatures = 50,
    featuresToUpper = FALSE,
    dataSelected = "",
    groupToRemove = "",
    classToRemove = ""
  ),
  csFinalSettings = list(),
  csResults = NULL,
  csStoredResults = NULL,
  csPlot = NULL,
  csSelectionTable = NULL,
  
  # Modals ====
  exportPlotFolder = NULL,
  exportTableFolder = NULL,
  exportPictureFolder = NULL,
  exportTable = NULL,
  exportPlot = NULL,
  exportPicture = NULL
)

# Initialize the final settings variables ====
# TODO: reset also when the variables are reset within a BIOMEX execution
isolate(global$feDoeFinalSettings <- global$feDoeSettings)
isolate(global$drDoeFinalSettings <- global$drDoeSettings)
isolate(global$clusteringDoeFinalSettings <- global$clusteringDoeSettings)
isolate(global$heatmapDoeFinalSettings <- global$heatmapDoeSettings)
isolate(global$graphDoeFinalSettings <- global$graphDoeSettings)
isolate(global$metadataGraphDoeFinalSettings <- global$metadataGraphDoeSettings)
isolate(global$daDoeFinalSettings <- global$daDoeSettings)
isolate(global$cseaDoeFinalSettings <- global$cseaDoeSettings)
isolate(global$sseaDoeFinalSettings <- global$sseaDoeSettings)
isolate(global$msaDoeFinalSettings <- global$msaDoeSettings)
isolate(global$saDoeFinalSettings <- global$saDoeSettings)
isolate(global$paDoeFinalSettings <- global$paDoeSettings)
isolate(global$cpDoeFinalSettings <- global$cpDoeSettings)
isolate(global$taDoeFinalSettings <- global$taDoeSettings)
isolate(global$mlDoeFinalSettings <- global$mlDoeSettings)
isolate(global$pretreatmentFinalSettings <- global$pretreatmentSettings)
isolate(global$feFinalSettings <- global$feSettings)
isolate(global$drFinalSettings <- global$drSettings)
isolate(global$clusteringFinalSettings <- global$clusteringSettings)
isolate(global$heatmapFinalSettings <- global$heatmapSettings)
isolate(global$graphFinalSettings <- global$graphSettings)
isolate(global$metadataGraphFinalSettings <- global$metadataGraphSettings)
isolate(global$daFinalSettings <- global$daSettings)
isolate(global$cseaFinalSettings <- global$cseaSettings)
isolate(global$sseaFinalSettings <- global$sseaSettings)
isolate(global$pmFinalSettings <- global$pmSettings)
isolate(global$taFinalSettings <- global$taSettings)
isolate(global$paFinalSettings <- global$paSettings)
isolate(global$cpFinalSettings <- global$cpSettings)
isolate(global$mlFinalSettings <- global$mlSettings)
isolate(global$saFinalSettings <- global$saSettings)
isolate(global$msaFinalSettings <- global$msaSettings)
isolate(global$bfFinalSettings <- global$bfSettings)
isolate(global$maFinalSettings <- global$maSettings)
isolate(global$csFinalSettings <- global$csSettings)

# *********************** ====

# We save the original values so we can reset them when we need to do that.
globalOriginal <- isolate(reactiveValuesToList(global, all.names = TRUE))

# General variables ====
globalGeneralVariables <- c(
  "mainDirectory",
  "performedStartup",
  "metaAnalysisData",
  "clusterSimilarityData",
  "compress",
  "doNotUseWebGL",
  "disableSwitchExperimentModal",
  "plotLimit",
  "tableLimit",
  "online"
)

globalGeneralVariablesNotSaved <- c(
  "workingDirectory",
  "sessionID",
  "applicationDataDirectory",
  "switchingExperiment",
  "loadStoredResults",
  "isCaseStudy",
  "loadLogic",
  "showBIOMEX"
)

# Data matrices ====
globalDataMatrixOriginal <- c(
  "dataMatrixOriginal"
)

globalDataMatrixNormalized <- c(
  "dataMatrixNormalized"
)

globalDataMatrixRegressed <- c(
  "dataMatrixRegression"
)

# Prepare data variables (common) ====
globalPrepareDataGeneralVariables <- c(
  "combinedAnnotation",
  "experiments",
  "selectedExperiment",
  "performedSelectDataCSV",
  "performedSelectMetadataCSV"
)

globalPrepareDataGeneralNotSaved <- c(
  "csvDataLocation",
  "csvMetadataLocation",
  "csvDataAnnotation"
)

# Prepare data variables (processing) ====
globalPrepareDataProcessingVariables <- c(
  "metadataMatrix",
  "metadataMatrixOriginal",
  "mappingTable",
  "experimentInformation",
  "processingInformation",
  "performedProcessing"
)

# Data ====
globalDataNotSaved <- c(
  "csvDataLocationAdd",
  "csvDataAnnotationAdd",
  "combinedAnnotationAdd",
  "performedSelectDataCSVAdd",
  "performedSelectMetadataCSVAdd"
)

# Metadata ====
globalMetadataVariables <- c(
  "metadataColors"
)

globalMetadataNotSaved <- c(
  "addMetadataMatrix",
  "addMetadataFilePath",
  "modifyMetadataMatrix",
  "additionalMetadataMatrix",
  "defineColorsMetadataMatrix"
)

# Pretreatment ====
globalDataPretreatmentVariables <- c(
  "pretreatmentSettings",
  "pretreatmentFinalSettings",
  "pretreatmentInformation",
  "performedPretreatment"
)

globalDataPretreatmentNotSaved <- c(
  "pretreatmentTrigger"
)

# Feature engineering ====
globalFeatureEngineeringVariables <- c( 
  "feDoeSettings",
  "feDoeFinalSettings",
  "feDoeResults",
  "feDoePerformed",
  "feSettings",
  "feFinalSettings",
  "feResults",
  "feStoredResults"
)

globalFeatureEngineeringNotSaved <- c(
  "feDoeTrigger"
)

# Data exploration ====
# Dimensionality reduction
globalDimensionalityReductionVariables <- c(
  "drDoeSettings",
  "drDoeFinalSettings",
  "drDoeResults",
  "drDoePerformed",
  "drSettings",
  "drFinalSettings",
  "drResults",
  "drStoredResults"
)

globalDimensionalityReductionNotSaved <- c(
  "drPlot",
  "drDoeTrigger"
)

# Clustering
globalClusteringVariables <- c(
  "clusteringDoeSettings",
  "clusteringDoeFinalSettings",
  "clusteringDoeResults",
  "clusteringDoePerformed",
  "clusteringSettings",
  "clusteringFinalSettings",
  "clusteringResults"
)

globalClusteringNotSaved <- c(
  "clusteringPlot",
  "clusteringDoeTrigger"
)

# Heatmap
globalHeatmapVariables <- c(
  "heatmapDoeSettings",
  "heatmapDoeFinalSettings",
  "heatmapDoeResults",
  "heatmapDoePerformed",
  "heatmapSettings",
  "heatmapFinalSettings",
  "heatmapResults",
  "heatmapStoredResults"
)


globalHeatmapNotSaved <- c(
  "heatmapPlot",
  "heatmapDoeTrigger"
)

# Graph
globalGraphVariables <- c(
  "graphDoeSettings",
  "graphDoeFinalSettings",
  "graphDoeResults",
  "graphDoePerformed",
  "graphSettings",
  "graphFinalSettings",
  "graphResults"
)

globalGraphNotSaved <- c(
  "graphData",
  "graphPlot",
  "graphDoeTrigger"
)

# Metadata graph
globalMetadataGraphVariables <- c(
  "metadataGraphDoeSettings",
  "metadataGraphDoeFinalSettings",
  "metadataGraphDoeResults",
  "metadataGraphDoePerformed",
  "metadataGraphSettings",
  "metadataGraphFinalSettings",
  "metadataGraphResults"
)

globalMetadataGraphNotSaved <- c(
  "metadataGraphPlot",
  "metadataGraphPlotData",
  "metadataGraphDoeTrigger"
)

# Brute force
globalBruteForceVariables <- c(
  "bfSettings",
  "bfFinalSettings",
  "bfResults",
  "bfAdditionalResults",
  "bfStoredResults"
)

globalBruteForceNotSaved <- c(
  "bfFileTable"
)

# Differential analysis
globalDifferentialAnalysisVariables <- c(
  "daDoeSettings",
  "daDoeFinalSettings",
  "daDoeResults",
  "daDoePerformed",
  "daSettings",
  "daFinalSettings",
  "daResults",
  "daStoredResults"
)

globalDifferentialAnalysisNotSaved <- c(
  "daPlot",
  "daDoeTrigger"
)

# Competitive set enrichment analysis
globalCompetitiveSetEnrichmentAnalysisVariables <- c(
  "cseaDoeSettings",
  "cseaDoeFinalSettings",
  "cseaDoeResults",
  "cseaDoePerformed",
  "cseaSettings",
  "cseaFinalSettings",
  "cseaResults",
  "cseaStoredResults"
)

globalCompetitiveSetEnrichmentAnalysisNotSaved <- c(
  "cseaPlot",
  "cseaEnrichmentPlot",
  "cseaSetContent",
  "cseaDoeTrigger"
)

# Self contained set enrichment analysis
globalSelfContainedSetEnrichmentAnalysisVariables <- c(
  "sseaDoeSettings",
  "sseaDoeFinalSettings",
  "sseaDoeResults",
  "sseaDoePerformed",
  "sseaSettings",
  "sseaFinalSettings",
  "sseaResults",
  "sseaStoredResults"
)

globalSelfContainedSetEnrichmentAnalysisNotSaved <- c(
  "sseaSetContent",
  "sseaDoeTrigger"
)

# Pathway mapping
globalPathwayMappingVariables <- c(
  "pmSettings",
  "pmFinalSettings",
  "pmResults",
  "pmStoredResults"
)

globalPathwayMappingNotSaved <- c()


# Survival analysis
globalSurvivalAnalysisVariables <- c(
  "saDoeSettings",
  "saDoeFinalSettings",
  "saDoeResults",
  "saDoePerformed",
  "saSettings",
  "saFinalSettings",
  "saResults"
)

globalSurvivalAnalysisNotSaved <- c(
  "saSurvivalResults",
  "saPlot",
  "saDoeTrigger"
)

# Marker set analysis
globalMarkerSetAnalysisVariables <- c(
  "msaDoeSettings",
  "msaDoeFinalSettings",
  "msaDoeResults",
  "msaDoePerformed",
  "msaSettings",
  "msaFinalSettings",
  "msaResults",
  "msaStoredResults"
)

globalMarkerSetAnalysisNotSaved <- c(
  "msaTable",
  "msaDoeTrigger"
)

# Pseudotime analysis
globalPseudotimeAnalysisVariables <- c(
  "paDoeSettings",
  "paDoeFinalSettings",
  "paDoeResults",
  "paDoePerformed",
  "paSettings",
  "paFinalSettings",
  "paResults",
  "paStoredResults"
)

globalPseudotimeAnalysisNotSaved <- c(
  "paPlot",
  "paExpressionPlot",
  "paDoeTrigger"
)

# Cluster prediction
globalClusterPredictionVariables <- c(
  "cpDoeSettings",
  "cpDoeFinalSettings",
  "cpDoeResults",
  "cpDoePerformed",
  "cpSettings",
  "cpFinalSettings",
  "cpResults",
  "cpStoredResults"
)

globalClusterPredictionNotSaved <- c(
  "cpPlot",
  "cpDoeTrigger"
)

# Trajectory analysis
globalTrajectoryAnalysisVariables <- c(
  "taDoeSettings",
  "taDoeFinalSettings",
  "taDoeResults",
  "taDoePerformed",
  "taSettings",
  "taFinalSettings",
  "taResults",
  "trajectoryLines",
  "trajectoryLinesThreshold"
)

globalTrajectoryAnalysisNotSaved <- c(
  "taPlot",
  "taExpressionPlot",
  "triggerTrajectoryAnalysis",
  "trajectoryData",
  "taDoeTrigger"
)

# Machine learning ====
globalMachineLearningVariables <- c(
  "mlDoeSettings",
  "mlDoeFinalSettings",
  "mlDoeResults",
  "mlDoePerformed",
  "mlSettings",
  "mlFinalSettings",
  "mlResults",
  "mlStoredResults"
)

globalMachineLearningNotSaved <- c(
  "mlPlot",
  "mlDoeTrigger"
)

# Meta analysis ====
globalMetaAnalysisVariables <- c(
  "maSettings",
  "maFinalSettings",
  "maResults",
  "maStoredResults"
)

globalMetaAnalysisNotSaved <- c(
  "maPlot",
  "maSelectionTable"
)

# Cluster similarity ====
globalClusterSimilarityVariables <- c(
  "csSettings",
  "csFinalSettings",
  "csResults",
  "csStoredResults"
)

globalClusterSimilarityNotSaved <- c(
  "csPlot",
  "csSelectionTable"
)

# Modals ====
globalModalsVariablesNotSaved <- c(
  "exportPlotFolder",
  "exportTableFolder",
  "exportPictureFolder",
  "exportTable",
  "exportPlot",
  "exportPicture"
)

# General variables ====
globalCommonVariables <- c(
  globalGeneralVariables,
  globalPrepareDataGeneralVariables
)

# After design of experiment variables ====
globalAfterPretreatmentVariables <- c(
  globalFeatureEngineeringVariables,
  globalFeatureEngineeringNotSaved,
  globalDimensionalityReductionVariables,
  globalDimensionalityReductionNotSaved,
  globalClusteringVariables,
  globalClusteringNotSaved,
  globalHeatmapVariables,
  globalHeatmapNotSaved,
  globalGraphVariables,
  globalGraphNotSaved,
  globalMetadataGraphVariables,
  globalMetadataGraphNotSaved,
  globalDifferentialAnalysisVariables,
  globalDifferentialAnalysisNotSaved,
  globalCompetitiveSetEnrichmentAnalysisVariables,
  globalCompetitiveSetEnrichmentAnalysisNotSaved,
  globalSelfContainedSetEnrichmentAnalysisVariables,
  globalSelfContainedSetEnrichmentAnalysisNotSaved,
  globalPathwayMappingVariables,
  globalPathwayMappingNotSaved,
  globalPseudotimeAnalysisVariables,
  globalPseudotimeAnalysisNotSaved,
  globalClusterPredictionVariables,
  globalClusterPredictionNotSaved,
  globalTrajectoryAnalysisVariables,
  globalTrajectoryAnalysisNotSaved,
  globalSurvivalAnalysisVariables,
  globalSurvivalAnalysisNotSaved,
  globalMarkerSetAnalysisVariables,
  globalMarkerSetAnalysisNotSaved,
  globalBruteForceVariables,
  globalBruteForceNotSaved,
  globalMachineLearningVariables,
  globalMachineLearningNotSaved
)

# After processing variables ====
globalAfterProcessingVariables <- c(
  globalDataPretreatmentVariables,
  globalDataPretreatmentNotSaved,
  globalDataNotSaved,
  globalMetadataVariables,
  globalMetadataNotSaved,
  globalAfterPretreatmentVariables
)

# All except essential variables ====
globalAllExceptEssentialVariables <- setdiff(names(globalOriginal), c("workingDirectory", "cores", "recentExperimentsTable", "applicationDataDirectory", "online"))

# All except general variables ====
globalAllExceptCommonVariables <- c(
  globalPrepareDataProcessingVariables,
  globalAfterProcessingVariables
)

# All settings ==== 
globalAllSettings <- c(
  "pretreatmentSettings",
  "feDoeSettings",
  "feDoeFinalSettings",
  "drDoeSettings",
  "drDoeFinalSettings",
  "clusteringDoeSettings",
  "clusteringDoeFinalSettings",
  "heatmapDoeSettings",
  "heatmapDoeFinalSettings",
  "graphDoeSettings",
  "graphDoeFinalSettings",
  "metadataGraphDoeSettings",
  "metadataGraphDoeFinalSettings",
  "daDoeSettings",
  "daDoeFinalSettings",
  "cseaDoeSettings",
  "cseaDoeFinalSettings",
  "sseaDoeSettings",
  "sseaDoeFinalSettings",
  "msaDoeSettings",
  "msaDoeFinalSettings",
  "saDoeSettings",
  "saDoeFinalSettings",
  "paDoeSettings",
  "paDoeFinalSettings",
  "cpDoeSettings",
  "cpDoeFinalSettings",
  "taDoeSettings",
  "taDoeFinalSettings",
  "mlDoeSettings",
  "mlDoeFinalSettings",
  "feSettings",
  "drSettings",
  "clusteringSettings",
  "heatmapSettings",
  "graphSettings",
  "metadataGraphSettings",
  "daSettings",
  "cseaSettings",
  "sseaSettings",
  "pmSettings",
  "taSettings",
  "paSettings",
  "cpSettings",
  "mlSettings",
  "saSettings",
  "msaSettings",
  "maSettings",
  "csSettings",
  "pretreatmentFinalSettings",
  "feFinalSettings",
  "drFinalSettings",
  "clusteringFinalSettings",
  "heatmapFinalSettings",
  "graphFinalSettings",
  "metadataGraphFinalSettings",
  "daFinalSettings",
  "cseaFinalSettings",
  "sseaFinalSettings",
  "pmFinalSettings",
  "taFinalSettings",
  "paFinalSettings",
  "cpFinalSettings",
  "mlFinalSettings",
  "saFinalSettings",
  "msaFinalSettings",
  "maFinalSettings",
  "csFinalSettings"
)
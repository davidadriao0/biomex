#' Calculate the overlap between all the combinations of the list.
#'
#' @param sets The named list to calculate the overlaps on.
#'
#' @return The overlap table and the overlap list.
biomex.calculateListOverlap <- function(sets)
{
  # Get the set names
  groups <- names(sets)
  
  # Calculate all the possible combinations
  combinations <- list()
  for (i in 1:length(groups))
  {
    comb <- combn(1:length(groups), i)
    for (j in 1:ncol(comb)) combinations <- append(combinations, list(as.numeric(comb[, j])))
  }
  
  # Calculate the intersections of all the combinations
  intersections <- list() 
  for (combination in combinations)
  {
    subset <- sets[combination]
    intersection <- Reduce(intersect, subset)
    if (length(intersection) == 0) intersection <- "No overlap"
    
    intersectionName <- paste(names(subset), collapse = " - ")
    intersections[[intersectionName]] <- intersection
  }
  
  # Fill out the list with empty elements so we can create a data.table later
  overlapList <- intersections
  maxElements <- max(sapply(intersections, length))
  for (i in 1:length(intersections))
  {
    n <- length(intersections[[i]])
    if (n < maxElements) intersections[[i]] <- c(intersections[[i]], rep("", maxElements - n))
  }
  
  # Create the overlap data table
  overlapTable <- as.data.table(intersections)
  
  # Create the result list
  resultList <- list(overlapTable = overlapTable, overlapList = overlapList)
  
  return (resultList)
}
#' Decompress a data.table to a sparse matrix.
#'
#' @param dataMatrix The compressed data matrix dgCMatrix.
#'
#' @return The decompressed data matrix (data.table).
biomex.decompressMatrix <- function(dataMatrix)
{
  # If it's already decompressed, do not decompress
  if ("data.table" %in% class(dataMatrix) || "data.frame" %in% class(dataMatrix)) return (dataMatrix)
  
  # Get variables
  features <- rownames(dataMatrix)
  observations <- colnames(dataMatrix)
  
  # Create data.table
  dataMatrix <- as.data.table(as.matrix(dataMatrix))
  dataMatrix[, Feature := features]
  setcolorder(dataMatrix, c("Feature", observations))
  
  # Clean memory
  biomex.callGC()
  
  return (dataMatrix)
}
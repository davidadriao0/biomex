#' Delete rows from a data table. This is done by "semi" reference, in a way that uses less memory than the normal R way.
#' The data table passed to the function WILL BE MODIFIED BY REFERENCE (it will be deleted actually) for data tables that have more than 1000 rows. 
#' Be careful in how you use this function.
#' 
#' @param dataTable The data table from which remove the rows.
#' @param deleteIndex The index (integer) of the rows to remove from the data table.
#' 
#' @return The subset data table.
biomex.deleteRows <- function(dataTable, deleteIndex)
{
  # If not index is specified, return the table as it is
  if (length(deleteIndex) == 0) return (dataTable)
  
  # Select row indexes to keep
  keepIndex <- setdiff(dataTable[, .I], deleteIndex) 
  cols <- copy(colnames(dataTable)) # Need to use copy since the column names are saved by reference
  
  # Only apply the methods if the data table has more than 1000 rows
  if (length(cols) > 1000)
  {
    # Divide the data in ranges proportional to the number of rows
    n <- 100
    lengthOut <- as.integer(ncol(dataTable) / n)
    ranges <- as.integer(seq(from = 1, to = ncol(dataTable), length.out = lengthOut))
   
    # Initialize the subset data table
    dataTableSubset <- data.table(dataTable[[1]][keepIndex]) 
    setnames(dataTableSubset, cols[1])
    
    # Delete (by reference) the first column of the original data table
    dataTable[, (cols[1]) := NULL]
    
    # Loop over the ranges defined previously
    for (i in 1:(length(ranges) - 1)) 
    {
      # Get the ranges and the column names of the corresponding range
      start <- ranges[i] + 1
      end <- ranges[i + 1]
      colsSubset <- cols[start:end]
     
      # Add the data to the subset data table by reference
      dataTableSubset[, (colsSubset) := dataTable[keepIndex, colsSubset, with = FALSE]]
      
      # Delete the columns by reference (from the original data) that were used on the subset data table
      dataTable[, (colsSubset) := NULL]
    }
    
    # Return the subset data table
    return(dataTableSubset)
  }
  else
  {
    # Return the subset data table with the normal method (for smaller tables)
    return (dataTable[-deleteIndex])
  }
}
#' Group a data matrix by the target variable.
#' 
#' @param dataMatrix The data matrix data.table.
#' @param target The variable to group on.
#' @param groupingFunction Function to use for grouping (eg: mean, sd, etc.).
#'
#' @return The grouped data matrix data.table.
biomex.fastGrouping <- function(dataMatrix, target, groupingFunction)
{
  # Melt the data.table so all values are in one column called "value".
  dataMatrix <- melt.data.table(dataMatrix, id.vars = target)
  functionToUse <<- groupingFunction # HACK: if this is not done, then dcast.data.table doesn't recognize the function
  # Cast the data.table back into the original shape, and take the mean.
  dataMatrix <- dcast.data.table(dataMatrix, get(eval(target)) ~ variable, value.var = "value",  fun.aggregate = functionToUse)
  colnames(dataMatrix)[1] <- target
  
  return (dataMatrix)
}
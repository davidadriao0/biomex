#' Noise filtering for Metabolomics data. It includes NA filtering and coefficient of variation filtering.
#' The data used as input is MODIFIED BY REFERENCE. Be careful of the data you pass to this function.
#' 
#' @param dataMatrix The data matrix data.table.
#' @param selectedObservations The observations to be used in the filtering process (usually the QC observations).
#' @param missingValuePercentageThreshold The percentage of missing values allowed.
#' @param coefficientVariationThreshold The coefficient of variation threshold.
#' 
#' @return A list with the filtered data matrix, the number of features filtered by NAs and the number of features filtered by coefficient of variation.
#' A list with an error message if it fails.
biomex.noiseFiltering <- function(dataMatrix, selectedObservations, missingValuePercentageThreshold, coefficientVariationThreshold)
{
  # If no observations or few observations are selected, return the data matrix as it is
  if (is.null(selectedObservations) || (length(selectedObservations) < 3 && selectedObservations != "All"))
    return (list(errorMessage = "You need at least 3 observations to perform the noise filtering."))
  
  if (any(selectedObservations == "All")) selectedObservations <- biomex.getObservationNames(dataMatrix = dataMatrix)
  
  # Get the selected observations to perform the global filtering on
  qcDataMatrix <- dataMatrix[, ..selectedObservations]
  
  # 1. Filter out features based on missing value percentage threshold
  # 2. Filter out features based on the coefficient of variance threshold
  
  # Step 1
  afterNAFiltering <- 0
  if (missingValuePercentageThreshold < 100)
  {
    # 0s are the missing values
    percentage <- (rowSums(qcDataMatrix == 0) * 100) / ncol(qcDataMatrix)
   
    index <- which(percentage > missingValuePercentageThreshold)
    afterNAFiltering <- length(index)
    
    if (length(index) > 0)
    {
      qcDataMatrix <- biomex.deleteRows(dataTable = qcDataMatrix, deleteIndex = index)
      dataMatrix <- biomex.deleteRows(dataTable = dataMatrix, deleteIndex = index)
    }
    
    # Check if the data matrix still contains some features
    if (nrow(dataMatrix) == 0) return (list(errorMessage = "The missing values threshold is too strict."))
  }
  
  # Step 2   
  afterCVFiltering <- 0
  if (coefficientVariationThreshold > 0)
  {
    mean <- rowMeans(qcDataMatrix)
    sd <- apply(qcDataMatrix, 1, sd)
    temp <- data.table(Mean = mean, SD = sd)
    
    CV <- (temp$SD / temp$Mean) * 100
    index <- which(CV < coefficientVariationThreshold)
    afterCVFiltering <- length(index)
    
    if (length(index) > 0)
    {
      qcDataMatrix <- biomex.deleteRows(dataTable = qcDataMatrix, deleteIndex = index)
      dataMatrix <- biomex.deleteRows(dataTable = dataMatrix, deleteIndex = index)
    }
    
    # Check if the data matrix still contains some features
    if (nrow(dataMatrix) == 0) return (list(errorMessage = "The coefficient of variation threshold is too strict."))
  }
  
  result <- list(dataMatrix = dataMatrix, afterNAFiltering = afterNAFiltering, afterCVFiltering = afterCVFiltering)
  
  return (result)
}
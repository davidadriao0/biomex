#' Obtain the correct data matrix in the Data tab.
#'
#' @param dataMatrixOriginal The original data matrix data.table.
#' @param dataMatrixNormalized The normalized data matrix data.table.
#' @param dataMatrixEngineered The engineered data matrix data.table.
#' @param dataMatrixNormalizedGrouped The grouped normalized data matrix data.table.
#' @param dataMatrixEngineeredGrouped The grouped engineered data matrix data.table.
#' @param mappingTable The mapping table.
#' @param dataSelected The type data matrix that is selected by the user.
#' @param featureType The type of feature to export (Original, ID or Name).
#' @param preview Whether you want to get the preview data or the full data.
#'
#' @return A list that contains: the correct data matrix
biomex.getSelectedMatrix <- function(dataMatrixOriginal, dataMatrixNormalized, dataMatrixNormalizedGrouped, dataMatrixEngineered, dataMatrixEngineeredGrouped,
                                     mappingTable, featuresSelected, observationsSelected, dataSelected, featureType, preview)
{
  # if (preview == TRUE)
  # {
  #   observationsSelected <- observationsSelected[1:min(length(observationsSelected), 10)]
  #   featuresSelected <- featuresSelected[1:min(length(featuresSelected), 10)]
  # }
  
  if (dataSelected == "After processing") 
  {
    dataMatrix <- dataMatrixOriginal
  }
  
  if (dataSelected == "After processing (pretreatment subset)") 
  {
    rows <- biomex.getFeatureNames(dataMatrix = dataMatrixNormalized)
    columns <- biomex.getObservationNames(dataMatrix = dataMatrixNormalized)
    if (is.null(rows) || is.null(columns)) dataMatrix <- NULL
    else dataMatrix <- biomex.getDataMatrixSubset(dataMatrixOriginal, rows = rows, columns = columns)
  }
    
  if (dataSelected == "After processing (subset)") 
  {
    if (is.null(featuresSelected) || is.null(observationsSelected)) dataMatrix <- NULL
    else dataMatrix <- biomex.getDataMatrixSubset(dataMatrixOriginal, rows = featuresSelected, columns = observationsSelected)
  }
    
  if (dataSelected == "After processing (subset, all features)") 
  {
    rows <- biomex.getFeatureNames(dataMatrix = dataMatrixNormalized)
    if (is.null(featuresSelected) || is.null(observationsSelected)) dataMatrix <- NULL
    else if (is.null(rows)) dataMatrix <- NULL
    else dataMatrix <- biomex.getDataMatrixSubset(dataMatrixOriginal, rows = rows, columns = observationsSelected)
  }
  
  if (dataSelected == "After pretreatment") 
  {
    dataMatrix <- dataMatrixNormalized
  }
  
  if (dataSelected == "After pretreatment (subset)") 
  {
    if (is.null(featuresSelected) || is.null(observationsSelected)) dataMatrix <- NULL
    else dataMatrix <- biomex.getDataMatrixSubset(dataMatrixNormalized, rows = featuresSelected, columns = observationsSelected)
  }
  
  if (dataSelected == "After pretreatment (subset, all features)") 
  {
    if (is.null(featuresSelected) || is.null(observationsSelected)) dataMatrix <- NULL
    else dataMatrix <- biomex.getDataMatrixSubset(dataMatrixNormalized, rows = NULL, columns = observationsSelected)
  }
  
  if (dataSelected == "After pretreatment grouped (subset)") 
  {
    if (is.null(featuresSelected) || is.null(observationsSelected) || is.null(dataMatrixNormalizedGrouped)) dataMatrix <- NULL
    else dataMatrix <- biomex.getDataMatrixSubset(dataMatrixNormalizedGrouped, rows = featuresSelected)
  }
  
  if (dataSelected == "After pretreatment grouped (subset, all features)") 
  {
    dataMatrix <- dataMatrixNormalizedGrouped
  }
  
  if (dataSelected == "Engineered") 
  {
    dataMatrix <- dataMatrixEngineered
  }
  
  if (dataSelected == "Engineered (subset)") 
  {
    if (is.null(featuresSelected) || is.null(observationsSelected)) dataMatrix <- NULL
    else dataMatrix <- biomex.getDataMatrixSubset(dataMatrixEngineered, columns = observationsSelected)
  }
  
  if (dataSelected == "Engineered grouped (subset)") 
  {
    dataMatrix <- dataMatrixEngineeredGrouped
  }
  
  if (dataSelected == "Mapping")
  {
    dataMatrix <- mappingTable
  }
   
  if (dataSelected == "Mapping (pretreatment subset)")
  {
    rows <- biomex.getFeatureNames(dataMatrix = dataMatrixNormalized)
    if (is.null(rows)) dataMatrix <- NULL
    else dataMatrix <- biomex.getDataMatrixSubset(mappingTable, rows = rows)
  }
  
  if (dataSelected == "Mapping (subset)")
  {
    if (is.null(featuresSelected)) dataMatrix <- NULL
    else dataMatrix <- biomex.getDataMatrixSubset(mappingTable, rows = featuresSelected)
  }
    
  if (dataSelected == "Features (subset)")
  {
    if (is.null(featuresSelected)) dataMatrix <- NULL
    else dataMatrix <- data.table(Feature = featuresSelected)
  }
    
  if (dataSelected == "Observations (subset)")
  {
    if (is.null(observationsSelected)) dataMatrix <- NULL
    else dataMatrix <- data.table(Observation = observationsSelected)
  }

  # Subset the data matrix if the preview parameter is selected
  if (preview == TRUE)
  {
    if (!is.null(dataMatrix)) dataMatrix <- dataMatrix[1:min(nrow(dataMatrix), 5), 1:min(ncol(dataMatrix), 5)]
  }
  
  # Decompress the matrix if it was the original matrix to be selected
  if (dataSelected %in% c("After processing", "After processing (pretreatment subset)", "After processing (subset)", "After processing (subset, all features)")) 
  {
    if (!is.null(dataMatrix)) dataMatrix <- biomex.decompressMatrix(dataMatrix = dataMatrix)
  }
  
  # Map the features to the selected ID
  if (dataSelected %in% c("After processing", "After processing (pretreatment subset)", "After processing (subset)", "After processing (subset, all features)", 
                          "After pretreatment", "After pretreatment (subset)", "After pretreatment (subset, all features)", 
                          "After pretreatment grouped (subset)", "After pretreatment grouped (subset, all features)",
                          "Features (subset)")) 
  {
    if (!is.null(dataMatrix)) dataMatrix$Feature <- biomex.mapFeatures(dataMatrix$Feature, from = "Original", to = featureType, mappingTable = mappingTable)
  }
  
  resultList <- list(dataMatrix = dataMatrix)
  
  return (resultList)
}
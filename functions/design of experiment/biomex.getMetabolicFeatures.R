#' Obtain the metabolic genes based on the organism
#' 
#' @param organism The organism (Human, Mouse, etc).
#' 
#' @return A vector of metabolic features.
biomex.getMetabolicFeatures <- function(organism)
{
  metabolicGenes <- KEGG[[organism]]$MetabolicGenes
  
  return(metabolicGenes)
}
#' Validates the design of experiment selections. If something is not consistent, an error message returned.
#'
#' @param settings The design of experiment settings list.
#' @param allowObservations Whether the observation selection is enabled or not.
#' @param allowFeatures Whether the feature selection is enabled or not.
#' @param allowSets Whether the set selection is enabled or not.
#' @param reservedVariables The vector of reserver variables that cannot be used by the design of experiment.
#' 
#' @return NULL if everything is consistent. An error string if there are some inconsinstencies.
biomex.validateDesignOfExperiment <- function(settings, allowObservations, allowFeatures, allowSets, reservedVariables)
{
  # Get values
  observationVariable <- settings$observationVariable
  observationGroup <- settings$observationGroup
  featureSelected <- settings$featureSelected
  featureSelectedCustomPath <- settings$featureSelectedCustomPath
  setSelected <- settings$setSelected
  setSelectedCustomPath <- settings$setSelectedCustomPath
  keggSelected <- settings$keggSelected
  featureEngineeringMethod <- settings$featureEngineeringMethod
  featureEngineeringSelectedCustomPath <- settings$featureEngineeringSelectedCustomPath
  
  message <- c()
  
  if (allowObservations == TRUE)
  {
    if (observationVariable == "") message <- c(message, "Select a variable.")
    
    if (observationVariable %in% reservedVariables) message <- c(message, "This variable is reserved and cannot be used.")
    
    if (is.null(observationGroup) || observationGroup == "") message <- c(message, "Select some groups.")
  }
  
  if (allowFeatures == TRUE)
  {
    if (featureSelected == "Custom")
    {
      if (is.null(featureSelectedCustomPath)) message <- c(message, "Select the custom feature file.")
      else if (!file.exists(featureSelectedCustomPath)) message <- c(message, "The custom feature file does not exist.")
    }
  }
  
  if (allowSets == TRUE)
  {
    if (setSelected == "Custom")
    {
      if (is.null(setSelectedCustomPath)) message <- c(message, "Select the custom set file.")
      else if (!file.exists(setSelectedCustomPath)) message <- c(message, "The custom set file does not exist.")
    }
    
    if (setSelected == "KEGG sets" && "" %in% keggSelected) message <- c(message, "Select a KEGG set.")
  }
  
  # if (featureEngineeringMethod == "Custom")
  # {
  #   if (is.null(featureEngineeringSelectedCustomPath)) message <- c(message, "Select the custom feature engineered file.")
  #   else if (!file.exists(featureEngineeringSelectedCustomPath)) message <- c(message, "The custom feature engineered file does not exist.")
  # }
    
  # Format the strings together
  if (!is.null(message)) message <- paste(message, collapse = '\n')
  
  return (message)
}
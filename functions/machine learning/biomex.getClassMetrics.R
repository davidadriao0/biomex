#' Obtain the class metrics table in a "showable" way by shiny.
#' 
#' @param confusionMatrix The confusion matrix object as obtained with caret.
#' 
#' @return The converted metrics table
biomex.getClassMetrics <- function(confusionMatrix)
{
  classMetrics <- confusionMatrix$byClass
  if (is.null(nrow(classMetrics))) classes <- names(classMetrics)
  else classes <- rownames(classMetrics)
  classes <- trimws(gsub("Class:", "", classes))
  classMetrics <- cbind(Class = classes, as.data.table(classMetrics))
  
  return (classMetrics)
}
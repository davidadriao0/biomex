biomex.menuInterfaceHandling <- function(session, performedStartup, performedProcessing, performedPretreatment, experimentInformation, metaAnalysisData, clusterSimilarityData)
{
  # Startup ====
  # If the startup is performed, then we can enable the saving button
  if (performedStartup == TRUE)
  {
    shinyjs::show("saveExperiment")
    shinyjs::show("reloadExperiment")
  }
  else
  {
    shinyjs::hide("saveExperiment")
    shinyjs::hide("reloadExperiment")
  }
  
  # Prepare experiments ===
  # I only show this tab if no experiment was processed yet
  if (performedStartup == TRUE && performedProcessing == FALSE)
  {
    biomex.toggleMenuElement(name = "prepareExperimentsTab", hide = FALSE)
  }
  else
  {
    biomex.toggleMenuElement(name = "prepareExperimentsTab", hide = TRUE)
  }
  
  # After processing ====
  # We can enable these tabs after the processing of at least one experiment has been performed
  if (performedProcessing == TRUE)
  {
    biomex.toggleMenuElement(name = "manageExperimentsTab", hide = FALSE)
    biomex.toggleMenuElement(name = "dataTab", hide = FALSE)
    biomex.toggleMenuElement(name = "metadataTab", hide = FALSE)
    biomex.toggleMenuElement(name = "dataPretreatmentTab", hide = FALSE)
    biomex.toggleMenuElement(name = "selectExperimentTab", hide = FALSE)
  }
  else
  {
    biomex.toggleMenuElement(name = "manageExperimentsTab", hide = TRUE)
    biomex.toggleMenuElement(name = "dataTab", hide = TRUE)
    biomex.toggleMenuElement(name = "metadataTab", hide = TRUE)
    biomex.toggleMenuElement(name = "dataPretreatmentTab", hide = TRUE)
    biomex.toggleMenuElement(name = "selectExperimentTab", hide = TRUE)
  }
  
  
  # After pretreatment ====
  # After the pretreatment has been performed, we can show the analyses
  if (performedPretreatment == TRUE)
  {
    biomex.toggleMenuElement(name = "featureEngineeringTab", hide = FALSE)
    biomex.toggleMenuElement(name = "quantificationTab", hide = FALSE)
    biomex.toggleMenuElement(name = "unsupervisedAnalysesTab", hide = FALSE)
    biomex.toggleMenuElement(name = "supervisedAnalysesTab", hide = FALSE)
    biomex.toggleMenuElement(name = "survivalAnalysisTab", hide = FALSE)
    biomex.toggleMenuElement(name = "machineLearningTab", hide = FALSE)
    biomex.toggleSubmenuElement(name = "dimensionalityReductionTab", hide = FALSE)
    biomex.toggleSubmenuElement(name = "clusteringTab", hide = FALSE)
    biomex.toggleSubmenuElement(name = "heatmapTab", hide = FALSE)
    biomex.toggleSubmenuElement(name = "graphTab", hide = FALSE)
    biomex.toggleSubmenuElement(name = "metadataGraphTab", hide = FALSE)
    biomex.toggleSubmenuElement(name = "markerSetAnalysisTab", hide = FALSE)
    biomex.toggleSubmenuElement(name = "bruteForceTab", hide = FALSE)
    biomex.toggleSubmenuElement(name = "differentialAnalysisTab", hide = FALSE)
    biomex.toggleSubmenuElement(name = "competitiveSetEnrichmentAnalysisTab", hide = FALSE)
    biomex.toggleSubmenuElement(name = "selfContainedSetEnrichmentAnalysisTab", hide = FALSE)
    biomex.toggleSubmenuElement(name = "pathwayMappingTab", hide = FALSE)
    biomex.toggleSubmenuElement(name = "metaAnalysisTab", hide = FALSE)
    biomex.toggleSubmenuElement(name = "clusterSimilarityTab", hide = FALSE)
    
    if (experimentInformation$Technology == "scRNA-seq")
    {
      biomex.toggleMenuElement(name = "singleCellAnalysesTab", hide = FALSE)
      biomex.toggleSubmenuElement(name = "pseudotimeAnalysisTab", hide = FALSE)
      biomex.toggleSubmenuElement(name = "clusterPredictionTab", hide = FALSE)
    }
    else
    {
      biomex.toggleMenuElement(name = "singleCellAnalysesTab", hide = TRUE)
      biomex.toggleSubmenuElement(name = "pseudotimeAnalysisTab", hide = TRUE)
      biomex.toggleSubmenuElement(name = "clusterPredictionTab", hide = TRUE)
    }
    
    if (experimentInformation$Data == "Corrected" || experimentInformation$PerformedBatchCorrection == "Yes")
    {
      biomex.toggleMenuElement(name = "supervisedAnalysesTab", hide = TRUE)
    }
  }
  else
  {
    biomex.toggleMenuElement(name = "featureEngineeringTab", hide = TRUE)
    biomex.toggleMenuElement(name = "quantificationTab", hide = TRUE)
    biomex.toggleMenuElement(name = "unsupervisedAnalysesTab", hide = TRUE)
    biomex.toggleMenuElement(name = "supervisedAnalysesTab", hide = TRUE)
    biomex.toggleMenuElement(name = "singleCellAnalysesTab", hide = TRUE)
    biomex.toggleMenuElement(name = "survivalAnalysisTab", hide = TRUE)
    biomex.toggleMenuElement(name = "machineLearningTab", hide = TRUE)
    biomex.toggleSubmenuElement(name = "dimensionalityReductionTab", hide = TRUE)
    biomex.toggleSubmenuElement(name = "clusteringTab", hide = TRUE)
    biomex.toggleSubmenuElement(name = "heatmapTab", hide = TRUE)
    biomex.toggleSubmenuElement(name = "graphTab", hide = TRUE)
    biomex.toggleSubmenuElement(name = "metadataGraphTab", hide = TRUE)
    biomex.toggleSubmenuElement(name = "markerSetAnalysisTab", hide = TRUE)
    biomex.toggleSubmenuElement(name = "bruteForceTab", hide = TRUE)
    biomex.toggleSubmenuElement(name = "differentialAnalysisTab", hide = TRUE)
    biomex.toggleSubmenuElement(name = "competitiveSetEnrichmentAnalysisTab", hide = TRUE)
    biomex.toggleSubmenuElement(name = "selfContainedSetEnrichmentAnalysisTab", hide = TRUE)
    biomex.toggleSubmenuElement(name = "pathwayMappingTab", hide = TRUE)
    biomex.toggleSubmenuElement(name = "pseudotimeAnalysisTab", hide = TRUE)
    biomex.toggleSubmenuElement(name = "clusterPredictionTab", hide = TRUE)
    biomex.toggleSubmenuElement(name = "metaAnalysisTab", hide = TRUE)
    biomex.toggleSubmenuElement(name = "clusterSimilarityTab", hide = TRUE)
  }
  
  # # Meta analysis ====
  # # If there are at least two analyses saved, show it
  # if (length(metaAnalysisData) > 1)
  # {
  #   biomex.toggleSubmenuElement(name = "metaAnalysisTab", hide = FALSE)
  # }
  # else
  # {
  #   biomex.toggleMenuElement(name = "metaAnalysisTab", hide = TRUE)
  # }
  # 
  # # Cluster similarity ====
  # # If there are at least two analyses saved, show it
  # if (length(clusterSimilarityData) > 1)
  # {
  #   biomex.toggleSubmenuElement(name = "clusterSimilarityTab", hide = FALSE)
  # }
  # else
  # {
  #   biomex.toggleMenuElement(name = "clusterSimilarityTab", hide = TRUE)
  # }
}
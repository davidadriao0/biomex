#' Hide or show a submenu element.
#' 
#' @param name The name (id) of the submenu element.
#' @param hide Whether to hide the element or not (TRUE or FALSE).
biomex.toggleSubmenuElement <- function(name, hide = TRUE)
{
  string <- paste0("#dashboardPage div aside section ul li ul a[href=\'#shiny-tab-", name, "\']")
  
  if (hide == FALSE)
    js$showMenu(name)
  else
    js$hideMenu(name)
}
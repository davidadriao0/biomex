#' Check consistency between the data type and the data values.
#' 
#' @param dataMatrix The data matrix data.table.
#' @param experimentInformation The list which contains the experiment information.
#' 
#' @return NULL if the check is passed. A list with an error message if the check is not passed.
biomex.checkNegativeProcessing <- function(dataMatrix, experimentInformation)
{
  # If the data is corrected, do not check for negative values
  if (experimentInformation$Data == "Corrected") return (NULL)
  
  # Micro-array normalized data can contain negative values
  if (experimentInformation$Type == "Micro-array" && experimentInformation$Data == "Normalized") return (NULL)
  
  if (experimentInformation$Type == "Metabolomics")
    negativeValues <- biomex.checkForNegativeValues(dataMatrix = dataMatrix, columnsToIgnore = c("Feature", "FeatureMetabolomics"))
  else
    negativeValues <- biomex.checkForNegativeValues(dataMatrix = dataMatrix, columnsToIgnore = "Feature")
      
  # If there are negative values in the data, the processing must be stopped
  if (negativeValues == TRUE)
  {
    return (list(errorMessage = "The data cannot be negative if you annotated it as 'Raw' or 'Normalized'."))
  }
  
  return (NULL)
}
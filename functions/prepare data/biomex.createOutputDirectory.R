#' Create the BIOMEX output directory.
#' 
#' @param combinedAnnotation The combined annotation data.table. If NULL, the function will just create the root folder.
#' @param csvDataLocation The data.table that contains the location of the csv files.
#' @param workingDirectory The working directory (where BIOMEX is located).
#' @param deleteExistingDirectory Whether to delete the existing directory or not.
biomex.createOutputDirectory <- function(combinedAnnotation, csvDataLocation, mainDirectory, deleteExistingDirectory = TRUE)
{
  # Delete the folder, if already exists
  if (deleteExistingDirectory == TRUE) unlink(mainDirectory, recursive = TRUE)
  
  # Create the main BIOMEX folder
  dir.create(mainDirectory, showWarnings = FALSE, recursive = TRUE)
  
  if (!is.null(combinedAnnotation))
  {
    # Create the folders for each experiment
    experiments <- combinedAnnotation$Name
    
    for (experiment in experiments)
    {
      unlink(file.path(mainDirectory, experiment), recursive = TRUE)
      dir.create(file.path(mainDirectory, experiment), showWarnings = FALSE, recursive = TRUE)
    }
  }
}
#' Read the data matrix
#' 
#' @param experimentInformation The list which contains the experiment information.
#' @param csvDataLocation The data.table that contains the location of the csv files.
#' 
#' @return A list with the read data matrix. A list with an error message if it fails.
biomex.getDataMatrix <- function(experimentInformation, csvDataLocation)
{
  location <- experimentInformation$Location
  
  # CSV
  if (location == "CSV")
  {
    # Get the csv path
    file <- experimentInformation$File
    path <- csvDataLocation[File == file, Path]
    
    # Read data matrix 
    dataMatrix <- biomex.readFile(path)
  }
  
  # If the data matrix has one column, something is wrong with the file, so return an error message
  if (is.null(dataMatrix)) return (list(errorMessage = "Data matrix file not found or it has an incorrect format."))
  if (ncol(dataMatrix) == 1) return (list(errorMessage = "The data matrix has only one column."))

  # Rename the first columns and make sure the first columns are characters
  if (experimentInformation$Type == "Metabolomics")
  {
    setnames(dataMatrix, colnames(dataMatrix)[c(1, 2)], c("Feature", "FeatureMetabolomics"))
    
    # Using the class function is not memory efficient
    # class(dataMatrix$Feature) <- "character"
    # class(dataMatrix$FeatureMetabolomics) <- "character"
    dataMatrix[, Feature := as.character(dataMatrix$Feature)]
    dataMatrix[, FeatureMetabolomics := as.character(dataMatrix$FeatureMetabolomics)]
    
    dataMatrix[is.na(FeatureMetabolomics), FeatureMetabolomics := ""] # Convert NA to empty strings
  }
  else
  {
    setnames(dataMatrix, colnames(dataMatrix)[1], "Feature")
    
    # Using the class function is not memory efficient
    # class(dataMatrix$Feature) <- "character"
    dataMatrix[, Feature := as.character(dataMatrix$Feature)]
  }
  
  # Check data consistecy (all columns except the first [and second for metabolomics] must be numeric)
  columnClasses <- sapply(dataMatrix, class)
  if (experimentInformation$Type == "Metabolomics" && any(!columnClasses[-c(1, 2)] %in% c("integer", "numeric"))) return (list(errorMessage = "One of your observation columns is not numeric."))
  if (experimentInformation$Type != "Metabolomics" && any(!columnClasses[-1] %in% c("integer", "numeric"))) return (list(errorMessage = "One of your observation columns is not numeric."))
  
  # Check for NA and NaN
  # if (any(is.na(dataMatrix))) return (list(errorMessage = "The data matrix contains NA or NaN. Check your file."))
  isNA <- biomex.checkNA(dataTable = dataMatrix)
  if (isNA == TRUE) return (list(errorMessage = "The data matrix contains NA or NaN. Check your file."))
  
  # Make sure the observations have an unique name and trim whitespaces. Also trim the whitespaces from the features.
  setnames(dataMatrix, make.unique(trimws(colnames(dataMatrix))))
  dataMatrix[, Feature := trimws(dataMatrix$Feature)] 
  
  resultList <- list(dataMatrix = dataMatrix)
  
  return (resultList)
}
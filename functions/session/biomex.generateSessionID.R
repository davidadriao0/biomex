#' Generate a session ID. This is done by just taking the milliseconds elapsed since the UNIX epoch.
#' 
#' @return The session ID (integer).
biomex.generateSessionID <- function()
{
  # Create session id
  id <- trunc(as.numeric(Sys.time()) * 1000) # Milliseconds since the UNIX epoch
  
  return (id)
}
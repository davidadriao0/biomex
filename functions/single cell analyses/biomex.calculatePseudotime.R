#' Calculate the trajectory pseudotime.
#'
#' @param trajectoryTime The trajectory time (subset originalEllipse).
#' @param originalEllipse The original full fitted ellipse.
#' @param fittingLoops How many times to loop the data.
#' @param pseudotimeCalculationMethod How to calculate the pseudotime (integration vs euclidian distance).
#' @param invertPseudotime Whether to invert the pseudotime direction or not.
#' 
#' @return The ellipse with the Pseudotime and PseudotimePercentage columns added.
biomex.calculatePseudotime <- function(trajectoryTime, originalEllipse, fittingLoops, pseudotimeCalculationMethod, invertPseudotime)
{ 
  e <- trajectoryTime
  
  # Assign ordering (1 to nrow)
  if (invertPseudotime == FALSE)
    e$Pseudotime <- 0:(nrow(e) - 1)
  else
    e$Pseudotime <- (nrow(e) - 1):0
  
  # Order by pseudotime
  e <- e[order(e$Pseudotime), ]
  
  # Assign correct pseudotime (based on distance)
  distances <- 0 # Start from 0
  for (i in 2:nrow(e))
  {
    print(i)
    
    p1 <- e[i - 1, c("x", "y")]
    p2 <- e[i, c("x", "y")]
    
    if (pseudotimeCalculationMethod == "Complex") distance <- biomex.calculateEllipseDistance(p1, p2, originalEllipse, invertPseudotime) # p1 and p2 must have rownames
    else if (pseudotimeCalculationMethod == "Simple")  distance <- as.numeric(dist(matrix(c(p1, p2), ncol = 2, byrow = TRUE)))
    
    distances <- c(distances, distance)
  }
  
  distances <- cumsum(distances)
  e$Pseudotime <- distances
  
  # Calculate distance between last point and first point (for looping)
  p1 <- e[nrow(e), c("x", "y")]
  p2 <- e[1, c("x", "y")]
  
  if (pseudotimeCalculationMethod == "Complex") loopDistance <- biomex.calculateEllipseDistance(p1, p2, originalEllipse, invertPseudotime) # p1 and p2 must have rownames
  else if (pseudotimeCalculationMethod == "Simple") loopDistance <- as.numeric(dist(matrix(c(p1, p2), ncol = 2, byrow = TRUE)))
    
  # Repeat data (if necessary)
  newE <- e
  for (i in 1:fittingLoops)
  {
    if (i == 1)
      next
    
    temp <- e
    maxPseudotime <- max(newE$Pseudotime)
    temp$Pseudotime <- (maxPseudotime + loopDistance) + temp$Pseudotime
    # print(max(temp$Pseudotime - min(temp$Pseudotime)))
    
    newE <- rbind(newE, temp)
  }
  
  # Calculate pseudotime in percentage
  newE$PseudotimePercentage <- (newE$Pseudotime / max(newE$Pseudotime)) * (100 * fittingLoops)
  
  return (newE)
}
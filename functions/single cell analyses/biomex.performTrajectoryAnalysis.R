#' Perform the trajectory pseudotime analysis.
#'
#' @param scoresMatrix The dimensionality reduction score matrix.
#' @param metadataMatrix The metadata matrix data.table.
#' @param variable The observation variable selected in the design of experiment.
#' @param group The groups selected in the design of experiment.
#' @param fittingType The type of fitting to perform (Ellipse, etc.).
#' @param fittingLoops How many times to loop the data.
#' @param pseudotimeCalculationMethod How to calculate the pseudotime (integration vs euclidian distance).
#' @param invertPseudotime Whether to invert the pseudotime direction or not.
#' @param startingObservation The observation selected as the starting point.
#' 
#' @return A list with: the looped data with pseudotime, the non-looped data with pseudotime, the full ellipse and the loops added.
#' A list with an error message if it fails.
biomex.performTrajectoryAnalysis <- function(scoresMatrix, metadataMatrix, variable, group, fittingType, fittingLoops, pseudotimeCalculationMethod, invertPseudotime, startingObservation)
{
  scoresMatrix <- global$drResults$scoresMatrix
  
  # Check the case when the DR is done on the grouped matrix. In that case, no trajectory analysis can be performed.
  index <- which(scoresMatrix$Observation %in% metadataMatrix$Observation)
  if (length(index) == 0) return (list(errorMessage = "You cannot perform the analysis on the Normal grouped data matrix."))
  
  # Get correct subset
  metadataMatrix <- biomex.getDataTableSubset(dataTable = metadataMatrix, rows = scoresMatrix$Observation)
  
  # Get observations for the reference and experimental groups
  if (!"" %in% group)
  {
    variableValues <- metadataMatrix[[variable]]
    index <- which(variableValues %in% group)
    observations <- metadataMatrix$Observation[index]
  }
  else
  {
    observations <- metadataMatrix$Observation
  }
  
  # Subset the score matrix and the metadata with the groups selected
  scoresMatrix <- biomex.getDataTableSubset(dataTable = scoresMatrix, rows = observations)
  metadataMatrix <- biomex.getDataTableSubset(dataTable = metadataMatrix, rows = observations)
  
  # scoresMatrix needs to have at least 3 rows
  if (nrow(scoresMatrix) < 3) return (list(errorMessage = "You need to have at least 3 observations."))
  
  # Fitting
  if (fittingType == "Ellipse")
  {
    fittingLoops <- fittingLoops + 2 # 2 loops are added by default
    ellipseFit <- fit.ellipse(scoresMatrix[, 2:3])
    # HACK: times 5000 is done to avoid overlapping points. Make it more elegant, because it could not work for all scenarios (it would crash otherwise)
    ellipse <- get.ellipse(ellipseFit, n = nrow(scoresMatrix) * 5000)
    originalEllipse <- ellipse
  }
  
  # Calculate distance
  distance <- RANN::nn2(data = ellipse, query = scoresMatrix[, 2:3], k = 1)
  distance <- as.numeric(distance$nn.idx)
  
  # It will crash if I don't get a 1:1 mapping, for the reason explained above (make the code better later)         
  ellipse <- as.data.frame(ellipse)
  ellipse$Observation <- ""
  ellipse[distance, "Observation"] <- scoresMatrix$Observation
  ellipse <- ellipse[ellipse$Observation != "", ]
  
  # Calculate density
  res <- RANN::nn2(ellipse[, c("x", "y")], k = nrow(ellipse), searchtype = "radius", radius = 1)
  density <- rowSums(res$nn.idx > 0) - 1
  ellipse$Density <- density
  
  # Change starting observation (if needed)
  if (!"" %in% startingObservation)
  {
    # Re-shape data.frame starting from the observation selected
    indexClosest <- which(ellipse$Observation == startingObservation)
    upper <- ellipse[indexClosest:nrow(ellipse), ]
    lower <- ellipse[1:(indexClosest - 1), ]
    ellipse <- rbind(upper, lower)
  }
  
  newE <- biomex.calculatePseudotime(trajectoryTime = ellipse, originalEllipse = originalEllipse, fittingLoops = fittingLoops, 
                                     pseudotimeCalculationMethod = pseudotimeCalculationMethod, invertPseudotime = invertPseudotime)
  
  # Add pseudotime to the no loops ellipse
  ellipse$Pseudotime <- newE[1:nrow(ellipse), "Pseudotime"]
  ellipse$PseudotimePercentage <- newE[1:nrow(ellipse), "PseudotimePercentage"]
  
  resultList <- list(trajectoryTime = as.data.table(newE), trajectoryTimeNoLoops = as.data.table(ellipse), fittingLoops = fittingLoops, ellipse = originalEllipse)
  
  return (resultList)
}
#' Perform the differential analysis using limma.
#' 
#' @param dataMatrix The data matrix data.table.
#' @param metadataMatrix The metadata matrix data.table.
#' @param experimentInformation The experiment information.
#' @param mappingTable The mapping table.
#' @param dataMatrixAsInput The type of data matrix provided as input.
#' @param features The features selected.
#' @param observations The observations selected.
#' @param variable The target variables to perform the differential analysis on.
#' @param referenceGroup The reference group names.
#' @param experimentalGroup The experimental group names.
#' @param covariates The covariates to use in the analysis.
#' @param type The type of differential analysis model to use.
#' @param mixedModelTable (Lme4 only) The mixed model table with information about the covariates.
#' @param cores The number of cores
#'
#' @return A list with: the differential expression, the model matrix, the model formula and the observations used. A list with an error message if it fails.
biomex.differentialAnalysis <- function(dataMatrix, metadataMatrix, experimentInformation, mappingTable, dataMatrixAsInput,
                                        features, observations, variable, referenceGroup, experimentalGroup, covariates, type, mixedModelTable,
                                        cores)
{
  # Differential analysis cannot be performed on Corrected data
  if (dataMatrixAsInput == "Normal" && (experimentInformation$Data == "Corrected" || experimentInformation$PerformedBatchCorrection == "Yes")) return (list(errorMessage = "You cannot perform a differential analysis on batch corrected data. Use the non-batch corrected data with the batch as a covariate instead."))
  
  # MAST cannot be used on the Engineered data
  if (dataMatrixAsInput == "Engineered" && type == "MAST") return (list(errorMessage = "MAST can be used only on zero-inflated count data."))
 
  # If the user didn't select any groups, do nothing
  if ("" %in% referenceGroup || "" %in% experimentalGroup) return (list(errorMessage = "The reference and experimental groups cannot be empty."))
  
  # If one covariate matches with the variable, do nothing
  if (any(covariates == variable)) return (list(errorMessage = "You cannot have your target group as a covariate."))
  
  # Check consistency of the mixed model table
  if (type == "Lme4")
  {
    if (is.null(mixedModelTable)) return (list(errorMessage = "You need to select at least one covariate with mixed models."))
    if ("" %in% mixedModelTable$Type) return (list(errorMessage = "No type selected in the mixed model table."))
    if ("" %in% mixedModelTable$Group) return (list(errorMessage = "No group selected in the mixed model table."))
    if ("" %in% mixedModelTable$Order) return (list(errorMessage = "No order selected in the mixed model table."))
    if (length(unique(mixedModelTable$Type)) == 1 && unique(mixedModelTable$Type) == "Fixed") return (list(errorMessage = "No random variables selected."))
    if (!all(check.numeric(mixedModelTable$Order, exceptions = NULL))) return (list(errorMessage = "The order field must be numeric."))
  }
  
  # Get correct subset
  metadataMatrix <- biomex.getDataTableSubset(dataTable = metadataMatrix, rows = observations)
  
  # Get observations for the reference and experimental groups
  variableValues <- metadataMatrix[[variable]]
  referenceIndex <- which(variableValues %in% referenceGroup)
  experimentalIndex <- which(variableValues %in% experimentalGroup)
  referenceObservations <- metadataMatrix$Observation[referenceIndex]
  experimentalObservations <- metadataMatrix$Observation[experimentalIndex]
  referenceNumber <- length(referenceObservations)
  experimentalNumber <- length(experimentalObservations)
  
  # Re-subset the metadata matrix and data matrix
  metadataMatrix <- biomex.getDataTableSubset(dataTable = metadataMatrix, rows = c(referenceObservations, experimentalObservations))
  variableValues <- metadataMatrix[[variable]]
  dataMatrix <- biomex.getDataMatrixSubset(dataMatrix = dataMatrix, columns = c(referenceObservations, experimentalObservations))
  
  # Translate names if the data matrix is Normal
  if (dataMatrixAsInput == "Normal") 
  {
    featuresData <- biomex.getFeatureNames(dataMatrix = dataMatrix)
    featuresConverted <- biomex.mapFeatures(featuresData, from = "Original", to = "Name", mappingTable = mappingTable)
    dataMatrix[, Feature := featuresConverted] 
  }
  
  # If you don't have enough observations, do nothing
  if (referenceNumber == 1) return (list(errorMessage = "Not enough observations in the reference condition."))
  if (experimentalNumber == 1) return (list(errorMessage = "Not enough observations in the experimental condition."))
  
  # If one observation is present both in the reference and experimental condition, stop
  if (any(experimentalObservations %in% referenceObservations)) return (list(errorMessage = "Observations are overlapping between the reference and experimental condition."))
  
  # Check if your variable has more than one level
  if (length(unique(variableValues)) == 1) return (list(errorMessage = "Your target variable has only one value (level)."))
  
  # Check the covariates (cannot have covariates with just one factor level)
  covariatesToRemove <- c()
  for (covariate in covariates)
  {
    if (covariate == "") next
    
    covariateValues <- metadataMatrix[[covariate]]
    if (length(unique(covariateValues)) == 1) covariatesToRemove <- c(covariatesToRemove, covariate)
  }
  
  if (!is.null(covariatesToRemove))
  {
    covariates <- setdiff(covariates, covariatesToRemove)
    if (length(covariates) == 0) covariates <- ""
  }
  
  # Transform covariates "" to NULL (so it doesn't get appended to vectors)
  if ("" %in% covariates) covariates <- NULL
  if (type == "T-Test" || type == "Wilcoxon") covariates <- NULL # covariates cannot be used with standard statistics

  # Create model formula
  # env = NULL is necessary to avoid having a huge file size if the parent environment is big, since formula objects also save the parent environment, which leads to huge file sizes when saved to file
  modelFormula <- as.formula(biomex.getModelFormula(metadataMatrix = metadataMatrix, variable = variable, covariates = covariates, type = "Linear model"), env = NULL)
  
  # The target variable must be set to 0 or 1
  metadataObservations <- metadataMatrix$Observation
  metadataMatrix[[variable]] <- 2
  metadataMatrix[metadataObservations %in% referenceObservations, variable] <- 0
  metadataMatrix[metadataObservations %in% experimentalObservations, variable] <- 1
  
  if (2 %in% metadataMatrix[[variable]]) return (list(errorMessage = "Consistency check failed."))
  
  # Creating the model matrix
  modelMatrix <- model.matrix(modelFormula, as.data.frame(metadataMatrix[, c(variable, covariates), with = FALSE]))
  rownames(modelMatrix) <- metadataMatrix$Observation
  
  # Check if the differential analysis can actually be performed
  nRows <- nrow(modelMatrix)
  nCols <- ncol(modelMatrix)
  modelMatrixRank <- qr(modelMatrix)$rank
  # Checking dimension
  if(nRows <= nCols) return (list(errorMessage = "Insufficient number of samples."))
  # Checking rank
  if(modelMatrixRank < nCols) return (list(errorMessage = "Insufficient number of independent samples."))

  # RNA-seq data needs to be re-normalized with VOOM, to be compatible with limma
  # The other data types don't need to be re-normalized with VOOM
  if (experimentInformation$Technology == "RNA-seq" && experimentInformation$Data == "Raw" && dataMatrixAsInput == "Normal")
  {
    allFeatures <- biomex.getFeatureNames(dataMatrix = dataMatrix)
    allObservations <- biomex.getObservationNames(dataMatrix = dataMatrix)
    
    # Re-normalize data
    dataMatrix[, Feature := NULL]
    dataMatrix <- DGEList(dataMatrix)
    dataMatrix <- calcNormFactors(dataMatrix)
    
    # Normalizing the data 2: VOOM normalization. VOOM only works if there are replicates.
    if (referenceNumber > 1 && experimentalNumber > 1)
    {
      dataMatrix <- voom(dataMatrix, modelMatrix)
      dataMatrix <- as.data.table(getEAWP(dataMatrix)$exprs)
    }
    else
    {
      dataMatrix <- as.data.table(edgeR::cpm(dataMatrix, log = TRUE, prior.count = 1))
    }
    
    # Restore the feature names
    dataMatrix[, Feature := allFeatures]
    setcolorder(dataMatrix, c("Feature", allObservations))
  }
  
  # Perform the differential analysis
  allFeatures <- biomex.getFeatureNames(dataMatrix = dataMatrix)
  dataMatrix[, Feature := NULL]
  features <- biomex.mapFeatures(features, from = "Original", to = "Name", mappingTable = mappingTable)
  
  # We don't subset from the engineered data matrix (we cannot)
  if (dataMatrixAsInput == "Normal") index <- which(allFeatures %in% features)
  else index <- 1:nrow(dataMatrix)
  
  # Perform the differential analysis based on the selected type
  if (type == "Limma")
  {
    # Perform the differential analysis with the linear model formula  
    model <- tryCatch({
      model <- lmFit(object = dataMatrix, design = modelMatrix)
    }, error = function(err) {
      print(err)
      return (NULL)
    })
    
    if (is.null(model)) return (list(errorMessage = "Could not compute the linear model."))
    
    # The eBayes step is different depending on the data type
    bayesModel <- tryCatch({
      if (dataMatrixAsInput == "Normal")
      {
        # For bulk RNA-seq data which we have the raw counts, we use normal limma, since we use voom before (limma-voom)
        if (experimentInformation$Technology == "RNA-seq" && experimentInformation$Data == "Raw") bayesModel <- eBayes(fit = model, trend = FALSE)
        # For everything else (normalized data), we use limma-trend 
        else bayesModel <- eBayes(fit = model, trend = TRUE)
      }
      else if (dataMatrixAsInput == "Engineered")
      {
        # For the engineered data, we use normal limma
        bayesModel <- eBayes(fit = model)
      }
    }, error = function(err) {
      print(err)
      return (NULL)
    })
    
    if (is.null(bayesModel)) return (list(errorMessage = "Could not compute the empirical bayes step."))
    
    # Format the target variable in the right way
    variableToUse <- variable
    containSpecialCharacter <- grepl(" ", variableToUse, fixed = TRUE) || grepl("+", variableToUse, fixed = TRUE) || grepl("-", variableToUse, fixed = TRUE) || grepl("/", variableToUse, fixed = TRUE) 
    if (containSpecialCharacter == TRUE) variableToUse <- paste0("`", variable, "`")
    
    differentialExpression <- tryCatch({   
      differentialExpression <- as.data.table(topTable(fit = bayesModel[index, ], coef = variableToUse, number = nrow(dataMatrix), adjust.method = "BH", sort.by = "none"))
    }, error = function(err) {
      print(err)
      return (NULL)
    })
    
    if (is.null(differentialExpression)) return (list(errorMessage = "Could not create the summary table of the linear model."))

    # Format the table in a nice way
    setnames(differentialExpression, colnames(differentialExpression), c("Log fold change", "Average expression", "t", "Pvalue", "Adjusted pvalue", "B"))
    differentialExpression <- differentialExpression[, c("Log fold change", "Average expression", "Pvalue", "Adjusted pvalue")]
    columns <- copy(colnames(differentialExpression))
    differentialExpression[, Feature := allFeatures[index]]
    setcolorder(differentialExpression, c("Feature", columns))
  }
  else if (type == "Lme4")
  {
    # Create model formula
    mixedModelFormula <- as.formula(biomex.getModelFormula(metadataMatrix = metadataMatrix, variable = variable, covariates = covariates, type = "Mixed model",
                          mixedModelTable = mixedModelTable))
    
    # Calculate the average expression, log fold change and statistics
    calculateStatistics <- function(dataVector, metadataMatrix, variable, modelFormula)
    {
      # Get the index for the reference and experimental observations
      indexReference <- which(metadataMatrix[[variable]] == 0)  # 0 = reference observations
      indexExperimental <- which(metadataMatrix[[variable]] == 1) # 1 = experimental observations
      
      # Get the values for the reference and experimental observations
      referenceValues <- dataVector[indexReference]
      experimentalValues <- dataVector[indexExperimental]
      
      # Calculate the average of the reference observations
      averageReference <- mean(referenceValues)
      
      # Calculate the average of the experimental observations
      averageExperimental <- mean(experimentalValues)
      
      # Calculate the log fold change
      logFoldChange <- averageExperimental - averageReference # This is because we assume the data is log transformed
      
      # Calculate the average expression (by using the two means found previously)
      averageExpression <- ((length(indexReference) * averageReference) + length(indexExperimental) * averageExperimental) / (length(indexReference) + length(indexExperimental))
      
      # Perform the t.test
      pvalue <- biomex.mixedModel(dataVector, metadataMatrix, variable, modelFormula)
      
      return (list(LogFoldChange = logFoldChange, AverageExpression = averageExpression, Pvalue = pvalue))
    }
    
    # Mixed models
    resultList <- apply(dataMatrix[index], 1, calculateStatistics, metadataMatrix, variable, mixedModelFormula)
    
    # Format the table in a nice way
    logFoldChanges <- sapply(resultList, "[[", "LogFoldChange")
    averageExpressions <- sapply(resultList, "[[", "AverageExpression")
    pvalues <- sapply(resultList, "[[", "Pvalue")
    adjustedPvalues <-  p.adjust(pvalues, "BH")
    differentialExpression <- data.table(Feature = features, `Log fold change` = logFoldChanges, `Average expression` = averageExpressions, Pvalue = pvalues, `Adjusted pvalue` = adjustedPvalues)
  }
  else if (type == "MAST")
  {
    # MAST does not support variable with special characters inside
    variableToUse <- variable
    containSpecialCharacter <- grepl(" ", variableToUse, fixed = TRUE) || grepl("+", variableToUse, fixed = TRUE) || grepl("-", variableToUse, fixed = TRUE) || grepl("/", variableToUse, fixed = TRUE) 
    if (containSpecialCharacter == TRUE) return (list(errorMessage = "Cannot perform the differential analysis if the variable of interest contains special characters."))
    
    # Create the matrix for MAST
    dataMatrixForMAST <- as.matrix(dataMatrix)
    rownames(dataMatrixForMAST) <- allFeatures
    colnames(dataMatrixForMAST) <- colnames(dataMatrix)
    
    # Create the additional information necessary for MAST to work
    metadataMatrix$wellKey <- metadataMatrix$Observation 
    featureData <- data.table(Feature = allFeatures, primerid = allFeatures)
    
    # Create the MAST object
    mastObject <- FromMatrix(exprsArray = dataMatrixForMAST, cData = metadataMatrix, fData = featureData)
    
    # We need to make sure that the variable column is a factor
    condition <- factor(x = colData(mastObject)[[variable]])
    condition <- relevel(x = condition, ref = "0")
    colData(mastObject)[[variable]] <- condition
    
    # Run the MAST hurdle model
    model <- tryCatch({
      options(mc.cores = cores)
      model <- zlm(formula = modelFormula, sca = mastObject, method = "bayesglm", ebayes = TRUE, parallel = TRUE)
    }, error = function(err) {
      print(err)
      return (NULL)
    })
    
    if (is.null(model)) return (list(errorMessage = "Could not compute the hurdle linear model."))
    
    # Get the summary of the model for the contrast of interest
    experimentalContrast <- paste0(variable, "1") # "0" is the reference, "1" is the experimental condition
    
    summaryTable <- tryCatch({
      summaryTable <- summary(model, doLRT = experimentalContrast, logFC = FALSE)$datatable # We use the normal log fold change instead
    }, error = function(err) {
      print(err)
      return (NULL)
    })
    
    if (is.null(summaryTable)) return (list(errorMessage = "Could not create the summary table of the hurdle linear model."))
    
    # Generate the differential expression table
    differentialExpression <- summaryTable[contrast == experimentalContrast & component == 'H', .(primerid, `Pr(>Chisq)`)] # Hurdle p-values
    differentialExpression[, fdr := p.adjust(`Pr(>Chisq)`, 'BH')]
    
    # Re-order the differential expression table based on the data matrix ordering
    matchIndex <- match(allFeatures, differentialExpression[[1]])
    differentialExpression <- differentialExpression[matchIndex]
    
    # Calculate the average expression and log fold change
    calculateStatistics <- function(dataVector, classLabels)
    {
      # Get the index for the reference and experimental observations
      indexReference <- which(classLabels == 0)  # 0 = reference observations
      indexExperimental <- which(classLabels == 1) # 1 = experimental observations
      
      # Get the values for the reference and experimental observations
      referenceValues <- dataVector[indexReference]
      experimentalValues <- dataVector[indexExperimental]
      
      # Calculate the average of the reference observations
      averageReference <- mean(referenceValues)
      
      # Calculate the average of the experimental observations
      averageExperimental <- mean(experimentalValues)
      
      # Calculate the log fold change
      logFoldChange <- averageExperimental - averageReference # This is because we assume the data is log transformed
      
      # Calculate the average expression (by using the two means found previously)
      averageExpression <- ((length(indexReference) * averageReference) + length(indexExperimental) * averageExperimental) / (length(indexReference) + length(indexExperimental))
      
      return (list(AverageExpression = averageExpression, LogFoldChange = logFoldChange))
    }
    
    resultList <- apply(dataMatrixForMAST, 1, calculateStatistics, metadataMatrix[[variable]])
    averageExpression <- sapply(resultList, "[[", "AverageExpression")
    logFoldChange <- sapply(resultList, "[[", "LogFoldChange")
    
    # Add the average expression and log fold change to the differential expression table
    differentialExpression[, logFoldChange := logFoldChange]
    differentialExpression[, averageExpression := averageExpression]
    
    # Format the table in a nice way
    setnames(differentialExpression, colnames(differentialExpression), c("Feature", "Pvalue", "Adjusted pvalue", "Log fold change", "Average expression"))
    setcolorder(differentialExpression, c("Feature", "Log fold change", "Average expression", "Pvalue", "Adjusted pvalue"))
    differentialExpression <- differentialExpression[index]
  }
  else if (type == "T-Test" || type == "Wilcoxon")
  {
    # Calculate the average expression, log fold change and statistics
    calculateStatistics <- function(dataVector, classLabels, statisticalTest)
    {
      # Get the index for the reference and experimental observations
      indexReference <- which(classLabels == 0)  # 0 = reference observations
      indexExperimental <- which(classLabels == 1) # 1 = experimental observations
      
      # Get the values for the reference and experimental observations
      referenceValues <- dataVector[indexReference]
      experimentalValues <- dataVector[indexExperimental]
      
      # Calculate the average of the reference observations
      averageReference <- mean(referenceValues)
      
      # Calculate the average of the experimental observations
      averageExperimental <- mean(experimentalValues)
      
      # Calculate the log fold change
      logFoldChange <- averageExperimental - averageReference # This is because we assume the data is log transformed
      
      # Calculate the average expression (by using the two means found previously)
      averageExpression <- ((length(indexReference) * averageReference) + length(indexExperimental) * averageExperimental) / (length(indexReference) + length(indexExperimental))
      
      # Perform the t.test
      pvalue <- statisticalTest(referenceValues, experimentalValues, alternative = "two.sided", paired = FALSE)$p.value
      
      return (list(LogFoldChange = logFoldChange, AverageExpression = averageExpression, Pvalue = pvalue))
    }
    
    if (type == "T-Test") statisticalTest <- t.test
    if (type == "Wilcoxon") statisticalTest <- wilcox.test
    
    resultList <- apply(dataMatrix[index], 1, calculateStatistics, metadataMatrix[[variable]], statisticalTest)
    logFoldChanges <- sapply(resultList, "[[", "LogFoldChange")
    averageExpressions <- sapply(resultList, "[[", "AverageExpression")
    pvalues <- sapply(resultList, "[[", "Pvalue")
    adjustedPvalues <-  p.adjust(pvalues, "BH")
    differentialExpression <- data.table(Feature = features, `Log fold change` = logFoldChanges, `Average expression` = averageExpressions, Pvalue = pvalues, `Adjusted pvalue` = adjustedPvalues)
  }
  else
  {
    return (list(errorMessage = "You need to select a differential analysis type."))
  }
  
  resultList <- list(differentialExpression = differentialExpression, modelMatrix = modelMatrix, modelFormula = modelFormula, observations = c(referenceObservations, experimentalObservations))
  
  return (resultList)
}
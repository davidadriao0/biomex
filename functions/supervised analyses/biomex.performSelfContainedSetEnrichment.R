#' Perform the self contained set enrichment analysis.
#' 
#' @param dataMatrix The data matrix data.table.
#' @param differentialExpression The differential expression.
#' @param differentialExpression The observations used in the differential analysis.
#' @param modelMatrix The model matrix used during the differential analysis.
#' @param variable The target variable used in the differential analysis.
#' @param mappingTable The mapping table.
#' @param featureSets The feature sets to peform the analysis on. 
#' @param minimumSetSize The minimum set size.
#' @param rotations The number of permutations for the bootstrapping.
#' @param randomSeed The random seed to get consistent results.
#'
#' @return A list with the mroast result data table. A list with an error message if it fails.
biomex.performSelfContainedSetEnrichment <- function(dataMatrix, differentialExpression, observations, modelMatrix, variable, mappingTable, featureSets, minimumSetSize, rotations, randomSeed)
{
  # Get data from the differential expression
  originalFeatures <- biomex.mapFeatures(differentialExpression$Feature, from = "Name", to = "Original", mappingTable = mappingTable)
  features <- biomex.mapFeatures(differentialExpression$Feature, from = "Name", to = "ID", mappingTable = mappingTable)

  # Subset data matrix
  dataMatrix <- biomex.getDataMatrixSubset(dataMatrix = dataMatrix, columns = observations, rows = originalFeatures)
  
  # Get the indexes for each set
  featureIndexes <- list()
  for (i in 1:length(featureSets))
  {
    index <- which(features %in% featureSets[[i]])
    featureIndexes[i] <- list(index)
  }
  
  names(featureIndexes) <- names(featureSets)
  empty <- (unname(unlist(lapply(featureIndexes, length))) == 0)
  featureIndexes <- featureIndexes[!empty]
  
  # Filter by set size
  lengths <- sapply(featureIndexes, length)
  featureIndexes <- featureIndexes[lengths >= minimumSetSize]
  
  if (length(featureIndexes) == 0) return (list(errorMessage = "Not enough sets."))
  
  # Compute mroast and refine result
  variableToUse <- variable
  containSpace <- grepl(" ", variable, fixed = TRUE)
  if (containSpace == TRUE) variableToUse <- paste0("`", variable, "`")
  
  set.seed(randomSeed)
  dataMatrix[, Feature := NULL]
  sseaTable <- mroast(dataMatrix, featureIndexes, modelMatrix, contrast = variableToUse, nrot = rotations)
  sseaTable$Set <- rownames(sseaTable)
  sseaTable <- as.data.table(sseaTable[, c("Set", "NGenes", "PropDown", "PropUp", "Direction", "PValue", "FDR")])
  setnames(sseaTable, colnames(sseaTable), c("Set", "Number of features", "Proportion down", "Proportion up", "Direction", "Pvalue", "Adjusted pvalue"))
  
  return (list(enrichmentTable = sseaTable, differentialExpression = differentialExpression))
}


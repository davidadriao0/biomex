#' Setup the survival analysis. 
#' 
#' @param metadataMatrix The metadata matrix data.table.
#' @param observationsSelected The observation selected in the design of experiment.
#' @param targetVariable The selected target variable (e.g. the one containing dead or alive).
#' @param eventVariable The name of the event of interest (e.g. dead).
#' @param timeVariable The selected time variable (e.g. the one containing the time to last follow up or time of death).
#' @param divideTime Whether to divide the time by 365 or not.
#' @param stratificationVariable The selected stratification variable.
#' 
#' @return A list which contains: the filtered and refined metadata to use in the survival analysis. A list with an error message if it fails.
biomex.setupSurvivalAnalysis <- function(metadataMatrix, observationsSelected, eventVariable, targetEvent, timeVariable, divideTime, stratificationVariable)
{
  # Get the right subset
  metadataMatrix <- biomex.getDataTableSubset(dataTable = metadataMatrix, rows = observationsSelected)
  
  if (eventVariable == "" || eventVariable == "Not selected") return (list(errorMessage = "You need to select the event variable."))
  if (timeVariable == "" || timeVariable == "Not selected") return (list(errorMessage = "You need to select the time variable."))
  
  # Take the relevant columns and rename them
  if (stratificationVariable == "Not selected") 
  {
    metadataMatrix$Stratification <- "No stratification"
    stratificationVariable <- "Stratification"
  }
 
  metadataMatrix <- metadataMatrix[, c("Observation", eventVariable, timeVariable, stratificationVariable), with = FALSE]
  colnames(metadataMatrix) <- c("Observation", "Target", "Time", "Stratification")
  
  # Remove the empty rows
  metadataMatrix <- metadataMatrix[Target != "" & !is.na(Target) & Time != "" & !is.na(Time) & Stratification != "" & !is.na(Stratification)]
  
  # Check for consistency. Target must have only 2 possible values (e.g. dead or alive), and Time must be numeric.
  if (length(unique(metadataMatrix$Target)) > 2) return (list(errorMessage = "Only two possible events allowed."))
  if (!all(check.numeric(metadataMatrix$Time, exceptions = NULL))) return (list(errorMessage = "The time event must be numeric."))
  
  # Make the time column numeric
  metadataMatrix$Time <- as.numeric(metadataMatrix$Time)
  
  # Divide time by 365 (if needed)
  if (divideTime == TRUE) metadataMatrix$Time <- metadataMatrix$Time / 365
  
  # Add the event column
  metadataMatrix$Event <- grepl(targetEvent, metadataMatrix$Target, ignore.case = TRUE)
  if (sum(metadataMatrix$Event) == 0) return (list(errorMessage = "Target event was not found."))
  
  resultList <- list(metadataMatrix = metadataMatrix)
  
  return (resultList)
}
    

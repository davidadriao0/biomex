#' Function to easily create a progress object, since it is used heavily in BIOMEX.
#' 
#' @param message The message you want to display.
#' @param progressValue The value of the progress bar (default is 1, since it is the most used in BIOMEX)
#' 
#' @return An initialized progress object.
biomex.createProgressObject <- function(message, style, progressValue = 1)
{
  # Create a Progress object
  progress <- shiny::Progress$new(style = style)
  progress$set(message = message, value = progressValue)
  
  # Close the progress when this reactive exits (even if there's an error)
  # on.exit(progress$close())
  
  return (progress)
}
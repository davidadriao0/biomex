#' Create the recent loaded experiments table.
#'
#' @param filePath The location of the recent loaded experiments table.
#' 
#' @return The recent loaded experiment table with the button to load the experiment.

biomex.createRecentExperimentsTable <- function(filePath)
{
  # Read the recent loaded experiments file
  recentExperiments <- biomex.readFile(filePath)
  
  if (is.null(recentExperiments) || nrow(recentExperiments) == 0) return (NULL)
  
  # Create the data table with the buttons inside ====
  recentExperiments <- reactiveValues(
    data = data.table(
      Time = recentExperiments$Time,
      Path = recentExperiments$Path,
      Load = shinyInput(actionButton, nrow(recentExperiments), 'recentExperimentsButton_', label = "Load", onclick = 'Shiny.onInputChange(\"recentExperimentsLoadButton\",  this.id)', icon = icon("check"), style= "color: #fff; background-color: #09a325; border-color: #04931e", width = "100%"),
      Delete = shinyInput(actionButton, nrow(recentExperiments), 'recentExperimentsButton_', label = "Delete", onclick = 'Shiny.onInputChange(\"recentExperimentsDeleteButton\",  this.id)', icon = icon("trash"), style= "color: #fff; background-color: #CE2929; border-color: #C42525", width = "100%")
    )
  )
  
  return (recentExperiments)
}

shinyInput <- function(FUN, len, id, ...) 
{
  inputs <- character(len)
  for (i in seq_len(len)) inputs[i] <- as.character(FUN(paste0(id, i), ...))
  
  inputs
}
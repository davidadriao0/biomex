#' Get the names of the metadata.
#' 
#' @param metadataMatrix The metadata matrix data.table.
#' @param ignoreFirstColumn Whether to take into consideration the first column or not.
#' 
#' @return The names of the metadata.
biomex.getMetadataNames <- function(metadataMatrix, ignoreFirstColumn = FALSE)
{
  names <- colnames(metadataMatrix)
  
  if (ignoreFirstColumn == TRUE)
    names <- names[-1]
  
  if (length(names) == 0)
    names <- NULL
  
  return (names)
}
#' Function to easily create a progress object, since it is used heavily in BIOMEX.
#' 
#' @param features The features to convert.
#' @param from The original type of the features.
#' @param to The desistion type of the features.
#' @param mappingTable The mapping table.
#' 
#' @return The converted features.
biomex.mapFeatures <- function(features, from, to, mappingTable)
{
  fromFeatures <- mappingTable[[from]]
  toFeatures <- mappingTable[[to]]
  
  index <- which(fromFeatures %in% features)
  
  if (length(index) == length(features))
  {
    mappingTable <- mappingTable[index]
    
    # Order features
    fromFeatures <- mappingTable[[from]]
    toFeatures <- mappingTable[[to]]
    index <- match(features, fromFeatures)
    
    # Wrong do not uncomment
    # index <- index[!is.na(index)]
    # index <- order(index)
    
    mappedFeatures <- toFeatures[index]
  }
  else
    stop("Cannot match the features.")
  
  return (mappedFeatures)
}
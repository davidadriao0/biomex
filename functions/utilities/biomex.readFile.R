#' Read a text or csv file.
#'
#' @param filePath The path of the file of interest.
#' 
#' @return The data.table containing the content of the file. NULL if the file couldn't be read.
biomex.readFile <- function(filePath, sep = "auto", colClasses = NA, nrows = -1)
{
  # Read data
  data <- tryCatch({
    data <- fread(filePath, sep = sep, colClasses = colClasses, nrows = nrows)
  }, error = function(err) {
    print(err)
    return (NULL)
  }) # , warning = function(w) {
  #   return (NULL)
  # })
  
  return (data)
}



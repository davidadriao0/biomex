#' Validate the value given back by the checkboxGroupInput widget.
#' 
#' @param value The value returned by the checkboxGroupInput.
biomex.validateCheckboxGroupInput <- function(value)
{
  if (is.null(value))
    value <- ""
  
  return (value)
}
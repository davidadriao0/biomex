#' Validate the value given back by the numericInput widget.
#' 
#' @param value The value returned by the numerInput.
#' @param defaultTo The value to default to if "value" is not allowed.
#' @param min The allowed minimum value.
#' @param max The allowed maximum value.
#' @param asInteger Whether you want to return the value as integer (so it will be truncated if it is a float or double).
biomex.validateNumericInput <- function(value, defaultTo, min = -Inf, max = Inf, asInteger = FALSE)
{
  if (is.null(value) || is.na(value))
    value <- defaultTo
  
  if (value < min)
    value <- min
  
  if (value > max)
    value <- max
  
  if (asInteger == TRUE) value <- as.integer(value)
  
  return (value)
}
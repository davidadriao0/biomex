#' The interface of the cluster similarity tab.
interfaceClusterSimilarity <- function()
{
  tabItem(tabName = "clusterSimilarityTab", fluidRow(
    column(
      width = 9,
      
      tabBox(
        title = "Cluster similarity analysis",
        width = NULL,
        id = "csMain",
        
        tabPanel("Plot",
          conditionalPanel(
            condition = "input['csResultType'] == 'Dimensionality reduction'",
            withSpinner(plotlyOutput("csDrPlot", height = 800, width = "100%"))
          ),

          conditionalPanel(
            condition = "input['csResultType'] == 'Congruent features'",
            withSpinner(plotlyOutput("csCfPlot", height = 800, width = "100%"))
          ),

          conditionalPanel(
            condition = "input['csResultType'] == 'Venn diagram'",
            tags$div(style = "overflow-x: scroll;", uiOutput("csVdPlotOut"))
          )
        ),
            
        tabPanel("Table",      
          withSpinner(shiny::dataTableOutput("csTable"), type = 7)
        )
      )
    ),
    
    column(
      width = 3,
      
      tags$div(id = "csSideSettings",
      box(
        title = "Settings",
        width = NULL,
        status = "success",
        solidHeader = FALSE,
        collapsible = TRUE,
        collapsed = FALSE,
        id = "csSettingsBox",
        
        actionButton("csMethodSettings", label = tags$b("Method settings"), icon = icon("cogs"), width = "100%", style = "color: #fff; background-color: #F1C40F; border-color: #F39C12"),       
        actionButton("csDataSelectionSettings", label = tags$b("Data selection settings"), icon = icon("cogs"), width = "100%", style = "color: #fff; background-color: #F1C40F; border-color: #F39C12"),
        actionButton("updateClusterSimilarity", label = tags$b("Update"), icon = icon("play"), width = "100%", style="color: #fff; background-color: #09a325; border-color: #04931e")
      )),
      
      tags$div(id = "csSideControls",
               
      tags$div(id = "csGroupToVisualizeBoxDiv", box(
        title = "Group to visualize",
        width = NULL,
        status = "primary",
        solidHeader = FALSE,
        collapsible = TRUE,
        collapsed = FALSE,
        
        conditionalPanel(
          condition = "input['csResultType'] == 'Dimensionality reduction'",
          tags$p("Not available for dimensionality reduction.")
        ),
        
        conditionalPanel(
          condition = "input['csResultType'] == 'Congruent features' || input['csResultType'] == 'Venn diagram'",
          selectInput("csGroupToVisualize", label = NULL, choices = NULL, selected = NULL)
        )
      )),
      
      box(
        title = "Customization",
        width = NULL,
        status = "warning",
        solidHeader = FALSE,
        collapsible = TRUE,
        collapsed = FALSE,
        
        actionButton("csResultSettings", label = tags$b("Result settings"), icon = icon("cogs"), width = "100%", style = "color: #fff; background-color: #F1C40F; border-color: #F39C12"),
      
        conditionalPanel(
          condition = "input['csMain'] == 'Plot'",
          conditionalPanel(
            condition = "input['csResultType'] == 'Dimensionality reduction' || input['csResultType'] == 'Congruent features'",   
            actionButton("csPlotSettings", label = tags$b("Plot settings"), icon = icon("cogs"), width = "100%", style = "color: #fff; background-color: #F1C40F; border-color: #F39C12"),
            actionButton("csColorSettings", label = tags$b("Color settings"), icon = icon("cogs"), width = "100%", style = "color: #fff; background-color: #F1C40F; border-color: #F39C12")
          )
        )
      ),
      
      biomex.storeAndLoadResultsInterface(id = "cs"),
      
      box(
        title = "Exporting",
        width = NULL,
        status = "info",
        solidHeader = FALSE,
        collapsible = TRUE,
        collapsed = TRUE,
      
        conditionalPanel(
          condition = "input['csMain'] == 'Plot'",
          
          conditionalPanel(
            condition = "input['csResultType'] == 'Dimensionality reduction' || input['csResultType'] == 'Congruent features'",    
            actionButton("exportCsPlot", label = tags$b("Export plot"), icon = icon("arrow-down"), width = "100%", style = "color: #fff; background-color: #3498DB; border-color: #2980B9")
          ),
              
          conditionalPanel(
            condition = "input['csResultType'] == 'Venn diagram'",    
            actionButton("exportCsPicture", label = tags$b("Export picture"), icon = icon("arrow-down"), width = "100%", style = "color: #fff; background-color: #3498DB; border-color: #2980B9")
          )
        ),
        conditionalPanel(
          condition = "input['csMain'] == 'Table'",
          actionButton("exportCsTable", label = tags$b("Export table"), icon = icon("arrow-down"), width = "100%", style = "color: #fff; background-color: #3498DB; border-color: #2980B9")
        )
      )
    )),
    
    # Method settings
    bsModal("csMethodSettingsModal", "Method settings", NULL, size = "large", trigger = "csMethodSettings", close.button = TRUE,
      numericInput("csNumberOfFeatures", label = h5("Number of (top) features to use"), min = 1, max = Inf, step = 1, value = 50, width = "100%"),
      checkboxInput("csToUpper", "Apply upper case to the features")
    ),
    
    # Data selection settings
    bsModal("csDataSelectionSettingsModal", "Data selection settings", NULL, size = "large", trigger = "csDataSelectionSettings", close.button = FALSE,   
      footer = actionButton("csDeleteEntry", label = tags$b("Delete selected entries"), icon = icon("trash"), width = "100%", style = "color: #fff; background-color: #CE2929; border-color: #C42525"),
      selectizeInput("csDataSelected", label = h5("Data to select"), choices = NULL, selected = NULL, multiple = TRUE, width = "100%", options = (list(placeholder = "Select the data to use in the analysis"))),
      selectizeInput("csGroupToRemove", label = h5("Group to remove"), choices = NULL, selected = NULL, multiple = TRUE, width = "100%", options = (list(placeholder = "Select the groups (columns) to remove (if necessary)"))),
      selectizeInput("csClassToRemove", label = h5("Class to remove"), choices = NULL, selected = NULL, multiple = TRUE, width = "100%", options = (list(placeholder = "Select the classes to remove (if necessary)"))),
      withSpinner(shiny::dataTableOutput("csSelectionTable"), type = 7)
    ),
    
    # Result settings
    bsModal("csResultSettingsModal", "Result settings", NULL, size = "small", trigger = "csResultSettings", close.button = TRUE, 
    
    selectInput("csResultType", label = h5("Result to visualize"), choices = c("Dimensionality reduction", "Congruent features", "Venn diagram"), selected = "Dimensionality reduction", width = "100%"),
      conditionalPanel(
          condition = "input['csResultType'] == 'Congruent features'",
          sliderInput("csCfPercentageThreshold", label = h5("Occurence threshold:"), min = 0, max = 100, value = c(25, 75), width = "100%"),
          sliderInput("csCfCommonMedianThreshold", label = h5("Common features rank threshold:"), min = 0, max = 100, value = 35, width = "100%"),
          sliderInput("csCfSpecificMedianThreshold", label = h5("Specific features rank threshold:"), min = 0, max = 100, value = 20, width = "100%")
      )
    ),
        
    # Plot settings
    bsModal("csPlotSettingsModal", "Plot settings", NULL, size = "small", trigger = "csPlotSettings", close.button = TRUE, 
            
      conditionalPanel(
        condition = "input['csResultType'] == 'Dimensionality reduction' || input['csResultType'] == 'Congruent features'",
        sliderInput("csDotSize", label = h5("Dots size:"), min = 1, max = 30, value = 10),
        checkboxInput("csShowGrid", "Show grid", value = TRUE)
      ),
            
      conditionalPanel(
        condition = "input['csResultType'] == 'Dimensionality reduction'",
        checkboxInput("csDrShow3DPlot", "Show 3D plot", value = FALSE),
        checkboxInput("csLegendHide", "Hide legend", value = FALSE),
        checkboxInput("csLegendBottom", "Put legend at the bottom", value = FALSE),
        selectInput("csLegendSize", label = h5("Legend text size"), choices = as.character(1:20), selected = "12")
      )
    ),
    
    # Color settings
    bsModal("csColorSettingsModal", "Color settings", NULL, size = "large", trigger = "csColorSettings", close.button = TRUE, 
      
      # Dimensionality reduction
      conditionalPanel(
        condition = "input['csResultType'] == 'Dimensionality reduction'",
        
        radioButtons("csColorSettingsSelection", label = NULL, choices = c("Colors", "Symbols"), selected = "Colors", inline = TRUE),
        
        # Colors
        conditionalPanel(
          condition = "input['csColorSettingsSelection'] == 'Colors'",
          selectInput("csDrMetadataColor", label = h5("Select color:"), choices = c("Plotly", "Rainbow", "Heat.colors", "Terrain.colors", "Topo.colors", "Cm.colors", "Custom"), selected = "Plotly", width = "100%"),
          conditionalPanel(
            condition = "input.csDrMetadataColor == 'Custom'",
            withSpinner(rHandsontableOutput("csDrColorCustomTable"), type = 7, proxy.height = "100px")
          ),
          checkboxInput("csDrReverseColors", label = "Reverse colors", value = FALSE, width = "100%")
        ),
        
        # Symbols
        conditionalPanel(
          condition = "input['csColorSettingsSelection'] == 'Symbols'",
          selectInput("csDrMetadataSymbol", label = h5("Select symbol:"), choices = c("Normal", "Custom"), selected = "Normal", width = "100%"),
          conditionalPanel(
            condition = "input.csDrMetadataSymbol == 'Custom'",
            withSpinner(rHandsontableOutput("csDrSymbolCustomTable"), type = 7, proxy.height = "100px")
          ),
          checkboxInput("csDrReverseSymbols", label = "Reverse symbols", value = FALSE, width = "100%")
        )
      ),
      
      # Congruent features
      conditionalPanel(
        condition = "input['csResultType'] == 'Congruent features'",
        textInput("csCfCommonColor", label = h5("Common features color"), value = "red", width = "100%", placeholder = "Type a color name or hexadecimal code"),
        textInput("csCfSpecificColor", label = h5("Specific features color"), value = "blue",  width = "100%", placeholder = "Type a color name or hexadecimal code"),
        textInput("csCfNotSelectedColor", label = h5("Not selected features color"), value = "black", width = "100%", placeholder = "Type a color name or hexadecimal code")
      ),
      
      checkboxInput("csShowColorPicker", label = "Show color picker"),
      conditionalPanel(
        condition = "input['csShowColorPicker'] == 1",
        colourpicker::colourInput("csColorPicker", label = NULL, allowTransparent = TRUE)
      )
    )
  ))
}
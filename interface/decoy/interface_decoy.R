#' The interface of the Decoy.
#' What is this decoy? This is the last element rendered in the hierarchy, which has the property to be executed as the last one in the chain of events.
#' We will use this decoy to set some toggle variables when an event happens, for example when switching experiments.
#' When switching experiments, we need to set the global$switchingExperiment variable to TRUE. After all the chain of event is finished, then we need to set
#' this variable to FALSE. So this variable will be set to FALSE when the decoy event (always the last one) is called.
interfaceDecoy <- function()
{  
  tabItem(tabName = "decoyTab", fluidRow(
    hidden(selectInput("decoyLoadLogic", label = NULL, choices = NULL, selected = NULL)),
    hidden(selectInput("decoySwitchExperiment", label = NULL, choices = NULL, selected = NULL)),
    hidden(selectInput("decoyLoadStoredResults", label = NULL, choices = NULL, selected = NULL))
  ))
}
biomex.storeAndLoadResultsInterface <- function(id)
{
  # **** IDs ====
  # Global
  globalSettingsId <- paste0(id, "DoeSettings")
  globalFinalSettingsId <- paste0(id, "DoeFinalSettings")
  globalResultsId <- paste0(id, "DoeResults")
  globalAnalysisSettingsId <- paste0(id, "Settings")
  globalAnalysisFinalSettingsId <- paste0(id, "FinalSettings")
  globalAnalysisResultsId <- paste0(id, "Results")
  globalAnalysisAdditionalResultsId <- paste0(id, "AdditionalResults")
  globalAnalysisPlotId <- paste0(id, "Plot")
  globalAnalysisTableId <- paste0(id, "Table")
  globalAnalysisStoredResults <- paste0(id, "StoredResults")
  
  # Box id
  storeAndLoadBoxId <- paste0(id, "StoreAndLoadBox")
  storeAndLoadBoxDivId <- paste0(id, "StoreAndLoadBoxDiv")
  
  # Action buttons
  storeResultId <- paste0(id, "StoreResult")
  loadResultId <- paste0(id, "LoadResult")
  performStoreResultId <- paste0(id, "PerformStoreResult")
  performLoadResultId <- paste0(id, "PerformLoadResult")
  performDeleteResultId <- paste0(id, "PerformDeleteResult")
  
  # Useful variables
  performedId <- paste0(id, "DoePerformed")
  performedAnalysisId <- paste0(id, "Performed")
  triggerId <- paste0(id, "DoeTrigger")
  triggerAnalysisId <- paste0(id, "Trigger")
  
  # Widget ids
  storeResultNameId <- paste0(id, "StoreResultName")
  loadResultNameId <- paste0(id, "LoadResultName")
  
  # Modals
  storeResultModalId <- paste0(storeResultId, "Modal")
  loadResultModalId <- paste0(loadResultId, "Modal")
  
  tags$div(
    
    tags$div(
    id = storeAndLoadBoxDivId,
    
      box(
        title = "Store and load results",
        width = NULL,
        status = "danger",
        solidHeader = FALSE,
        collapsible = TRUE,
        collapsed = TRUE,
        id = storeAndLoadBoxId,
        
        actionButton(storeResultId, label = tags$b("Store result"), icon = icon("upload"), width = "100%", style = "color: #fff; background-color: #B22222; border-color: #9e1f1f"),
        actionButton(loadResultId, label = tags$b("Load result"), icon = icon("download"), width = "100%", style = "color: #fff; background-color: #B22222; border-color: #9e1f1f")
      )
    ),
    
    # Modals
    # Store result
    bsModal(storeResultModalId, "Store result", NULL, size = "small", trigger = storeResultId,
      footer = actionButton(performStoreResultId, label = tags$b("Store"), icon = icon("play"), width = "100%", style="color: #fff; background-color: #09A325; border-color: #04931E"), close.button = FALSE, 
      textInput(storeResultNameId, label = h5("Name to store the result with"), width = "100%", placeholder = "Type a name")
    ),
    
    # Load result
    bsModal(loadResultModalId, "Load result", NULL, size = "small", trigger = NULL, 
      footer = tags$div(
                actionButton(performLoadResultId, label = tags$b("Load"), icon = icon("play"), width = "48%", style="color: #fff; background-color: #09A325; border-color: #04931E; display:inline-block"),
                actionButton(performDeleteResultId, label = tags$b("Delete"), icon = icon("trash"), width = "48%", style = "color: #fff; background-color: #CE2929; border-color: #C42525; display:inline-block")
               ), close.button = FALSE, 
      selectInput(loadResultNameId, label = h5("Result to load"), choices = "", selected = "", width = "100%")
    )
  )
}                  
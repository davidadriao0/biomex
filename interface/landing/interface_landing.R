#' The interface of the landing page.
interfaceLanding <- function()
{
  tabItem(tabName = "landingTab", fluidRow(
       
    box(
      title = NULL,
      width = 12,
      
      HTML('
        <span class = "rmcirclespan"><span></span></span>
        <div class="rmwrap">
          <div class="rmslice"><div onclick="Shiny.onInputChange(\'newExperiment\', (new Date()).getTime());"></div></div>
          <div class="rmslice"><div onclick="Shiny.onInputChange(\'loadExperiment\', (new Date()).getTime());"></div></div>
          <div class="rmslice"><div onclick="Shiny.onInputChange(\'openCaseStudies\', (new Date()).getTime());"></div></div>
          <div class="rmslice"><div onclick="Shiny.onInputChange(\'recentlyLoadedExperiments\', (new Date()).getTime());"></div></div>
          <div class="rmslice" id="rmlogoslice"><div onclick="Shiny.onInputChange(\'logo\', (new Date()).getTime());"></div></div>
        </div>
      ')
    ),
    
    box(
      title = "Welcome to BIOMEX!",
      width = 12,
      status = "primary",
      
      HTML("
      <p>BIOMEX was created to empower bench scientists to unlock insights from complex -omics datasets through interactive visualization. BIOMEX is free for everyone.</p>
      <p><b><h4>First time users should check out the manual before proceeding.</h4></b></p>
      <p>You can open the manual by clicking on the \"?\" button on the top right of the page.</p>
      <p><b><h4>Be sure to read the additional information by clicking on the link below!</b></h4></p>"),
      actionLink("additionalInformation", label = "See additional information.")
    ),
    
      
    #   box(
    #     title = "Start or load an experiment",
    #     width = NULL,
    #     status = "success",
    #     actionButton("newExperiment", label = tags$b("Analyse a new experiment"), icon = icon("pencil"), width = "100%", style = "color: #fff; background-color: #09A325; border-color: #04931E"),
    #     actionButton("loadExperiment", label = tags$b("Continue a previous session"), icon = icon("upload"), width = "100%", style = "color: #fff; background-color: #09A325; border-color: #04931E")
    #    ),
    #     
    #   tags$div(id = "recentExperimentsBoxDiv", box(
    #      title = "Recently loaded experiments",
    #      width = NULL,
    #      status = "warning",
    #      
    #      actionButton("recentlyLoadedExperiments", label = tags$b("Open the recently loaded experiments list"),  icon = icon("table"), width = "100%", style = "color: #fff; background-color: #F1C40F; border-color: #F39C12")
    #   )),
    #   
    #   box(
    #     title = "Case studies",
    #     width = NULL,
    #     status = "info",
    #     id = "caseStudiesBox",
    #     
    #     actionButton("openCaseStudies", label = tags$b("Open case studies"), icon = icon("list"), width = "100%", style = "color: #fff; background-color: #3498DB; border-color: #2980B9")
    #   )
    # ),
      
    # column(
    #   width = 6,
    #   
    #   box(
    #     title = "",
    #     width = NULL,
    #       
    #     tags$p(style = "text-align: center",
    #     tags$img(src = "landing/BIOMEX.png", width = "500px", height = "600px", `object-fit` = "contain"))
    #   )
    # ),
      
    # Modals ====
    # Additional information
    bsModal("additionalInformationModal", "Additional information", trigger = "additionalInformation", size = "large", close.button = TRUE,
      HTML("
      <p>Before continuing, there are some concepts to keep in mind while using BIOMEX.
      <ul>
        <li>
        In BIOMEX, we make use of prompts to enable you to select directories and files (the output folder, the data files, etc.).
        <b>Sometimes, these kind of prompts open 'under' the current browser window.</b>
        If that happens, you can minimize the browser window or check the taskbar to see the prompt.
        </li>
        <li>
        When BIOMEX is performing a computation, a progress bar is going to appear in the bottom right corner of the page. 
        <b>Until the computation is finished (and the progress bar disappears), BIOMEX will become unresponsive to your input.</b>
        Please wait until the computation is performed before continuing using BIOMEX.
        </li>
        <li>
        <b>By closing the progress bar, computations are not stopped</b>. We recommend not closing the progress bar, since you can use it to see when a computation has finished.
        </li>
        <li>
        <b>If the screen becomes greyed out and is not responsive anymore, something unexpected happened</b>. If this happens, please send us the application log via email together with a detailed description of the steps taken up until that happened.
        </li>
      </ul>
      </p>")
    ),
    
    
    
    # Recently opened experiments
    bsModal("recentlyLoadedExperimentsModal", "Recently loaded experiments", trigger = NULL, size = "large", close.button = TRUE,
      withSpinner(DT::dataTableOutput("recentLoadedExperimentsTable"), type = 7)
      # Still active in the logic if you uncomment it
      # br(),
      # br(),
      # actionButton("clearRecentLoadedExperimentaTable", label = tags$b("Clear table"), icon = icon("trash"), width = "100%", style = "color: #fff; background-color: #CE2929; border-color: #C42525")
    ),
    
    # Case studies
    bsModal("openCaseStudiesModal", "Case studies", trigger = NULL, size = "large", close.button = TRUE,
      withSpinner(DT::dataTableOutput("caseStudiesTable"), type = 7),
      tags$script(HTML(
        "$(document).on('click', '.clickCaseStudy', function() {",
        "Shiny.onInputChange('clickCaseStudy', $(this).data('value'))",
        "});"))
    )
  ))
}
                  
                  
                  
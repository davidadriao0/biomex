#' The interface of the machine learning tab
interfaceMachineLearning <- function()
{
  mimeType <- c("text/csv", "text/comma-separated-values", "text/plain", ".csv")
  
  tabItem(tabName = "machineLearningTab", fluidRow(
    
    column(
      width = 9,
      
      tabBox(
        title = "Machine learning",
        width = NULL,
        id = "mlMain",
        
        tabPanel("Plot",
          withSpinner(plotlyOutput("mlPlot", height = 800, width = "100%"))
        ),
        
        tabPanel("Table",
          withSpinner(shiny::dataTableOutput("mlTable"), type = 7)
        )
      )
    ),
    
    column(
      width = 3,
      
      biomex.designOfExperimentInterface(id = "ml"),
    
      tags$div(id = "mlSideSettings",
      box(
        title = "Settings",
        width = NULL,
        status = "success",
        solidHeader = FALSE,
        collapsible = TRUE,
        collapsed = FALSE,
        id = "mlSettingsBox",
        
        actionButton("mlMethodSettings", label = tags$b("Method settings"), icon = icon("cogs"), width = "100%", style = "color: #fff; background-color: #F1C40F; border-color: #F39C12"),
        actionButton("mlFeatureSelectionSettings", label = tags$b("Feature selection settings"), icon = icon("cogs"), width = "100%", style = "color: #fff; background-color: #F1C40F; border-color: #F39C12"),
        actionButton("mlModelSettings", label = tags$b("Model settings"), icon = icon("cogs"), width = "100%", style = "color: #fff; background-color: #F1C40F; border-color: #F39C12"),
        actionButton("mlDataToPredictOnSettings", label = tags$b("Data to predict on"), icon = icon("cogs"), width = "100%", style = "color: #fff; background-color: #F1C40F; border-color: #F39C12"),    
        actionButton("updateMachineLearning", label = tags$b("Update"), icon = icon("play"), width = "100%", style="color: #fff; background-color: #09a325; border-color: #04931e")
      )),
      
      tags$div(id = "mlSideControls",
      box(
        title = "Stratification",
        width = NULL,
        status = "primary",
        solidHeader = FALSE,
        collapsible = TRUE,
        collapsed = FALSE,
        
        selectInput("mlVariableToStratifyWith", label = NULL, choices = "Not selected", selected = "Not selected", width = "100%")
      ),
               
      box(
        title = "Customization",
        width = NULL,
        status = "warning",
        solidHeader = FALSE,
        collapsible = TRUE,
        collapsed = FALSE,
        
        conditionalPanel(
          condition = "input['mlMain'] == 'Plot'",      
          actionButton("mlPlotSettings", label = tags$b("Plot settings"), icon = icon("cogs"), width = "100%", style = "color: #fff; background-color: #F1C40F; border-color: #F39C12"),
          actionButton("mlColorSettings", label = tags$b("Color settings"), icon = icon("cogs"), width = "100%", style = "color: #fff; background-color: #F1C40F; border-color: #F39C12")
        ),
        conditionalPanel(
          condition = "input['mlMain'] == 'Table'",      
          actionButton("mlTableSettings", label = tags$b("Table settings"), icon = icon("cogs"), width = "100%", style = "color: #fff; background-color: #F1C40F; border-color: #F39C12")
        )
      ),
      
      biomex.storeAndLoadResultsInterface(id = "ml"),
      
      box(
        title = "Exporting",
        width = NULL,
        status = "info",
        solidHeader = FALSE,
        collapsible = TRUE,
        collapsed = TRUE,
        
        conditionalPanel(
          condition = "input['mlMain'] == 'Plot'",    
          actionButton("exportMlPlot", label = tags$b("Export plot"), icon = icon("arrow-down"), width = "100%", style = "color: #fff; background-color: #3498DB; border-color: #2980B9")
        ),
        
        conditionalPanel(
          condition = "input['mlMain'] == 'Table'",    
          actionButton("exportMlTable", label = tags$b("Export table"), icon = icon("arrow-down"), width = "100%", style = "color: #fff; background-color: #3498DB; border-color: #2980B9")
        )
      )
    )),
    
    # Modals ====
    # Method
    bsModal("mlMethodSettingsModal", "Method settings", NULL, size = "small", trigger = "mlMethodSettings", close.button = TRUE,  
      selectInput("mlDataMatrixAsInput", label = h5("Data matrix to input"), choices = c("Normal", "Engineered"), selected = "Normal", width = "100%"),
      selectInput("mlScaling", label = h5("Scaling"), choices = c("None", "Auto", "Center", "Scale", "Range", "Pareto", "Vast", "Level"), selected = "Auto", width = "100%"),
      conditionalPanel(
        condition = "input['mlDataMatrixAsInput'] == 'Normal'", 
        selectInput("mlFeatureIdentifier", label = h5("Feature identifier to use"), choices = c("Original", "ID", "Name"), selected = "Original")
      ),
      selectInput("mlVariableToPredict", label = h5("Variable to predict"), choices = NULL, selected = NULL, width = "100%")
    ),
    
    # Feature selection
    bsModal("mlFeatureSelectonSettingsModal", "Feature selection settings", NULL, size = "small", trigger = "mlFeatureSelectionSettings", close.button = TRUE, 
      radioButtons("mlFeatureSelectionType", label = NULL, choices = c("None", "Recursive feature elimination"), selected = "None", inline = FALSE),
      
      conditionalPanel(
        condition = "input['mlFeatureSelectionType'] == 'Recursive feature elimination'",    
        numericInput("mlFeatureSelectionMinimum", label = h5("Minimum features"), min = 1, max = Inf, step = 1, value = 50, width = "100%"),
        numericInput("mlFeatureSelectionMaximum", label = h5("Maximum features"), min = 1, max = Inf, step = 1, value = 500, width = "100%"),
        numericInput("mlFeatureSelectionStep", label = h5("Step"), min = 1, max = Inf, step = 1, value = 50, width = "100%"),
        numericInput("mlFeatureSelectionCrossValidationFolds", label = h5("Cross validation folds"), min = 0, max = Inf, step = 1, value = 10, width = "100%"),
        numericInput("mlFeatureSelectionCrossValidationRepeats", label = h5("Cross validation repeats"), min = 0, max = Inf, step = 1, value = 10, width = "100%")
      )
    ),
    
    # Model
    bsModal("mlModelSettingsModal", "Model settings", NULL, size = "small", trigger = "mlModelSettings", close.button = TRUE, 
      radioButtons("mlModel", label = NULL, choices = c("Random forest"), selected = "Random forest", inline = FALSE),
      numericInput("mlTrainingCrossValidationFolds", label = h5("Cross validation folds"), min = 1, max = Inf, step = 1, value = 10, width = "100%"),
      numericInput("mlTrainingCrossValidationRepeats", label = h5("Cross validation repeats"), min = 1, max = Inf, step = 1, value = 10, width = "100%"),
      sliderInput("mlTrainingDataPercentage", label = h5("Training data percentage:"), min = 0, max = 100, value = 75, step = 1),
      numericInput("mlDataSplitRandomSeed", label = h5("Random seed to use when splitting the data"), min = -Inf, max = Inf, step = 1, value = 1234, width = "100%")
    ),
    
    # Data to predict
    bsModal("mlDataToPredictOnSettingsModal", "Data to predict on", NULL, size = "large", trigger = "mlDataToPredictOnSettings", close.button = TRUE, 
      tags$div(id = "mlFileSelectionDiv",
        actionButton("mlDataToPredict", label = tags$b("Upload CSV file with the data"), icon = icon("play"), width = "100%", style="color: #fff; background-color: #09A325; border-color: #04931E"),
        withSpinner(shiny::dataTableOutput("mlDataFilePath"), type = 7, proxy.height = "50px"),
        br(),
        actionButton("mlMetadataToPredict", label = tags$b("Upload CSV file with the metadata"), icon = icon("play"), width = "100%", style="color: #fff; background-color: #09A325; border-color: #04931E"),
        withSpinner(shiny::dataTableOutput("mlMetadataFilePath"), type = 7, proxy.height = "50px")
      ),
      tags$div(id = "mlFileSelectionOnlineDiv",
       fileInput("mlDataToPredictOnline", label = NULL, multiple = FALSE, accept = mimeType, width = "100%", buttonLabel = "Select data", placeholder = "No file selected"),
       fileInput("mlMetadataToPredictOnline", label = NULL, multiple = FALSE, accept = mimeType, width = "100%", buttonLabel = "Select metadata", placeholder = "No file selected")
      )
    ),
        
    # Plot settings
    bsModal("mlPlotSettingsModal", "Plot settings", NULL, size = "small", trigger = "mlPlotSettings", close.button = TRUE, 
      sliderInput("mlLabelSize", label = h5("Label size:"), min = 0, max = 20, value = 10, step = 1),
      sliderInput("mlPadding", label = h5("Padding:"), min = 0, max = 100, value = 10, step = 1),
      sliderInput("mlThickness", label = h5("Thickness:"), min = 0, max = 1000, value = 200, step = 1),
      checkboxInput("mlHideLabels", "Hide labels", value = FALSE)
    ),
    
    # Color settings
    bsModal("mlColorSettingsModal", "Color settings", NULL, size = "large", trigger = "mlColorSettings", close.button = TRUE, 
      
      radioButtons("mlColorSettingsSelection", label = NULL, choices = c("Predicted colors", "Original colors"), selected = "Predicted colors", inline = TRUE),
      
      # Predicted colors
      conditionalPanel(
        condition = "input['mlColorSettingsSelection'] == 'Predicted colors'",
        selectInput("mlNodesPredictedColor", label = h5("Select color:"), choices = c("Plotly", "Rainbow", "Heat.colors", "Terrain.colors", "Topo.colors", "Cm.colors", "Predefined", "Custom"), selected = "Plotly", width = "100%"),
        conditionalPanel(
          condition = "input.mlNodesPredictedColor == 'Custom'",
          withSpinner(rHandsontableOutput("mlNodesPredictedColorCustomTable"), type = 7, proxy.height = "100px")
        ),
        
        checkboxInput("mlPredictedReverseColors", label = "Reverse colors", value = FALSE, width = "100%")
      ),
      
      # Original colors
      conditionalPanel(
        condition = "input['mlColorSettingsSelection'] == 'Original colors'",
        selectInput("mlNodesOriginalColor", label = h5("Select color:"), choices = c("Plotly", "Rainbow", "Heat.colors", "Terrain.colors", "Topo.colors", "Cm.colors", "Custom"), selected = "Rainbow", width = "100%"),
        conditionalPanel(
          condition = "input.mlNodesOriginalColor == 'Custom'",
          withSpinner(rHandsontableOutput("mlNodesOriginalColorCustomTable"), type = 7, proxy.height = "100px")
        ),
        
        checkboxInput("mlOriginalReverseColors", label = "Reverse colors", value = FALSE, width = "100%")
      ),
      
      checkboxInput("mlShowColorPicker", label = "Show color picker"),
      conditionalPanel(
        condition = "input['mlShowColorPicker'] == 1",
        colourpicker::colourInput("mlColorPicker", label = NULL, allowTransparent = TRUE)
      )
    ),
          
    # Table settings
    bsModal("mlTableSettingsModal", "Table settings", NULL, size = "small", trigger = "mlTableSettings", close.button = TRUE, 
      selectInput("mlTableType", label = h5("Table to show"), choices = c("Prediction", "Model metrics", "Confusion matrix", "Prediction metrics", "Variable importance", "Feature selection metrics"), selected = "Prediction", width = "100%")
    )
  ))
}
interfaceHeader <- function()
{
  tags$li(
    class = "dropdown",
    tags$div(style="display:inline-block; margin-top: 8px;", actionButton("settings", "", icon = icon("wrench"))),
    tags$div(style="display:inline-block; margin-top: 8px;", actionButton("displayManual", "", icon = icon("question"))),
    tags$div(style="display:inline-block; margin-top: 8px;", actionButton("reloadExperiment", "", icon = icon("retweet"))),
    tags$div(style="display:inline-block; margin-top: 8px;", actionButton("saveExperiment", "", icon = icon("floppy-o")))
  )
}
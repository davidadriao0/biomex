#' The interface of the metadata tab.
interfaceMetadata <- function()
{
  # This variable should be in sync with the one in the data tab
  analyses <- c("Feature engineering", "Feature quantification",  "Metadata quantification","Dimensionality reduction", "Clustering", "Heatmap",
  "Differential analysis", "Marker set analysis", "Pseudotime analysis", "Cluster prediction", "Survival analysis", "Machine learning")
  mimeType <- c("text/csv", "text/comma-separated-values", "text/plain", ".csv")
  
  tabItem(tabName = "metadataTab", fluidRow(
  
    column(
      width = 9,
      
      box(
        title = "Metadata",
        width = NULL,
        
        withSpinner(shiny::dataTableOutput("metadataMatrix"), type = 7)
      )
    ),
    
    column(
      width = 3,
      
      box(
        title = "Settings",
        width = NULL,   
        status = "success",      
        solidHeader = FALSE,
        collapsible = TRUE,
        collapsed = FALSE,
        
        selectInput("metadataType", label = h5("Metadata to visualize"), choice = c("Full", "After pretreatment", "Subset")), 
        conditionalPanel(
          condition = "input['metadataType'] == 'Subset'",
          selectInput("metadataSubsetType", label = h5("Select the subset type"), choices = analyses, selected = analyses[1], width = "100%")
        )
      ),
      
      box(
        title = "Add",
        width = NULL,
        status = "primary",        
        solidHeader = FALSE,
        collapsible = TRUE,
        collapsed = TRUE,
        
        actionButton("addMetadata", label = tags$b("Add metadata"), icon = icon("play"), width = "100%", style="color: #fff; background-color: #09A325; border-color: #04931E"),
        actionButton("addByFileMetadata", label = tags$b("Add metadata by file"), icon = icon("play"), width = "100%", style="color: #fff; background-color: #09A325; border-color: #04931E"),
        actionButton("copyColumnMetadata", label = tags$b("Copy metadata column"), icon = icon("play"), width = "100%", style="color: #fff; background-color: #09A325; border-color: #04931E")
      ),
      
      box(
        title = "Modify",
        width = NULL,
        status = "warning",        
        solidHeader = FALSE,
        collapsible = TRUE,
        collapsed = TRUE,
      
        actionButton("modifyMetadata", label = tags$b("Modify metadata"), icon = icon("exchange"), width = "100%", style = "color: #fff; background-color: #F1C40F; border-color: #F39C12"),
        actionButton("mergeColumnMetadata", label = tags$b("Merge metadata columns"), icon = icon("exchange"), width = "100%", style = "color: #fff; background-color: #F1C40F; border-color: #F39C12"),
        actionButton("changeValuesMetadata", label = tags$b("Change metadata values"), icon = icon("exchange"), width = "100%", style = "color: #fff; background-color: #F1C40F; border-color: #F39C12")
      ),
      
      box(
        title = "Delete",
        width = NULL,
        status = "danger",        
        solidHeader = FALSE,
        collapsible = TRUE,
        collapsed = TRUE,
        
        actionButton("deleteMetadata", label = tags$b("Delete metadata"), icon = icon("trash"), width = "100%", style = "color: #fff; background-color: #CE2929; border-color: #C42525"),     
        actionButton("deleteMultipleMetadata", label = tags$b("Delete metadata (multiple)"), icon = icon("trash"), width = "100%", style = "color: #fff; background-color: #CE2929; border-color: #C42525")
      ),
      
      # Removed since it can cause conflicts with downstream analyses. The logic is still active in case it is renabled (in logic_metadata.R).
      # box(
      #   title = "Restore",
      #   width = NULL,
      #   solidHeader = FALSE,
      #   collapsible = TRUE,
      #   collapsed = TRUE,
      # 
      #   actionButton("restoreMetadata", label = tags$b("Restore metadata"), icon = icon("repeat"), width = "100%", style = "color: #fff; background-color: #95A5A6; border-color: #7F8C8D")
      # ),
      
      box(
        title = "Define colors",
        width = NULL,
        solidHeader = FALSE,
        collapsible = TRUE,
        collapsed = TRUE,

        actionButton("defineColorsMetadata", label = tags$b("Define colors"), icon = icon("play"), width = "100%", style = "color: #fff; background-color: #95A5A6; border-color: #7F8C8D")
      ),
      
      box(
        title = "Exporting",
        width = NULL,
        status = "info",        
        solidHeader = FALSE,
        collapsible = TRUE,
        collapsed = TRUE,
        
        actionButton("exportMetadata", label = tags$b("Export metadata"), icon = icon("arrow-down"), width = "100%", style = "color: #fff; background-color: #3498DB; border-color: #2980B9")
      )
    ),
    
    # Modals ====
    # Add metadata
    shinyBS::bsModal("addMetadataModal", "Add metadata", NULL, size = "large", 
      footer = actionButton("confirmAddMetadata", label = tags$b("Add"), icon = icon("play"), width = "100%", style="color: #fff; background-color: #09A325; border-color: #04931E"), close.button = FALSE, 
      textInput("addMetadataName", label = h5("Name of the metadata column to add"), width = "100%", placeholder = "Type a name"),
      withSpinner(rHandsontableOutput("addMetadataMatrix"), type = 7)
    ),
    
    # Add metadata by file
    shinyBS::bsModal("addByFileMetadataModal", "Add metadata by file", trigger = "addByFileMetadata", size = "large", 
      footer = actionButton("confirmAddByFileMetadata", label = tags$b("Add"), icon = icon("play"), width = "100%", style="color: #fff; background-color: #09A325; border-color: #04931E"), close.button = FALSE,
      actionButton("addByFileMetadataSelect", label = tags$b("Select file"), icon = icon("arrow-up"), width = "100%", style="color: #fff; background-color: #09A325; border-color: #04931E"),
      withSpinner(shiny::dataTableOutput("addMetadataFilePath"), type = 7, proxy.height = "50px"),
      fileInput("addByFileMetadataSelectOnline", label = NULL, multiple = FALSE, accept = mimeType, buttonLabel = "Select file", placeholder = "No files selected", width = "100%")
    ),
            
    # Copy column metadata
    bsModal("copyColumnMetadataModal", "Copy metadata column", NULL, size = "large", 
      footer = actionButton("confirmCopyColumnMetadata", label = tags$b("Copy"), icon = icon("play"), width = "100%", style="color: #fff; background-color: #09A325; border-color: #04931E"), close.button = FALSE, 
      textInput("copyColumnMetadataName", label = h5("Name of the metadata column to add"), width = "100%", placeholder = "Type a name"),
      selectInput("copyColumnMetadataChoice", label = h5("Metadata to copy"), choices = NULL, selected = NULL, width = "100%")
    ),
    
    # Modify metadata
    shinyBS::bsModal("modifyMetadataModal", "Modify metadata", NULL, size = "large", 
      footer = actionButton("confirmModifyMetadata", label = tags$b("Modify"), icon = icon("play"), width = "100%", style="color: #fff; background-color: #09A325; border-color: #04931E"), close.button = FALSE, 
      selectInput("modifyMetadataName", label = h5("Name of the metadata to modify"), choices = NULL, selected = NULL, width = "100%"),
      withSpinner(rHandsontableOutput("modifyMetadataMatrix"), type = 7)
    ),
        
    # Merge metadata
    bsModal("mergeColumnMetadataModal", "Merge metadata columns", NULL, size = "large", 
      footer = actionButton("confirmMergeColumnMetadata", label = tags$b("Merge"), icon = icon("play"), width = "100%", style="color: #fff; background-color: #09A325; border-color: #04931E"), close.button = FALSE, 
      textInput("mergeColumnMetadataName", label = h5("Name of the metadata column to add"), width = "100%", placeholder = "Type a name"),
      selectizeInput("mergeColumnMetadataChoice", label = h5("Metadata columns to merge"), choices = NULL, selected = NULL, width = "100%", multiple = TRUE, options = (list(placeholder = "Select the metadata columns to merge")))
    ),
    
    # Chage values metadata
    bsModal("changeValuesMetadataModal", "Change metadata values", NULL, size = "large", 
      footer = actionButton("confirmChangeValuesMetadata", label = tags$b("Change"), icon = icon("play"), width = "100%", style="color: #fff; background-color: #09A325; border-color: #04931E"), close.button = FALSE, 
      selectInput("changeValuesMetadataName", label = h5("Metadata column to change"), choices = NULL, selected = NULL, width = "100%"),
      selectizeInput("changeValuesMetadataChoice", label = h5("Values to change"), choices = NULL, selected = NULL, width = "100%", multiple = TRUE, options = (list(placeholder = "Select the values to change/override"))),
      textInput("changeValuesMetadataValue", label = h5("New value"), width = "100%", placeholder = "Type the new value")
    ),
    
    # Delete metadata
    bsModal("deleteMetadataModal", "Delete metadata", NULL, size = "large", 
      footer = actionButton("confirmDeleteMetadata", label = tags$b("Delete"), icon = icon("play"), width = "100%", style="color: #fff; background-color: #09A325; border-color: #04931E"), close.button = FALSE, 
      selectInput("deleteMetadataName", label = h5("Name of the metadata to delete"), choices = NULL, selected = NULL, width = "100%")
    ),
       
    # Delete multiple metadata
    bsModal("deleteMultipleMetadataModal", "Delete metadata", NULL, size = "large", 
      footer = actionButton("confirmDeleteMultipleMetadata", label = tags$b("Delete"), icon = icon("play"), width = "100%", style="color: #fff; background-color: #09A325; border-color: #04931E"), close.button = FALSE, 
      selectizeInput("deleteMultipleMetadataName", label = h5("Names of the metadata to delete"), choices = NULL, selected = NULL, multiple = TRUE, width = "100%", options = (list(placeholder = "Select the metadata to delete")))
    ),
    
    # Define colors metadata
    shinyBS::bsModal("defineColorsMetadataModal", "Define colors", NULL, size = "large", 
      footer = actionButton("confirmDefineColorsMetadata", label = tags$b("Save"), icon = icon("play"), width = "100%", style="color: #fff; background-color: #09A325; border-color: #04931E"), close.button = FALSE, 
      selectInput("defineColorsMetadataName", label = h5("Name of the metadata to define colors on"), choices = NULL, selected = NULL, width = "100%"),
      withSpinner(rHandsontableOutput("defineColorsMetadataMatrix"), type = 7),
      checkboxInput("defineColorsMetadataShowColorPicker", label = "Show color picker"),
      conditionalPanel(
        condition = "input['defineColorsMetadataShowColorPicker'] == 1",
        colourpicker::colourInput("defineColorsMetadataColorPicker", label = NULL, allowTransparent = TRUE)
      ),
      
      checkboxInput("defineColorsMetadataShowPlot", label = "Show color preview"),
      conditionalPanel(
        condition = "input['defineColorsMetadataShowPlot'] == 1",
        withSpinner(plotlyOutput("defineColorsMetadataPlot", width = "100%"))
      )
    )
  ))
}
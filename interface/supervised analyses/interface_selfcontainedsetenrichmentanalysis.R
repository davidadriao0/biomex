#' The interface of the self contained set enrichment analysis tab.
interfaceSelfContainedSetEnrichmentAnalysis <- function()
{
  tabItem(tabName = "selfContainedSetEnrichmentAnalysisTab", fluidRow(
   
    column(
      width = 9,
      
     box(
        title = "Self contained set enrichment analysis",
        width = NULL,
        id = "sseaMain",
        
        withSpinner(DT::dataTableOutput("sseaTable"), type = 7)
      )
    ),
    
    column(
      width = 3,
      
      biomex.designOfExperimentInterface(id = "ssea"),
      
      tags$div(id = "sseaSideSettings",
      box(
        title = "Settings",
        width = NULL,
        status = "success",
        solidHeader = FALSE,
        collapsible = TRUE,
        collapsed = FALSE,
        id = "sseaSettingsBox",
        
        actionButton("sseaMethodSettings", label = tags$b("Method settings"), icon = icon("cogs"), width = "100%", style = "color: #fff; background-color: #F1C40F; border-color: #F39C12"),
        actionButton("updateSelfContainedSetEnrichmentAnalysis", label = tags$b("Update"), icon = icon("play"), width = "100%", style="color: #fff; background-color: #09a325; border-color: #04931e")
      )),
      
      tags$div(id = "sseaSideControls",
      box(
        title = "Customization",
        width = NULL,
        status = "warning",
        solidHeader = FALSE,
        collapsible = TRUE,
        collapsed = FALSE,
        
        actionButton("sseaTableSettings", label = tags$b("Table settings"), icon = icon("cogs"), width = "100%", style = "color: #fff; background-color: #F1C40F; border-color: #F39C12"),  
        actionButton("sseaShowSetContent", label = tags$b("Show set content"), icon = icon("table"), width = "100%", style = "color: #fff; background-color: #F1C40F; border-color: #F39C12")
      ),
      
      biomex.storeAndLoadResultsInterface(id = "ssea"),
      
      box(
        title = "Exporting",
        width = NULL,
        status = "info",
        solidHeader = FALSE,
        collapsible = TRUE,
        collapsed = TRUE,
        
        actionButton("exportSseaTable", label = tags$b("Export table"), icon = icon("arrow-down"), width = "100%", style = "color: #fff; background-color: #3498DB; border-color: #2980B9")
      )
    )),
     
    # Method settings
    bsModal("sseaMethodSettingsModal", "Method settings", NULL, size = "small", trigger = "sseaMethodSettings", close.button = TRUE, 
      selectInput("sseaFeaturesToUse", label = h5("Features to use"), choices = c("All", "Significant (p-value)", "Significant (adjusted p-value)"), selected = "All", width = "100%"),
      numericInput("sseaMinimumSetSize", label = h5("Minimal set size"), min = 2, max = Inf, step = 1, value = 5, width = "100%"),
      numericInput("sseaRotations", label = h5("Number of rotations"), min = 1, max = Inf, step = 100, value = 1000, width = "100%"),
      numericInput("sseaRandomSeed", label = h5("Random seed"), min = -Inf, max = Inf, step = 1, value = 1234, width = "100%")
    ),
    
    # Settings
    bsModal("sseaTableSettingsModal", "Table settings", NULL, size = "small", trigger = "sseaTableSettings", close.button = TRUE, 
      selectInput("sseaFeaturesToShow", label = h5("Features to show"), choices = c("All", "Significant (p-value)", "Significant (adjusted p-value)"), selected = "All", width = "100%")
    ),
   
    # Set content
    bsModal("sseaSetContentModal", "Set content", NULL, size = "large", trigger = NULL, close.button = FALSE,
      footer =  actionButton("exportSseaSetContent", label = tags$b("Export table"), icon = icon("arrow-down"), width = "100%", style = "color: #fff; background-color: #3498DB; border-color: #2980B9"),
      withSpinner(shiny::dataTableOutput("sseaSetContent"), type = 7)
    )
  ))
}
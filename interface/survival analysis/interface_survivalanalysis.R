#' The interface of the survival analysis tab.
interfaceSurvivalAnalysis <- function()
{
  tabItem(tabName = "survivalAnalysisTab", fluidRow(
   
    column(
      width = 9,
      
      box(
        title = "Survival analysis",
        width = NULL,
        id = "saMain",
        
        withSpinner(plotlyOutput("saPlot", height = 800, width = "100%"))
      )
    ),
    
    column(
      width = 3,
      
      biomex.designOfExperimentInterface(id = "sa"),
      
      tags$div(id = "saSideSettings",
      box(
        title = "Settings",
        width = NULL,
        status = "success",
        solidHeader = FALSE,
        collapsible = TRUE,
        collapsed = FALSE,
        id = "saSettingsBox",
        
        actionButton("saMethodSettings", label = tags$b("Method settings"), icon = icon("cogs"), width = "100%", style = "color: #fff; background-color: #F1C40F; border-color: #F39C12"), 
        actionButton("saSurvivalSettings", label = tags$b("Survival settings"), icon = icon("cogs"), width = "100%", style = "color: #fff; background-color: #F1C40F; border-color: #F39C12"), 
        actionButton("updateSurvivalAnalysis", label = tags$b("Update"), icon = icon("play"), width = "100%", style="color: #fff; background-color: #09a325; border-color: #04931e")
      )),
      
      tags$div(id = "saSideControls",
      box(
        title = "Features",
        width = NULL,
        status = "primary",
        solidHeader = FALSE,
        collapsible = TRUE,
        collapsed = FALSE,
        
        radioButtons("saFeatures", label = NULL, choices = c("Original feature", "Engineered feature"), inline = TRUE),
        conditionalPanel(
          condition = "input['saFeatures'] == 'Original feature'",
          selectizeInput("saOriginalFeature", label = NULL, choices = NULL, selected = NULL)
        ),
        conditionalPanel(
          condition = "input['saFeatures'] == 'Engineered feature'",
          selectizeInput("saEngineeredFeature", label = NULL, choices = NULL, selected = NULL)
        )
      ),
      
      box(
        title = "Customization",
        width = NULL,
        status = "warning",
        solidHeader = FALSE,
        collapsible = TRUE,
        collapsed = FALSE,
        
        actionButton("saPlotSettings", label = tags$b("Plot settings"), icon = icon("cogs"), width = "100%", style = "color: #fff; background-color: #F1C40F; border-color: #F39C12"),
        actionButton("saColorSettings", label = tags$b("Color settings"), icon = icon("cogs"), width = "100%", style = "color: #fff; background-color: #F1C40F; border-color: #F39C12") 
      ),
      
      box(
        title = "Information",
        width = NULL,
        solidHeader = FALSE,
        collapsible = TRUE,
        collapsed = TRUE,
        
        actionButton("saInformation", label = tags$b("Information"), icon = icon("info"), width = "100%", style = "color: #fff; background-color: #F1C40F; border-color: #F39C12")
      ),
      
      box(
        title = "Exporting",
        width = NULL,
        status = "info",
        solidHeader = FALSE,
        collapsible = TRUE,
        collapsed = TRUE,
        
        actionButton("exportSaPlot", label = tags$b("Export plot"), icon = icon("arrow-down"), width = "100%", style = "color: #fff; background-color: #3498DB; border-color: #2980B9")
      )
    )),
       
    # Modals ====
    # Method settings
    bsModal("saMethodSettingsModal", "Method settings", NULL, size = "small", trigger = "saMethodSettings", close.button = TRUE, 
      selectInput("saEventVariable", label = h5("Event variable"), choices = NULL, selected = NULL),
      textInput("saEventTarget", label = h5("Event target"), placeholder = "Type the target event (e.g. dead)"),
      selectInput("saTimeVariable", label = h5("Time variable"), choices = NULL, selected = NULL)
    ),
    
    # Survival settings
    bsModal("saSurvivalSettingsModal", "Survival settings", NULL, size = "small", trigger = "saSurvivalSettings", close.button = TRUE, 
      sliderInput("saQuantiles", label = h5("Quantile (lower and upper)"), min = 0, max = 1, value = c(0.25, 0.75)),
      checkboxInput("saExcludeMedium", label = "Exclude inbetween observations"),
      checkboxInput("saDivideTime", "Divide time by 365", value = FALSE),
      selectInput("saStratificationVariable", label = h5("Stratification"), choices = NULL, selected = NULL)
    ),
    
    # Plot settings
    bsModal("saPlotSettingsModal", "Plot settings", NULL, size = "small", trigger = "saPlotSettings", close.button = TRUE, 
      selectInput("saStratificationToVisualize", label = h5("Stratification to visualize"), choices = "All", selected = "All"),
      sliderInput("saLineThickness", label = h5("Line thickness"), min = 0.5, max = 5, value = 0.5, step = 0.5),
      checkboxInput("saLegendHide", "Hide legend", value = FALSE),
      checkboxInput("saGridHide", "Hide grid", value = FALSE)
    ),
    
    # Information
    bsModal("saInformationModal", "Survival analysis information", NULL, size = "large", trigger = "saInformation",
      close.button = TRUE, 
      withSpinner(shiny::dataTableOutput("saInformationTable"), type = 7)
    ),
        
    # Color settings
    bsModal("saColorSettingsModal", "Color settings", NULL, size = "small", trigger = "saColorSettings", close.button = TRUE, 
      textInput("saLineColorLow", label = h5("Lower color"), value = "blue", placeholder = "Type a color name or hexadecimal code"),
      textInput("saLineColorMedium", label = h5("Medium color"), value = "grey", placeholder = "Type a color name or hexadecimal code"),
      textInput("saLineColorHigh", label = h5("Higher color"), value = "red", placeholder = "Type a color name or hexadecimal code"),
      
      checkboxInput("saShowColorPicker", label = "Show color picker"),
      conditionalPanel(
        condition = "input['saShowColorPicker'] == 1",
        colourpicker::colourInput("saColorPicker", label = NULL, allowTransparent = TRUE)
      )
    )
  ))
}
#' Function that handle the set selection logic in BIOMEX.
#' 
#' @param id The id.
biomex.setSelectionLogic <- function(input, output, session, global, id)
{
  # IDs
  # Global
  globalSettingsId <- paste0(id, "Settings")
  globalFinalSettingsId <- paste0(id, "FinalSettings")
  
  # Set selection
  setSelectionTypeId <- paste0(id, "SetSelectionType")
  keggSelectionId <- paste0(id, "KeggSelection")
  setFileSelectionId <- paste0(id, "SetFileSelection")
  setFilePathId <- paste0(id, "SetFilePath")
  setFileSelectionDivId <- paste0(id, "SetFileSelectionDiv")
  setFileSelectionOnlineId <- paste0(id, "SetFileSelectionOnline")
  setFileSelectionOnlineDivId <- paste0(id, "SetFileSelectionOnlineDiv")
  
  # **** UI ====
  # Hide/show upload buttons based on whether BIOMEX is offline or online
  observeEvent(global$online, {
    # I need to hide the selection div because the spinner causes problems otherwise
    shinyjs::hide(setFileSelectionDivId)
    shinyjs::hide(setFileSelectionOnlineDivId)
    
    if (global$online == TRUE)
    {
      shinyjs::show(setFileSelectionOnlineDivId)
    }
    else if (global$online == FALSE)
    {
      shinyjs::show(setFileSelectionDivId)
    }
  })
    
  # Reset the upload button when performing the pretreatment
  observeEvent(global$pretreatmentTrigger, {
    shinyjs::reset(setFileSelectionOnlineId)
  })
  
  # Update widgets when switching experiments or when pressing the pretreatment button ====
  # Force the update after pretreatment with performedPretreatment
  observeEvent(c(global$pretreatmentFinalSettings, global[[globalFinalSettingsId]]), {
    # If the matrices are not computed yet, do nothing
    if (is.null(global$dataMatrixNormalized) || is.null(global$metadataMatrix)) return (NULL)

    # Set selection
    updateRadioButtons(session, setSelectionTypeId, selected = global[[globalFinalSettingsId]]$setSelected)
    updateCheckboxGroupInput(session, keggSelectionId, selected = global[[globalFinalSettingsId]]$keggSelected)
  })
    
   
  # **** Assign to global variables ====
  # Set selection
  observeEvent(input[[setSelectionTypeId]], {
    global[[globalSettingsId]]$setSelected <- input[[setSelectionTypeId]]
  })
  
  observeEvent(input[[keggSelectionId]], {
    global[[globalSettingsId]]$keggSelected <- biomex.validateCheckboxGroupInput(value = input[[keggSelectionId]])
  }, ignoreNULL = FALSE)
  
  # Set selection custom path offline
  observeEvent(input[[setFileSelectionId]], {
    filePath <- biomex.chooseFile()
    
    if (is.null(filePath))
      return (NULL)
    
    global[[globalSettingsId]]$setSelectedCustomPath <- filePath
  })
  
  # Set selection custom online
  observeEvent(input[[setFileSelectionOnlineId]], {
    file <- input[[setFileSelectionOnlineId]]
    global[[globalSettingsId]]$setSelectedCustomPath <- file$datapath
  })
    
  # **** Render paths ====
  output[[setFilePathId]] <- shiny::renderDataTable({
    if (is.null(global[[globalSettingsId]]$setSelectedCustomPath)) return (data.table(Path = "No file selected."))

    data.table(Path = global[[globalSettingsId]]$setSelectedCustomPath)
  }, options = list(scrollX = TRUE, paging = FALSE, searching = FALSE, info = FALSE, autoWidth = FALSE), callback = JS(paste0('function(settings) { $("#', setFilePathId, ' thead").remove(); }')))
}
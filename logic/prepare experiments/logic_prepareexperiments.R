#' The logic of the page of the "Prepare experiments" menu. Here the user will be able to prepare and annotate the uploaded CSV files, and then
#' the data will be prepared in such a way that it will be ready for downstream analyses.
#' 
#' @param input The shiny input object.
#' @param output The shiny output object. 
#' @param session The shiny session object.
#' @param global The reactive list of global variables.
logicPrepareExperiments <- function(input, output, session, global)
{
  # **** UI ====
  # Show/hide ====
   observeEvent(c(global$mainDirectory, global$online), {
    shinyjs::hide("csvFilesSelectionDiv")
    shinyjs::hide("csvFilesSelectionOnlineDiv")
    
    if (!is.null(global$mainDirectory))
    {
      if (global$online == TRUE) 
      {
        shinyjs::show("csvFilesSelectionOnlineDiv")
        shinyjs::hide("outputFolderDiv")
      }
      
      if (global$online == FALSE) 
      {
        shinyjs::show("csvFilesSelectionDiv")
        shinyjs::show("outputFolderDiv")
      }
      
      updateDirectoryInput(session, "mainDirectory", value = global$mainDirectory)
    }
  }, ignoreNULL = FALSE)
  
  observeEvent(global$performedSelectDataCSV, {
    shinyjs::hide("csvFilesDiv")
    
    if (global$performedSelectDataCSV == TRUE) shinyjs::show("csvFilesDiv")
  })
  
  # Output directory widget ====
  observeEvent(input$mainDirectory, {
    directory <- biomex.chooseDir()
    
    if (is.null(directory)) return (NULL)
    
    global$mainDirectory <- file.path(directory, "BIOMEX")
  }, ignoreInit = TRUE)
  
  # Data button ====
  # It's a render UI because we want to switch colors when the data is uploaded
  output$selectDataCSV <- renderUI({
    if (global$performedSelectDataCSV == FALSE)
      actionButton("csvFilesData", tags$b("Upload the CSV data files"), icon = icon("alert"), width = "100%", style = "color: #fff; background-color: #CE2929; border-color: #C42525")
    else
      actionButton("csvFilesData", tags$b("Upload the CSV data files"), icon = icon("ok"), width = "100%", style="color: #fff; background-color: #09A325; border-color: #04931E")
  })
  
  # Metadata button ====
  # It's a render UI because we want to switch colors when the data is uploaded
  output$selectMetadataCSV <- renderUI({
    if (global$performedSelectMetadataCSV == FALSE)
      actionButton("csvFilesMetdata",  tags$b("Upload the CSV metadata files"), icon = icon("alert"),  width = "100%", style="color: #fff; background-color: #CE2929; border-color: #C42525")
    else
      actionButton("csvFilesMetdata",  tags$b("Upload the CSV metadata files"), icon = icon("ok"), width = "100%", style="color: #fff; background-color: #09A325; border-color: #04931E")
  })

  # Setting up CSV table offline ====
  # Setting up the table that will be filled-out by the user
  observeEvent(input$csvFilesData, {
    files <- biomex.chooseFiles()
    
    if (is.null(files)) return (NULL)
    
    names <- basename(files)
    names <- biomex.removeExtension(names)
    
    global$csvDataLocation <- data.table(File = basename(files), Path = files)  
    global$csvDataAnnotation <- data.table(Name = rep("", length(names)), Type = "", Technology = "", Data = "", Organism = "", Identifier = "", Metadata = "")
    global$performedSelectDataCSV <- TRUE
  })
  
  # Handling metadata offline ====
  observeEvent(input$csvFilesMetdata, {
    files <- biomex.chooseFiles()
    
    if (is.null(files)) return (NULL)
    
    names <- basename(files)
    names <- biomex.removeExtension(names)
    
    global$csvMetadataLocation <- data.table(File = basename(files), Path = files)
    global$performedSelectMetadataCSV <- TRUE
  })
  
  # Setting up CSV table online ====
  # Setting up the table that will be filled-out by the user
  observeEvent(input$csvFilesDataOnline, {
    files <- input$csvFilesDataOnline
    
    if (is.null(files)) return (NULL)
    
    names <- files$name
    names <- biomex.removeExtension(names)
    
    global$csvDataLocation <- data.table(File = files$name, Path = files$datapath)   
    global$csvDataAnnotation <- data.table(Name = rep("", length(names)), Type = "", Technology = "", Data = "", Organism = "", Identifier = "", Metadata = "")
    global$performedSelectDataCSV <- TRUE
  })
  
  # Handling metadata online ====
  observeEvent(input$csvFilesMetdataOnline, {
    files <- input$csvFilesMetdataOnline
    
    if (is.null(files)) return (NULL)
    
    names <- files$name
    names <- biomex.removeExtension(names)
    
    global$csvMetadataLocation <- data.table(File = files$name, Path = files$datapath)
    global$performedSelectMetadataCSV <- TRUE
  })
  
  # **** Plots and tables ====
  # Table ====
  output$csvDataAnnotationOutput <- renderUI({
    input$resetAnnotationTable # This resets the table elements (for example, stuck elements and cells)
    
    rHandsontableOutput("csvDataAnnotation")
  })
  
  output$csvDataAnnotation <- renderRHandsontable({
    input$resetAnnotationTable # This resets the table content
    
    if (!is.null(global$csvDataAnnotation)) 
    {
      file <- global$csvDataLocation$File
      type <- c("Transcriptomics", "Metabolomics", "Proteomics", "Cytometry")
      technology <- c("Micro-array", "RNA-seq", "scRNA-seq", "Mass spectrometry")
      data <- c("Raw", "Normalized", "Corrected")
      organism <- c("Human", "Mouse", "Rat", "Cow", "Zebrafish", "Pig", "Crab-eating macaque")
      identifier <- c(clusterProfiler::idType("org.Hs.eg.db"), "HMDB", "KEGG", "METLIN", "NAMES")
      metadata <- c("", "None", global$csvMetadataLocation$File)
      
      df <- global$csvDataAnnotation
      rhandsontable(df, rowHeaders = file, stretchH = "all", rowHeaderWidth = 250, 
                    fillHandle = list(direction = 'vertical', autoInsertRow = FALSE), maxRows  = nrow(df)) %>%
        hot_rows(rowHeights = 25, fixedRowsTop = 1) %>%
        hot_cols(columnSorting = FALSE, manualColumnResize = FALSE, halign = "htCenter") %>%
        hot_col(col = "Type",  type = "dropdown", source = type, strict = TRUE, allowInvalid = FALSE) %>%
        hot_col(col = "Technology",  type = "dropdown", source = technology, strict = TRUE, allowInvalid = FALSE) %>%
        hot_col(col = "Data",  type = "dropdown", source = data, strict = TRUE, allowInvalid = FALSE) %>%
        hot_col(col = "Organism",  type = "dropdown", source = organism, strict = TRUE, allowInvalid = FALSE) %>%
        hot_col(col = "Identifier",  type = "dropdown", source = identifier, strict = TRUE, allowInvalid = FALSE) %>%
        hot_col(col = "Metadata",  type = "dropdown", source = metadata, strict = TRUE, allowInvalid = FALSE) %>%
        hot_context_menu(allowRowEdit = FALSE, allowColEdit = FALSE)
    }
  })
  
  # **** Perform data processing ====
  observeEvent(input$prepareDataProcessing, {
    # Set up progress bar
    progress <- biomex.createProgressObject(message = "Processing experiments...", style = global$progressStyle)
    on.exit(progress$close())
    
    # Call garbage collector
    biomex.callGC()
    
    # Before starting, reset the downstream global variables
    biomex.resetGlobalVariables(global = global, original = globalOriginal, variables = c(globalAllExceptCommonVariables, "experiments", "selectedExperiment"))
    
    # Get annotation table and other variables
    csvDataAnnotation <- as.data.table(hot_to_r(input$csvDataAnnotation))
    csvDataLocation <- global$csvDataLocation
    csvMetadataLocation <- global$csvMetadataLocation
    
    # Combine all the possible selections together (obsolete now since we only use CSV files)
    combinedAnnotation <- biomex.combineDataSelection(csvDataAnnotation = csvDataAnnotation, csvDataLocation = csvDataLocation)

    # Check if the annotations are consistent and do not create conflicts
    errorMessage <- biomex.validateCombinedAnnotation(combinedAnnotation = combinedAnnotation, mainDirectory = global$mainDirectory)
    
    # If there were errors, let the user know and exit from the event
    if (!is.null(errorMessage))
    {
      js$alertMessage(errorMessage)
      return (NULL)
    }
    
    # Create the BIOMEX output folder
    biomex.createOutputDirectory(combinedAnnotation = combinedAnnotation, csvDataLocation = csvDataLocation, mainDirectory = global$mainDirectory)
    
    # Obtain the experiments' name
    experiments <- combinedAnnotation$Name
    
    # Loop over all the experiments and process them
    for (experiment in experiments)
    {
      print(paste0("Processing: ", experiment))
      
      # Get the information about the experiment
      row <- combinedAnnotation[Name == experiment]
      experimentInformation <- as.list(row)
      experimentInformation$OrganismAnnotation <- biomex.getOrganismAnnotation(organism = experimentInformation$Organism)
      experimentInformation$PerformedBatchCorrection <- "No"

      # Read the data matrix
      progress$set(message = paste0("Processing ", experiment), detail = "Reading data", value = 1)
      resultList <- biomex.getDataMatrix(experimentInformation = experimentInformation, csvDataLocation = csvDataLocation)
      
      # Error message if the data matrix was not read correctly
      if (!is.null(resultList$errorMessage))
      {
        js$alertMessage(paste0(experiment, ": ", resultList$errorMessage))
        return (NULL)
      }
      
      dataMatrix <- resultList$dataMatrix
      
      # Remove unwanted variables
      biomex.removeVariables(variables = "resultList")
      
      # Read the metadata matrix
      progress$set(message = paste0("Processing ", experiment), detail = "Reading metadata", value = 1)
      resultList <- biomex.getMetadataMatrix(experimentInformation = experimentInformation, csvMetadataLocation = csvMetadataLocation)
      
      # Error message if the metadata matrix was not read correctly
      if (!is.null(resultList$errorMessage))
      {
        js$alertMessage(paste0(experiment, ": ", resultList$errorMessage))
        return (NULL)
      }
      
      metadataMatrix <- resultList$metadataMatrix
      
      # Remove unwanted variables
      biomex.removeVariables(variables = "resultList")
      
      # Check consistency between the data type and the data values.
      progress$set(message = paste0("Processing ", experiment), detail = "Checking negative values", value = 1)
      resultList <- biomex.checkNegativeProcessing(dataMatrix = dataMatrix, experimentInformation = experimentInformation)
         
      # Error message if the check is not passed
      if (!is.null(resultList$errorMessage))
      {
        js$alertMessage(paste0(experiment, ": ", resultList$errorMessage))
        return (NULL)
      }
      
      # Remove unwanted variables
      biomex.removeVariables(variables = "resultList")
      
      # Clean data matrix
      progress$set(message = paste0("Processing ", experiment), detail = "Cleaning data", value = 1)
      resultList <- biomex.cleanDataMatrix(dataMatrix = dataMatrix, experimentInformation = experimentInformation) # This function cleans the data matrix by reference
      dataMatrix <- resultList$dataMatrix
      
      # Save information
      processingInformation <- list()
      processingInformation$featuresNumber <- resultList$featuresNumber
      processingInformation$featuresRemoved <- resultList$featuresRemoved
      processingInformation$featuresAggregated <- resultList$featuresAggregated
      processingInformation$observationsNumber <- resultList$observationsNumber 
      processingInformation$observationsRemoved <- resultList$observationsRemoved
      
      # Remove unwanted variables
      biomex.removeVariables(variables = "resultList")
      
      # Check consistency between data and metadata
      progress$set(message = paste0("Processing ", experiment), detail = "Checking data and metadata", value = 1)
      resultList <- biomex.checkDataMetadataConsistency(dataMatrix = dataMatrix, metadataMatrix = metadataMatrix, experimentInformation = experimentInformation)
      metadataMatrix <- resultList$metadataMatrix
      
      # Save information
      processingInformation$metadataRemoved <- resultList$removedObservations
      processingInformation$metadataAdded <- resultList$addedObservations
      
      # Remove unwanted variables
      biomex.removeVariables(variables = "resultList")
      
      # Create mapping table
      progress$set(message = paste0("Processing ", experiment), detail = "Creating mapping table", value = 1)
      resultList <- biomex.createMappingTable(dataMatrix = dataMatrix, experimentInformation = experimentInformation) # This function modifies the data matrix by reference
      
      # Check for errors
      if (!is.null(resultList$errorMessage))
      {
        js$alertMessage(paste0(experiment, ": ", resultList$errorMessage))
        return (NULL)
      }
      
      dataMatrix <- resultList$dataMatrix
      mappingTable <- resultList$mappingTable
      experimentInformation <- resultList$experimentInformation

      # Save information
      processingInformation$featuresRemovedMapping <- resultList$featuresRemoved
      processingInformation$featuresAggregatedMapping <- resultList$featuresAggregated
      processingInformation$featuresMadeUniqueMapping <- resultList$featureMadeUnique
      
      # Remove unwanted variables
      biomex.removeVariables(variables = "resultList")
      
      # Annotate mitochondrial genes (Transcriptomics and Proteomics only)
      if (experimentInformation$Type %in% c("Transcriptomics", "Proteomics", "Cytometry"))
      {
        progress$set(message = paste0("Processing ", experiment), detail = "Annotating mitochondrial features", value = 1)
        resultList <- biomex.findMitochondrialGenes(mappingTable = mappingTable, experimentInformation = experimentInformation)
        mappingTable <- resultList$mappingTable
        
        # Save information
        processingInformation$mitochondrialAnnotated <- resultList$annotatedGenes
      }
      
      # Assign variables to the global variables
      globalTemp <- reactiveValues()
      globalTemp$dataMatrixOriginal <- dataMatrix
      globalTemp$metadataMatrix <- metadataMatrix
      globalTemp$metadataMatrixOriginal <- metadataMatrix
      globalTemp$experimentInformation <- experimentInformation
      globalTemp$mappingTable <- mappingTable
      globalTemp$processingInformation <- processingInformation
      globalTemp$performedProcessing <- TRUE
      
      # Remove unwanted variables
      biomex.removeVariables(variables = c("dataMatrix", "metadataMatrix", "experimentInformation", "mappingTable", "processingInformation"))
      
      # Save variables to file
      progress$set(message = paste0("Processing ", experiment), detail = "Saving variables", value = 1)
      biomex.saveVariables(global = globalTemp, variablesToSave = globalDataMatrixOriginal, filePath = file.path(global$mainDirectory, experiment, "DataOriginal.var"), compress = global$compress)
      biomex.saveVariables(global = globalTemp, variablesToSave = globalPrepareDataProcessingVariables, filePath = file.path(global$mainDirectory, experiment, "General.var"), compress = global$compress)
      
      # Remove unwanted variable
      biomex.removeVariables(variables = "globalTemp")
    }
    
    # Assign some variables
    global$combinedAnnotation <- combinedAnnotation
    global$experiments <- experiments
    
    # First experiment defaults to the selected experiment
    global$selectedExperiment <- experiments[1]
    global$switchingBehaviour <- "Skip"

    # Save common variables to file
    biomex.saveVariables(global = global, variablesToSave = globalCommonVariables, filePath = file.path(global$mainDirectory, "Common.var"), compress = global$compress)
    
    # Reset the upload buttons
    shinyjs::reset("csvFilesDataOnlineAdd")
    shinyjs::reset("csvFilesMetdataOnlineAdd")
    
    # Update the recently loaded experiments table only if BIOMEX is running locally
    if (global$online == FALSE)
    {
      resultList <- biomex.updateRecentExperimentsTable(recentExperimentsTable = global$recentExperimentsTable$data, experimentPath = global$mainDirectory, filePath = file.path(global$applicationDataDirectory, "experiments.csv"))
      
      if (!is.null(resultList$errorMessage))
      {
        js$alertMessage(resultList$errorMessage)
        return (NULL)
      }
      
      global$recentExperimentsTable <- resultList$recentExperimentsTable
    }
    
    # Perform some actions after the processing is over
    # It will be set to FALSE after the message is displayed (that code is located in logic_menu.R)
    global$performActionsAfterProcessing <- TRUE
    
    # Call garbage collector
    biomex.callGC()
  })
}
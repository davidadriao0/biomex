#' The logic of the Graph tab.
#' 
#' @param input The shiny input object.
#' @param output The shiny output object. 
#' @param session The shiny session object.
#' @param global The reactive list of global variables.
logicMetadataGraph <- function(input, output, session, global)
{
  # **** Design of experiment ====
  biomex.designOfExperimentLogic(input, output, session, global, id = "metadataGraph", 
    allowObservations = TRUE, allowFeatures = FALSE , allowSets = FALSE, allowAdditionalFiltering = FALSE, allowGrouping = FALSE)
  
  # **** UI ====
  # Feature input ====
  observeEvent(global$metadataMatrix, {
    choices <- c("Not selected", biomex.getMetadataNames(metadataMatrix = global$metadataMatrix, ignoreFirstColumn = TRUE))
    selected <- biomex.getValidChoice(choices = choices, selected = input$metadataGraphBarplotFeature)
    updateSelectInput(session, "metadataGraphBarplotFeature", choices = choices, selected = selected)
  })
  
  # Values to include in the barplot =====
  observeEvent(c(input$metadataGraphBarplotFeature, global$metadataGraphFinalSettings, global$metadataGraphDoeTrigger), {
    if (is.null(global$metadataGraphDoeResults)) return (NULL)
    
    # If the value is NULL, reset the widget
    if (is.null(input$metadataGraphBarplotFeature) || input$metadataGraphBarplotFeature == "" || input$metadataGraphBarplotFeature == "Not selected")
    {
      updateSelectizeInput(session, "metadataGraphBarplotValuesToInclude", choices = "", selected = "")
      return (NULL)
    }
    
    # If the data was subset in the Method settings, then use that subset. Otherwise use all the observations selected in the design of experiment.
    if (!is.null(global$metadataGraphResults$observations)) observations <- global$metadataGraphResults$observations
    else  observations <- global$metadataGraphDoeResults$observationsSelected
    
    # Get subset metadata matrix
    metadataMatrix <- biomex.getDataTableSubset(global$metadataMatrix, rows = observations)
    variables <- biomex.getMetadataNames(metadataMatrix = global$metadataMatrix, ignoreFirstColumn = TRUE)
    
    # Some conflict could happen when changing experiments, in that case do nothing
    if (!input$metadataGraphBarplotFeature %in% variables) return (NULL)
    
    # Update widget
    choices <- unique(metadataMatrix[[input$metadataGraphBarplotFeature]])
    updateSelectizeInput(session, "metadataGraphBarplotValuesToInclude", choices = choices, selected = "")
  })
  
  # Group selection ====
  # Update selectInput based on the metadata columns
  observeEvent(c(global$metadataMatrix, global$metadataGraphDoeResults, global$metadataGraphDoeTrigger), {
    if (is.null(global$metadataGraphDoeResults)) return (NULL)
    
    # Get the subset data matrix
    metadataMatrix <- biomex.getDataTableSubset(dataTable = global$metadataMatrix, rows = global$metadataGraphDoeResults$observationsSelected)
    
    # Update observationVariable
    choices <- biomex.getMetadataNames(metadataMatrix = metadataMatrix)
    selected <- biomex.getValidChoice(choices = choices, selected = global$metadataGraphFinalSettings$observationVariable)
    updateSelectInput(session, "metadataGraphObservationVariable", choices = choices, selected = selected)
    global$metadataGraphSettings$observationVariable <- selected # Force update because if observationVariable doesn't get invalidated, this element will never be updated
    
    # Update normalization factor
    choices <- c("Not selected", biomex.getMetadataNames(metadataMatrix = metadataMatrix))
    selected <- biomex.getValidChoice(choices = choices, selected = global$metadataGraphFinalSettings$normalizationFactor)
    updateSelectInput(session, "metadataGraphNormalizationFactor", choices = choices, selected = selected)
    global$metadataGraphSettings$normalizationFactor <- selected # Force update because if normalizationFactor doesn't get invalidated, this element will never be updated
    
    # Also update observationGroup when the metadata is changed
    if (selected == "") return (NULL) # HACK: FIX: CRASH: If observationVariable was not set, then stop here  
    selected <- global$metadataGraphSettings$observationVariable
    choices <- unique(metadataMatrix[[selected]])
    selected <- biomex.getValidChoice(choices = choices, selected = global$metadataGraphFinalSettings$observationGroup, defaultAsEmptyString = TRUE)
    updateSelectizeInput(session, "metadataGraphObservationGroup", choices = choices, selected = selected, server = TRUE)
    global$metadataGraphSettings$observationGroup <- selected # Force update because if observationGroup doesn't get invalidated, this element will never be updated
  })
  
  # Update the second selectInput based and the selection of observationVariable
  observeEvent(input$metadataGraphObservationVariable, {
    if (is.null(global$metadataMatrix) || is.null(global$metadataGraphDoeResults)) return (NULL)
    
    global$metadataGraphSettings$observationVariable <- input$metadataGraphObservationVariable
    
    # Get the subset data matrix
    metadataMatrix <- biomex.getDataTableSubset(dataTable = global$metadataMatrix, rows = global$metadataGraphDoeResults$observationsSelected)
    
    # Get the choices
    choices <- unique(metadataMatrix[[global$metadataGraphSettings$observationVariable]])
    
    # When you are switching experiment, you don't want to reset observationGroup, you want to display what it was
    if (global$switchingExperiment == TRUE || global$loadStoredResults == TRUE)
    {
      updateSelectizeInput(session, "metadataGraphObservationGroup", choices = choices, selected = global$metadataGraphFinalSettings$observationGroup, server = TRUE)
      return (NULL) # Don't do anything after this
    }
    
    updateSelectizeInput(session, "metadataGraphObservationGroup", choices = choices, selected = "", server = TRUE)
  })
  
  # **** Assign to global variables ====
  # Observation variable
  observeEvent(input$metadataGraphObservationVariable, {
    global$metadataGraphSettings$observationVariable <- biomex.validateSelectizeInputMultiple(value = input$metadataGraphObservationVariable)
  })
  
  # Normalization factor
  observeEvent(input$metadataGraphNormalizationFactor, {
    global$metadataGraphSettings$normalizationFactor <- biomex.validateSelectizeInputMultiple(value = input$metadataGraphNormalizationFactor)
  })
  
  # Observation group
  observeEvent(input$metadataGraphObservationGroup, {
    global$metadataGraphSettings$observationGroup <- biomex.validateSelectizeInputMultiple(value = input$metadataGraphObservationGroup)
  }, ignoreNULL = FALSE)
  
  # **** Perform data gathering ====
  observeEvent(c(input$updateMetadataGraph, input$metadataGraphBarplotFeature), {
    # If some variables are missing, do nothing
    if (is.null(global$dataMatrixNormalized) || is.null(global$metadataGraphDoeResults) || is.null(global$metadataMatrix)) return (NULL)
    
    # If the experiment is switching, do nothing
    if (global$switchingExperiment == TRUE) return (NULL)
    
    # Set up progress bar
    progress <- biomex.createProgressObject(message = "Performing metadata quantification...", style = global$progressStyle)
    on.exit(progress$close())
    
    # Parameters
    observationVariable <- global$metadataGraphSettings$observationVariable
    observationGroup <- global$metadataGraphSettings$observationGroup
    normalizationFactor <- global$metadataGraphSettings$normalizationFactor

    # Obtain graph data
    metadataGraphResults <- biomex.obtainDataForMetadataGraph(metadataMatrix = global$metadataMatrix, feature = observationVariable, barplotFeature = input$metadataGraphBarplotFeature,
      observationsOrder = observationGroup, groupVariable = observationVariable, normalizationFactor = normalizationFactor,
      observations = global$metadataGraphDoeResults$observationsSelected)
    
    # If the selectInput widget is empty, keep the current plot, do not reset it
    if (!is.null(metadataGraphResults)) global$metadataGraphResults <- metadataGraphResults
    
    # Save settings
    global$metadataGraphFinalSettings <- global$metadataGraphSettings
  })
    
   # **** Plots and tables ====
  output$metadataGraphPlot <- renderPlotly({
    # If there are no results, don't do anything
    if (is.null(global$metadataGraphResults))
    {
      global$metadataGraphPlot <- NULL
      return (biomex.blankPlot(text = "No results available."))
    }
    
    # Get results
    metadataMatrix <- global$metadataGraphResults$metadataMatrix
    
    # Get values
    featureValues <- metadataMatrix$Feature
    featureBarplotValues <- metadataMatrix$BarplotFeature
    feature <- global$metadataGraphResults$feature
    barplotFeature <- global$metadataGraphResults$barplotFeature
    observations <- global$metadataGraphResults$observations
    observationsOrder <- global$metadataGraphResults$observationsOrder
    observationVariable <- global$metadataGraphResults$observationVariable
    
    # Get information
    showPlots <- input$metadataGraphShowPlots
    showLegend <- input$metadataGraphShowLegend
    labelPosition <- input$metadataGraphPiechartLabelPosition
    labelText <- input$metadataGraphPiechartLabelText
    holeSize <- input$metadataGraphPiechartHoleSize
    piechartDirection <- input$metadataGraphPiechartDirection
    piechartSort <- input$metadataGraphPiechartSort
    barplotValuesToInclude <- biomex.validateSelectizeInputMultiple(value = input$metadataGraphBarplotValuesToInclude)
    scaleByNormalizationFactor <- input$metadataGraphScaleByNormalizationFactor
    divideByFactors <- input$metadataGraphBarplotDivideByFactors
    rowScale <- input$metadataGraphBarplotRowScale
    metadataLineWidth <- biomex.validateNumericInput(value = input$metadataGrapBarplotLineWidth, defaultTo = 1, asInteger = TRUE)
    plotColor <- input$metadataGraphColor
    barplotColor <- input$metadataGraphBarplotColor
    reverseColors <- input$metadataGraphReverseColors
    barplotReverseColors <- input$metadataGraphBarplotReverseColors
    customColors <- NULL
    predefinedColors <- NULL
    
    # Define correct order
    if (!"" %in% observationsOrder) featureValues <- factor(featureValues, levels = observationsOrder)
    else featureValues <- factor(featureValues, levels = unique(featureValues))
    
    # Initialize all plots
    pieChart <- NULL
    barPlot <- NULL
    
    # Piechart
    if ("Piechart" %in% showPlots)
    {
      # Get the custom colors and the predefined colors
      customColors <- biomex.getCustomColors(colorCustomTable = input$metadataGraphColorCustomTable)
      predefinedColors <- biomex.getPredefinedColors(metadataColors = global$metadataColors, name = observationVariable, values = featureValues, useFactorOrder = FALSE)
   
      # Get colors
      finalColors <- biomex.getColorPalette(palette = plotColor, number = length(unique(featureValues)), reverse = reverseColors, customColors = customColors, predefinedColors = predefinedColors)
      
      # Check colors validity
      if (biomex.areColors(colors = finalColors) == FALSE)  
      {
        global$metadataGraphPlot <- NULL
        return (biomex.blankPlot(text = "Invalid color detected."))
      }
      
      summaryData <- as.data.table(table(featureValues))
      summaryData$Color <- finalColors
      colnames(summaryData) <- c("Label", "Count", "Color")
      
      labelPosition <- tolower(labelPosition)
      if (labelText == "Only text") labelText <- "label"
      if (labelText == "Only percentage") labelText <- "percentage"
      if (labelText == "Text and percentage") labelText <- "label+percent"
      piechartDirection <- tolower(piechartDirection)
      
      # If there are too many factors, don't plot them
      if (nrow(summaryData) > global$plotLimit)
      {
        global$metadataGraphPlot <- NULL
        return (biomex.blankPlot(text = "Too many factors to create the plot."))
      }
      
      # Take only the element which count is above percentageThreshold of the total
      pieChart <- plot_ly(summaryData, labels = ~Label, values = ~Count, sort = piechartSort,
                          marker = list(colors = ~Color, line = list(color = '#FFFFFF', width = 0.5)),
                          textposition = labelPosition, textinfo = labelText, direction = piechartDirection, 
                          insidetextfont = list(color = '#FFFFFF')) %>%
                          add_pie(hole = holeSize) %>%
        layout(title = '', showlegend = showLegend,
        xaxis = list(showgrid = FALSE, zeroline = FALSE, showticklabels = FALSE),
        yaxis = list(showgrid = FALSE, zeroline = FALSE, showticklabels = FALSE))
      
      plotData <- summaryData
    }
    
    # Barplot
    if ("Barplot" %in% showPlots && !is.null(featureBarplotValues))
    {
      if (!"" %in% barplotValuesToInclude)
      {
        index <- which(featureBarplotValues %in% barplotValuesToInclude)
        
        # If the index is empty, do nothing
        if (length(index) == 0)
        {
          global$metadataGraphPlot <- NULL
          return (biomex.blankPlot(text = "The values to include are not present."))
        }
        
        metadataMatrix <- metadataMatrix[index]
        featureValues <- featureValues[index]
        featureBarplotValues <- featureBarplotValues[index]
        
        # Order the values by the barplotValuesToInclude order
        index <- order(match(featureBarplotValues, barplotValuesToInclude))
        metadataMatrix <- metadataMatrix[index]
        featureValues <- featureValues[index]
        featureBarplotValues <- featureBarplotValues[index]
      }
      
      countTable <- as.data.table(table(featureValues, featureBarplotValues))
      countTable <- dcast(countTable, featureValues ~ featureBarplotValues, value.var = "N")
      colnames(countTable)[1] <- "Variable"
      if (!"" %in% barplotValuesToInclude) countTable <- countTable[, c("Variable", barplotValuesToInclude), with = FALSE]
      
      # If there are too many factors, don't plot them
      if (nrow(countTable) > global$plotLimit || (ncol(countTable) - 1) > global$plotLimit)
      {
        global$metadataGraphPlot <- NULL
      return (biomex.blankPlot(text = "Too many factors to create the plot."))
      }
      
      # Whether or not to normalize the data based on the user selected normalization factor
      if (scaleByNormalizationFactor == TRUE && !is.null(metadataMatrix$NormalizationFactor))
      {
        # Create a temp table, which will be modified/overridden step by step
        tempTable <- countTable
        variables <- countTable$Variable
        factors <- colnames(countTable)[-1]
        
        # Conver columns to double
        for (factor in factors) tempTable[[factor]] <- as.double(tempTable[[factor]])
        
        # Loop over all the variables
        for (variable in variables)
        {
          # Loop over all the factors
          for (factor in factors)
          {
            # Get the data that has only the current variable and factor
            normalizationMetadata <- metadataMatrix[Feature == variable & BarplotFeature == factor]
            
            # If no metadata is present for that combionation of variable and factor, set it to 0 and move on
            if (nrow(normalizationMetadata) == 0) 
            {
              tempTable[Variable == variable][[factor]] <- 0
              next
            }
            
            # Obtain the count of each normalization factor per factor
            normalizationTable <- as.data.table(table(normalizationMetadata$NormalizationFactor, normalizationMetadata$BarplotFeature))
            normalizationTable <- dcast(normalizationTable, V1 ~ V2, value.var = "N")
            colnames(normalizationTable)[1] <- "Variable"
            
            # Loop over the normalization factors, divide the current value by the total
            normalizationFactors <- normalizationTable$Variable
            normalizedValues <- c()
            for (normalizationFactor in normalizationFactors)
            {
              value <- normalizationTable[Variable == normalizationFactor][[factor]]
              total <- sum(metadataMatrix$NormalizationFactor == normalizationFactor)
              normalizedValue <- value / total
              if (is.na(normalizedValue)) normalizedValue <- 0
              normalizedValues <- c(normalizedValues, normalizedValue)
            }
            
            # Add the sum of the normalized values to the correct cell
            tempTable[Variable == variable][[factor]] <- sum(normalizedValues)
          }
        }
        
        # Override the count table with the new normalized table
        countTable <- tempTable
      }
      
      # Divide by the sum of the factors (column wise)
      if (divideByFactors == TRUE)
      {
        columnSum <- apply(countTable[, -1], 2, sum)
        columnNames <- colnames(countTable)
        firstColumn <- countTable[[1]]
        countTable <- t(t(countTable[, -1]) / columnSum)
        countTable <- cbind(firstColumn, as.data.table(countTable))
        colnames(countTable) <- columnNames
      }
      
      # Scale or do not scale data based on the user choice
      if (rowScale == TRUE)
      {
        variables <- countTable$Variable
        percentages <- apply(countTable[, -1], 1, function(x) { x <- x / sum(x) })
        
        # When the result is a vector (it means there was only one value) when need to handle it a bit differently
        if (class(percentages) == "numeric") 
        {
          name <- unique(featureBarplotValues)
          percentages <- t(matrix(percentages))
          rownames(percentages) <- name
        }
        
        countTable <- as.data.table(t(percentages))
        countTable <- cbind(Variable = variables, countTable)
      }
      
      # For the ordering we want the first selection to be at the top, so we need to reverse it
      # WARNING: it gives a warning when you use the subset by metadata values option (it's fine)
      if (!"" %in% observationsOrder) countTable$Variable <- factor(countTable$Variable, levels = rev(observationsOrder))
      
      # Get the custom colors and the predefined colors
      customColors <- biomex.getCustomColors(colorCustomTable = input$metadataGraphBarplotColorCustomTable)
      predefinedColors <- biomex.getPredefinedColors(metadataColors = global$metadataColors, name = barplotFeature, values = featureBarplotValues, useFactorOrder = FALSE)
   
      # Get colors
      barplotColors <- biomex.getColorPalette(palette = barplotColor, number = length(unique(featureBarplotValues)), reverse = barplotReverseColors, customColors = customColors, predefinedColors = predefinedColors)
      
      # Check colors validity
      if (biomex.areColors(colors = barplotColors) == FALSE)
      {
        global$metadataGraphPlot <- NULL
        return (biomex.blankPlot(text = "Invalid color detected."))
      }
      
      barPlot <- plot_ly(x = countTable[[2]], y = countTable$Variable, name = colnames(countTable)[2], type = "bar", orientation = "h", 
                              marker = list(color = barplotColors[1], line = list(color = 'rgb(248, 248, 249)', width = metadataLineWidth)), showlegend = showLegend)
      
      if (ncol(countTable) > 2)
      {
        for (i in 3:ncol(countTable)) barPlot <- add_trace(barPlot, x = countTable[[i]], name = colnames(countTable)[i], marker = list(color = barplotColors[i - 1]))
      }
      
      barPlot <- barPlot %>% layout(barmode = "stack")
      
      # Match ordering (for when downloading the file, you want the file in the correct order)
      if (!"" %in% observationsOrder) countTable <- countTable[match(observationsOrder, countTable$Variable)]
      colorsTable <- data.table(Variable = "Color", t(barplotColors))
      colnames(colorsTable) <- colnames(countTable)
      plotData <- rbind(countTable, colorsTable)
    }
    else
    {
      plotData <- data.table(Information = "No feature selected.")
      barPlot <- biomex.blankPlot(text = "No feature selected.")
    }
    
    # Combine plots together as needed
    subPlots <- list()
    if ("Piechart" %in% showPlots && !is.null(pieChart)) subPlots <- append(subPlots, list(pieChart))
    if ("Barplot" %in% showPlots && !is.null(barPlot)) subPlots <- append(subPlots, list(barPlot))
  
    if (length(subPlots) > 0)
      p <- plotly::subplot(subPlots, nrows = length(subPlots), margin = c(0.02, 0.02, 0.03, 0.03), shareX = FALSE)
    else
    {
      global$metadataGraphPlot <- NULL
      return (biomex.blankPlot(text = "No plot type is selected."))
    }
    
    global$metadataGraphPlotData <- plotData
    global$metadataGraphPlot <- p
    
    if (global$doNotUseWebGL == TRUE) p else toWebGL(p)
  })
  
  # Custom colors table ====
  output$metadataGraphColorCustomTable <- renderRHandsontable({
    if (is.null(global$metadataGraphResults$metadataMatrix)) return (NULL)
    
    # Get results
    values <- unique(global$metadataGraphResults$metadataMatrix$Feature)
    observationGroup <- global$metadataGraphFinalSettings$observationGroup
    if (!"" %in% observationGroup) values <- observationGroup
    
    # Create the data frame
    df <- data.table(Value = values, Color = rep("black", length(values)))    
    
    # If there are too many rows, don't create the table
    if (nrow(df) > global$tableLimit) return (biomex.blankHandsontable(text = "Too many factors to create the table.", header = "Warning"))
    
    # Create rhandsontable
    rhandsontable(df[, -"Value"], rowHeaders = df$Value, stretchH = "all", rowHeaderWidth = 250, 
                  fillHandle = list(direction = 'vertical', autoInsertRow = FALSE), maxRows  = nrow(df)) %>%
      hot_rows(rowHeights = 25, fixedRowsTop = 1) %>%
      hot_cols(columnSorting = FALSE, manualColumnResize = FALSE, halign = "htCenter") %>%
      hot_context_menu(allowRowEdit = FALSE, allowColEdit = FALSE)
  })
  
  # Custom metadata colors table ====
  output$metadataGraphBarplotColorCustomTable <- renderRHandsontable({
    if (is.null(global$metadataGraphResults$metadataMatrix)) return (NULL)
    
    # Get values
    metadataMatrix <- global$metadataGraphResults$metadataMatrix
    barplotValuesToInclude <- biomex.validateSelectizeInputMultiple(value = input$metadataGraphBarplotValuesToInclude)
    
    # If the barplot feature was not selected, do nothing
    if (is.null(metadataMatrix$BarplotFeature)) return (NULL)
    
    # If the metadata was subset using then metadata values, then we need to reflect it here
    if (!"" %in% barplotValuesToInclude) 
    {
      index <- which(metadataMatrix$BarplotFeature %in% barplotValuesToInclude)
        
      # If the index is empty, do nothing
      if (length(index) == 0) return (NULL)
        
      metadataMatrix <- metadataMatrix[index]
              
      # Order the values by the barplotValuesToInclude order
      index <- order(match(metadataMatrix$BarplotFeature, barplotValuesToInclude))
      metadataMatrix <- metadataMatrix[index]
    }
    
    # Get metadata values
    featureBarplotValues <- unique(metadataMatrix$BarplotFeature)
    
    # If the order is not set by the user, use the default factor ordering
    if ("" %in% barplotValuesToInclude) values <- levels(factor(featureBarplotValues))
    else values <- barplotValuesToInclude
    
    # Create the data frame
    df <- data.table(Value = values, Color = rep("black", length(values)))
        
    # If there are too many rows, don't create the table
    if (nrow(df) > global$tableLimit) return (biomex.blankHandsontable(text = "Too many factors to create the table.", header = "Warning"))
    
    # Create rhandsontable
    rhandsontable(df[, -"Value"], rowHeaders = df$Value, stretchH = "all", rowHeaderWidth = 250, 
                  fillHandle = list(direction = 'vertical', autoInsertRow = FALSE), maxRows  = nrow(df)) %>%
      hot_rows(rowHeights = 25, fixedRowsTop = 1) %>%
      hot_cols(columnSorting = FALSE, manualColumnResize = FALSE, halign = "htCenter") %>%
      hot_context_menu(allowRowEdit = FALSE, allowColEdit = FALSE)
  })
    
  # **** Export ====
  # Plot export ====
  observeEvent(input$exportMetadataGraphPlot, {
    if (is.null(global$metadataGraphPlot))
    {
      js$alertMessage("No results available to export.")
      return (NULL)
    }
    
    global$exportPlot <- global$metadataGraphPlot
    
    # Open modal
    toggleModal(session, "exportPlotModal", toggle = "open")
  })
  
  # Table export ====
  observeEvent(input$exportMetadataGraphTable, {
    if (is.null(global$metadataGraphResults))
    {
      js$alertMessage("No results available to export.")
      return (NULL)
    }
    
    # Get results
    global$exportTable <- global$metadataGraphResults$metadataMatrix
    
    # Open modal
    toggleModal(session, "exportTableModal", toggle = "open")
  })
   
  # Values export ====
  observeEvent(input$exportMetadataGraphValues, {
    if (is.null(global$metadataGraphPlotData))
    {
      js$alertMessage("No results available to export.")
      return (NULL)
    }
    
    # Get results
    global$exportTable <- global$metadataGraphPlotData
    
    # Open modal
    toggleModal(session, "exportTableModal", toggle = "open")
  })
}
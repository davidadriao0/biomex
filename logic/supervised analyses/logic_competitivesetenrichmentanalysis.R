#' The logic of the set enrichment analysis tab.
#' 
#' @param input The shiny input object.
#' @param output The shiny output object. 
#' @param session The shiny session object.
#' @param global The reactive list of global variables.
logicCompetitiveSetEnrichmentAnalysis <- function(input, output, session, global)
{
  # **** Design of experiment ====
  biomex.designOfExperimentLogic(input, output, session, global, id = "csea", 
    allowObservations = FALSE, allowFeatures = FALSE , allowSets = TRUE, allowAdditionalFiltering = FALSE, allowGrouping = FALSE)
  
  # **** Store and load results ====
  biomex.storeAndLoadResultsLogic(input, output, session, global, id = "csea")
  
  # **** UI ====
  # Update widgets when switching experiments and when performing the analysis ====
  observeEvent(c(global$cseaFinalSettings, global$cseaDoeTrigger), {
    # Method settings
    updateSelectInput(session, "cseaFeaturesToUse", selected = global$cseaFinalSettings$featuresToUse)
    updateSelectInput(session, "cseaMinimumSetSize", selected = global$cseaFinalSettings$minimumSetSize)
    updateSelectInput(session, "cseaPermutations", selected = global$cseaFinalSettings$permutations)
    updateSelectInput(session, "cseaRandomSeed", selected = global$cseaFinalSettings$randomSeed)
  })
 
  # **** Assign to global variables ====
  # Method settings
  observeEvent(input$cseaFeaturesToUse, {
    global$cseaSettings$featuresToUse <- input$cseaFeaturesToUse
  })
  
  observeEvent(input$cseaMinimumSetSize, {
    global$cseaSettings$minimumSetSize <- biomex.validateNumericInput(value = input$cseaMinimumSetSize, defaultTo = globalOriginal[["cseaSettings"]][["minimumSetSize"]], min = 2)
  })
  
  observeEvent(input$cseaPermutations, {
    global$cseaSettings$permutations <- biomex.validateNumericInput(value = input$cseaPermutations, defaultTo = globalOriginal[["cseaSettings"]][["permutations"]], min = 2)
  })
  
  observeEvent(input$cseaRandomSeed, {
    global$cseaSettings$randomSeed <- biomex.validateNumericInput(value = input$cseaRandomSeed, defaultTo = globalOriginal[["cseaSettings"]][["randomSeed"]])
  })
  
  # Modals ====
  observeEvent(input$cseaEnrichment, {
    selectedIndex <- input$cseaTable_rows_selected
    
    # If there are no results, don't do anything
    if (is.null(global$cseaResults) || is.null(selectedIndex)) 
    {
      js$alertMessage("You need to click on the set to visualize first.")
      return (NULL)
    }
    
    toggleModal(session, "cseaEnrichmentModal", toggle = "open")
  })
  
  observeEvent(input$cseaShowSetContent, {
    selectedIndex <- input$cseaTable_rows_selected
    
    # If there are no results, don't do anything
    if (is.null(global$cseaResults) || is.null(selectedIndex)) 
    {
      js$alertMessage("You need to click on the set to visualize first.")
      return (NULL)
    }
        
    toggleModal(session, "cseaSetContentModal", toggle = "open")
  })
   
  # **** Perform competivie set enrichment analysis ====
  observeEvent(input$updateCompetitiveSetEnrichmentAnalysis, {
    if (is.null(global$cseaDoeResults)) return (NULL)
    
    # If daResults is NULL, do nothing
    if (is.null(global$daResults))    
    {
      js$alertMessage("You need to perform the differential analysis first.")
      return (NULL)
    }
    
    # Cannot perform CSEA when the matrix used in the differential analysis was the Engineered data matrix
    if (global$daFinalSettings$dataMatrixAsInput == "Engineered")
    {
      js$alertMessage("You cannot perform this analysis when the differential analysis is performed on the engineered data matrix.")
      return (NULL)
    }
    
    # Set up progress bar
    progress <- biomex.createProgressObject(message = "Performing competitive set enrichment analysis...", style = global$progressStyle)
    on.exit(progress$close())
                
    # Call garbage collector
    biomex.callGC()
    
    # Get the right data matrix
    progress$set(message = "Performing competitive set enrichment analysis...", detail = "Preparing data", value = 1)
    differentialExpression <- global$daResults$differentialExpression
    
    if (global$cseaSettings$featuresToUse == "Significant (p-value)") differentialExpression <- differentialExpression[Pvalue < 0.05]
    if (global$cseaSettings$featuresToUse == "Significant (adjusted p-value)") differentialExpression <- differentialExpression[`Adjusted pvalue` < 0.05]
    
    if (nrow(differentialExpression) == 0) 
    {
      js$alertMessage("Not enough features.")
      global$cseaResults <- NULL
      global$cseaPlot <- NULL
      global$cseaEnrichmentPlot <- NULL
      return (NULL)
    }
    
    # Perform competitive set enrichment analysis
    progress$set(message = "Performing competitive set enrichment analysis...", detail = "Calculating enrichment", value = 1)
    global$cseaResults <- biomex.performCompetitiveSetEnrichment(differentialExpression = differentialExpression,
      mappingTable = global$mappingTable, featureSets = global$cseaDoeResults$featureSets, minimumSetSize = global$cseaSettings$minimumSetSize, 
      permutations = global$cseaSettings$permutations, randomSeed = global$cseaSettings$randomSeed)
    
    # If it fails, stop and reset results
    if (!is.null(global$cseaResults$errorMessage)) 
    {
      js$alertMessage(global$cseaResults$errorMessage)
      global$cseaResults <- NULL
      global$cseaPlot <- NULL
      global$cseaEnrichmentPlot <- NULL
      return (NULL)
    }
    
    # Remove unwanted variables
    biomex.removeVariables(variables = "differentialExpression")
    
    # Save settings
    global$cseaFinalSettings <- global$cseaSettings
                
    # Call garbage collector
    biomex.callGC()
  })
  
  # **** Save results for the meta analysis ====
  observeEvent(input$cseaMetaAnalysisSave, {
    name <- input$cseaMetaAnalysisName
    resultNames <- names(global$metaAnalysisData)
    table <- global$cseaResults$enrichmentTable
    
    # You cannot save the result if there are no results
    if (is.null(table))
    {
      js$alertMessage("No results available to save.")
      return (NULL)
    }
    
    # If the name is empty, do nothing
    if (name == "")
    {
      js$alertMessage("You need to specify a name.")
      return (NULL)
    }
    
    # If the name is already present, do nothing
    if (name %in% resultNames)
    {
      js$alertMessage("This name already exists.")
      return (NULL)
    }
    
    progress <- biomex.createProgressObject(message = "Saving result...", style = global$progressStyle)
    on.exit(progress$close())
    
    # Save enrichment table for the meta analysis
    maLength <- length(global$metaAnalysisData)
    if (maLength == 0) maLength <- 1
    else maLength <- maLength + 1
        
    global$metaAnalysisData[[maLength]] <- list(Name = name, Experiment = global$selectedExperiment, Variable = global$daFinalSettings$observationVariable,
      Feature = global$daDoeFinalSettings$featureSelected, Reference = global$daSettings$referenceGroup, Experimental = global$daSettings$experimentalGroup, Covariate = global$daSettings$covariates,
      Analysis = "CSEA", Matrix = "Normal", Comparison = global$cseaResults$enrichmentTable)
    
    # Reset text widget
    updateTextInput(session, "cseaMetaAnalysisName", value = "")
    
    # Close modal
    toggleModal(session, "cseaMetaAnalysisSettingsModal", toggle = "close")
  })
  
  # **** Plots and tables ====
  # Competitive set enrichment table ====
  output$cseaTable <- DT::renderDataTable({
    # If there are no results, don't do anything
    if (is.null(global$cseaResults))  return (biomex.blankTable(text = "No results available."))
    
    # Get results
    enrichmentTable <- global$cseaResults$enrichmentTable
    
    # Get parameters
    featuresToShow <- input$cseaFeaturesToShow
    
    if (featuresToShow == "Significant (p-value)") enrichmentTable <- enrichmentTable[Pvalue < 0.05]
    if (featuresToShow == "Significant (adjusted p-value)") enrichmentTable <- enrichmentTable[`Adjusted pvalue` < 0.05]
    
    enrichmentTable
  }, options = list(scrollX = TRUE, autoWidth = FALSE), selection = 'single', rownames = FALSE)
  
  # Set content ====
  output$cseaSetContent <- shiny::renderDataTable({
    selectedIndex <- input$cseaTable_rows_selected
    
    # If there are no results, don't do anything
    if (is.null(global$cseaResults) || is.null(selectedIndex)) return (biomex.blankTable(text = "No results available."))
    
    # Get results
    differentialExpression <- global$cseaResults$differentialExpression
    enrichmentTable <- global$cseaResults$enrichmentTable
    featureSets <- global$cseaDoeResults$featureSets
    
    # Get parameters
    featuresToShow <- input$cseaFeaturesToShow
    
    if (featuresToShow == "Significant (p-value)") enrichmentTable <- enrichmentTable[Pvalue < 0.05]
    if (featuresToShow == "Significant (adjusted p-value)") enrichmentTable <- enrichmentTable[`Adjusted pvalue` < 0.05]
    
    # Get features
    set <- enrichmentTable$Set[selectedIndex]
    features <- featureSets[[set]]
    daFeatures <- differentialExpression$Feature
    daFeatures <- biomex.mapFeatures(daFeatures, from = "Name", to = "ID", mappingTable = global$mappingTable)
    
    # Subset differential expression
    index <- which(daFeatures %in% features)
    daSubset <- differentialExpression[index]
    
    global$cseaSetContent <- daSubset
    
    daSubset
  }, options = list(scrollX = TRUE, autoWidth = FALSE))
  
  # Plot ====
  output$cseaPlot <- renderPlotly({
    # If there are no results, don't do anything
    if (is.null(global$cseaResults)) 
    {
      global$cseaPlot <- NULL
      return (biomex.blankPlot(text = "No results available."))
    }
    
    # Get results
    enrichmentTable <- global$cseaResults$enrichmentTable
    enrichmentTable <- enrichmentTable[order(NES)]
    
    # Subset table if necessary
    featuresToShow <- input$cseaFeaturesToShow
    if (featuresToShow == "Significant (p-value)") enrichmentTable <- enrichmentTable[Pvalue < 0.05]
    if (featuresToShow == "Significant (adjusted p-value)") enrichmentTable <- enrichmentTable[`Adjusted pvalue` < 0.05]
    
    # Get parameters
    plotType <- input$cseaPlotType
    dotSize <- input$cseaDotSize
    halfSize <- as.integer(nrow(enrichmentTable) / 2)
    showTopFeatureNumber <-  biomex.validateNumericInput(value = input$cseaShowTopFeatureNumber, defaultTo = min(10, halfSize), min = 1, max = halfSize)
    labelSize <- input$cseaLabelSize
    upregulatedColor <- input$cseaUpregulatedColor
    downregulatedColor <- input$cseaDownregulatedColor
    
    if (nrow(enrichmentTable) == 0)
    {
      global$cseaPlot <- NULL
      return (biomex.blankPlot(text = "There are no features to show."))
    }
    
    if (plotType == "Waterfall plot")
    {
      # Define axis
      ax <- list(zeroline = FALSE, showline = TRUE, mirror = "ticks", gridcolor = toRGB("gray50"), gridwidth = 1, zerolinecolor = toRGB("black"),
              zerolinewidth = 1, linecolor = toRGB("black"), linewidth = 5, titlefont = list(size = 15))
      
      # Define colors
      colors <- ifelse(enrichmentTable$Direction == "Down", downregulatedColor, upregulatedColor)
        
      # Check colors validity
      if (biomex.areColors(colors = colors) == FALSE)  
      {
        global$cseaPlot <- NULL
        return (biomex.blankPlot(text = "Invalid color detected."))
      }
      
      p <- plot_ly(x = 1:length(enrichmentTable$Set), y = enrichmentTable$NES, type = "scatter", mode = "markers",
            color = colors, colors = colors,  marker = list(size = dotSize), text = enrichmentTable$Set)  %>%
            layout(showlegend = FALSE, xaxis = c(ax, showticklabels = FALSE), yaxis = c(ax, title = "NES"))
    }
    else if (plotType == "Barplot")
    {
      showTopFeatureNumber <- if (showTopFeatureNumber > nrow(enrichmentTable)) nrow(enrichmentTable) else showTopFeatureNumber
      
      # Subset by the top features
      enrichmentTable <- enrichmentTable[c(1:showTopFeatureNumber, (nrow(enrichmentTable) - showTopFeatureNumber + 1):nrow(enrichmentTable))]
      enrichmentTable$Set <- factor(enrichmentTable$Set, levels = enrichmentTable$Set)
      
      # Check colors validity
      if (biomex.areColors(colors = c(downregulatedColor, upregulatedColor)) == FALSE) 
      {
        global$cseaPlot <- NULL
        return (biomex.blankPlot(text = "Invalid color detected."))
      }
      
      # Get colors
      colors <- biomex.getColorPalette(palette = c(downregulatedColor, upregulatedColor), number = length(enrichmentTable$NES))
      
      tickfont <- list(size = labelSize)
      yaxis <- list(tickfont = tickfont)
      p <- plot_ly(x = enrichmentTable$NES, y = enrichmentTable$Set, color = enrichmentTable$Direction, colors = colors, type = "bar", orientation = "h", showlegend = FALSE) %>% layout(yaxis = yaxis)
    }
    
    global$cseaPlot <- p
  })
  
  # Enrichment plot ====
  output$cseaEnrichmentPlot <- renderPlotly({
    # Set up progress bar
    progress <- biomex.createProgressObject(message = "Generatin enrichment plot...", style = global$progressStyle)
    on.exit(progress$close())
    
    selectedIndex <- input$cseaTable_rows_selected
      
    # If there are no results, don't do anything
    if (is.null(selectedIndex)) 
    {
      global$cseaEnrichmentPlot <- NULL
      return (biomex.blankPlot(text = "Set not selected."))
    }
    
    # Get results
    enrichmentTable <- global$cseaResults$enrichmentTable
    cseaObject <- global$cseaResults$cseaObject
    
    # Get parameters
    featuresToShow <- input$cseaFeaturesToShow
    
    if (featuresToShow == "Significant (p-value)") enrichmentTable <- enrichmentTable[Pvalue < 0.05]
    if (featuresToShow == "Significant (adjusted p-value)") enrichmentTable <- enrichmentTable[`Adjusted pvalue` < 0.05]
        
    if (nrow(enrichmentTable) == 0)
    {
      global$cseaPlot <- NULL
      return (biomex.blankPlot(text = "There are no features to show."))
    }
    
    set <- enrichmentTable$Set[selectedIndex]

    R.devices::suppressGraphics({
      plots <- gseaplot(gseaResult = cseaObject, geneSetID = set)
    })
    
    p <- ggplotly(plots$runningScore)
    
    global$cseaEnrichmentPlot <- p
  })
    
  # **** Export ====
  # Plot export ====
  observeEvent(input$exportCseaPlot, {
    if (is.null(global$cseaPlot))
    {
      js$alertMessage("No results available to export.")
      return (NULL)
    }
    
    global$exportPlot <- global$cseaPlot
    
    # Open modal
    toggleModal(session, "exportPlotModal", toggle = "open")
  })
  
  observeEvent(input$exportCseaEnrichmentPlot, {
    if (is.null(global$cseaEnrichmentPlot))
    {
      js$alertMessage("No results available to export.")
      return (NULL)
    }
    
    global$exportPlot <- global$cseaEnrichmentPlot
    
    # Open modal
    toggleModal(session, "cseaEnrichmentModal", toggle = "close")
    toggleModal(session, "exportPlotModal", toggle = "open")
  })
  
  # Table export ====
  observeEvent(input$exportCseaTable, {
    if (is.null(global$cseaResults))
    {
      js$alertMessage("No results available to export.")
      return (NULL)
    }
    
    # Get results
    enrichmentTable <- global$cseaResults$enrichmentTable
    
    # Get parameters
    featuresToShow <- input$cseaFeaturesToShow
    
    if (featuresToShow == "Significant (p-value)") enrichmentTable <- enrichmentTable[Pvalue < 0.05]
    if (featuresToShow == "Significant (adjusted p-value)") enrichmentTable <- enrichmentTable[`Adjusted pvalue` < 0.05]
    
    global$exportTable <- enrichmentTable
    
    # Open modal
    toggleModal(session, "exportTableModal", toggle = "open")
  })
  
  observeEvent(input$exportCseaSetContent, {
    if (is.null(global$cseaSetContent))
    {
      js$alertMessage("No results available to export.")
      return (NULL)
    }
    
    global$exportTable <- global$cseaSetContent
    
    # Open modal
    toggleModal(session, "cseaSetContentModal", toggle = "close")
    toggleModal(session, "exportTableModal", toggle = "open")
  })
}
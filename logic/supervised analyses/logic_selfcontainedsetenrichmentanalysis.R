#' The logic of the set enrichment analysis tab.
#' 
#' @param input The shiny input object.
#' @param output The shiny output object. 
#' @param session The shiny session object.
#' @param global The reactive list of global variables.
logicSelfContainedSetEnrichmentAnalysis <- function(input, output, session, global)
{
  # **** Design of experiment ====
  biomex.designOfExperimentLogic(input, output, session, global, id = "ssea", 
    allowObservations = FALSE, allowFeatures = FALSE , allowSets = TRUE, allowAdditionalFiltering = FALSE, allowGrouping = FALSE)
  
  # **** Store and load results ====
  biomex.storeAndLoadResultsLogic(input, output, session, global, id = "ssea")
  
  # **** UI ====
  # Update widgets when switching experiments and when performing the analysis ====
  observeEvent(c(global$sseaFinalSettings, global$sseaDoeTrigger), {
    # Method settings
    updateSelectInput(session, "sseaFeaturesToUse", selected = global$sseaFinalSettings$featuresToUse)
    updateSelectInput(session, "sseaMinimumSetSize", selected = global$sseaFinalSettings$minimumSetSize)
    updateSelectInput(session, "sseaRotations", selected = global$sseaFinalSettings$permutations)
    updateSelectInput(session, "sseaRandomSeed", selected = global$sseaFinalSettings$randomSeed)
  })
 
  # **** Assign to global variables ====
  # Method settings
  observeEvent(input$sseaFeaturesToUse, {
    global$sseaSettings$featuresToUse <- input$sseaFeaturesToUse
  })
  
  observeEvent(input$sseaMinimumSetSize, {
    global$sseaSettings$minimumSetSize <- biomex.validateNumericInput(value = input$sseaMinimumSetSize, defaultTo = globalOriginal[["sseaSettings"]][["minimumSetSize"]], min = 2)
  })
  
  observeEvent(input$cseaPermutations, {
    global$sseaSettings$permutations <- biomex.validateNumericInput(value = input$sseaPermutations, defaultTo = globalOriginal[["sseaSettings"]][["rotations"]], min = 1)
  })
    
  observeEvent(input$sseaRandomSeed, {
    global$sseaSettings$randomSeed <- biomex.validateNumericInput(value = input$sseaRandomSeed, defaultTo = globalOriginal[["sseaSettings"]][["randomSeed"]])
  })
  
  # Modals ====
  observeEvent(input$sseaShowSetContent, {
    selectedIndex <- input$sseaTable_rows_selected
    
    # If there are no results, don't do anything
    if (is.null(global$sseaResults) || is.null(selectedIndex)) return (NULL)
        
    toggleModal(session, "sseaSetContentModal", toggle = "open")
  })
    
  # **** Perform self contained enrichment analysis ====
  observeEvent(input$updateSelfContainedSetEnrichmentAnalysis, {
    if (is.null(global$sseaDoeResults)) return (NULL)
    
    # If daResults is NULL, do nothing
    if (is.null(global$daResults))
    {
      js$alertMessage("You need to perform the differential analysis first.")
      return (NULL)
    }
    
    # Cannot perform SSEA when the matrix used in the differential analysis was the Engineered data matrix
    if (global$daFinalSettings$dataMatrixAsInput == "Engineered")
    {
      js$alertMessage("You cannot perform this analysis when the differential analysis is performed on the engineered data matrix.")
      return (NULL)
    }
    
    # Set up progress bar
    progress <- biomex.createProgressObject(message = "Performing self contained set enrichment analysis...", style = global$progressStyle)
    on.exit(progress$close())
            
    # Call garbage collector
    biomex.callGC()
    
    # Get the right data matrix
    progress$set(message = "Performing self contained set enrichment analysis...", detail = "Preparing data", value = 1)
    differentialExpression <- global$daResults$differentialExpression
    dataMatrix <- global$dataMatrixNormalized
    
    if (global$sseaSettings$featuresToUse == "Significant (p-value)") differentialExpression <- differentialExpression[Pvalue < 0.05]
    if (global$sseaSettings$featuresToUse == "Significant (adjusted p-value)") differentialExpression <- differentialExpression[`Adjusted pvalue` < 0.05]
    
    if (nrow(differentialExpression) == 0) 
    {
      js$alertMessage("Not enough features.")
      global$sseaResults <- NULL
      return (NULL)
    }
    
    # Perform self contained set enrichment analysis
    progress$set(message = "Performing self contained set enrichment analysis...", detail = "Calculating enrichment", value = 1)
    global$sseaResults <- biomex.performSelfContainedSetEnrichment(dataMatrix = dataMatrix, differentialExpression = differentialExpression,
      observations = global$daResults$observations, modelMatrix = global$daResults$modelMatrix, variable = global$daFinalSettings$observationVariable, 
      mappingTable = global$mappingTable, featureSets = global$sseaDoeResults$featureSets, minimumSetSize = global$sseaSettings$minimumSetSize, rotations = global$sseaSettings$rotations,
      randomSeed = global$sseaSettings$randomSeed)
    
    # If it fails, stop and reset results
    if (!is.null(global$sseaResults$errorMessage)) 
    {
      js$alertMessage(global$sseaResults$errorMessage)
      global$sseaResults <- NULL
      return (NULL)
    }
    
    # Remove unwanted variables
    biomex.removeVariables(variables = c("dataMatrix", "differentialExpression"))
    
    # Save settings
    global$sseaFinalSettings <- global$sseaSettings
    
    # Call garbage collector
    biomex.callGC()
  })
  
  # **** Plots and tables ====
  # Self contained set enrichment table ====
  output$sseaTable <- DT::renderDataTable({
    # If there are no results, don't do anything
    if (is.null(global$sseaResults)) return (biomex.blankTable(text = "No results available."))
    
    # Get results
    enrichmentTable <- global$sseaResults$enrichmentTable
    
    # Get parameters
    featuresToShow <- input$sseaFeaturesToShow
    
    if (featuresToShow == "Significant (p-value)") enrichmentTable <- enrichmentTable[Pvalue < 0.05]
    if (featuresToShow == "Significant (adjusted p-value)") enrichmentTable <- enrichmentTable[`Adjusted pvalue` < 0.05]
    
    enrichmentTable
  }, options = list(scrollX = TRUE, autoWidth = FALSE), selection = 'single', rownames = FALSE)
  
  # Set content ====
  output$sseaSetContent <- shiny::renderDataTable({
    selectedIndex <- input$sseaTable_rows_selected
    
    # If there are no results, don't do anything
    if (is.null(global$sseaResults) || is.null(selectedIndex)) return (biomex.blankTable(text = "No results available."))
    
    # Get results
    differentialExpression <- global$sseaResults$differentialExpression
    enrichmentTable <- global$sseaResults$enrichmentTable
    featureSets <- global$sseaDoeResults$featureSets
    
    # Get parameters
    featuresToShow <- input$sseaFeaturesToShow
    
    if (featuresToShow == "Significant (p-value)") enrichmentTable <- enrichmentTable[Pvalue < 0.05]
    if (featuresToShow == "Significant (adjusted p-value)") enrichmentTable <- enrichmentTable[`Adjusted pvalue` < 0.25]
    
    # Get features
    set <- enrichmentTable$Set[selectedIndex]
    features <- featureSets[[set]]
    daFeatures <- differentialExpression$Feature
    daFeatures <- biomex.mapFeatures(daFeatures, from = "Name", to = "ID", mappingTable = global$mappingTable)
    
    # Subset differential expression
    index <- which(daFeatures %in% features)
    daSubset <- differentialExpression[index]
    
    global$sseaSetContent <- daSubset
    
    daSubset
  }, options = list(scrollX = TRUE, autoWidth = FALSE))
  
  
  # **** Export ====
  # Table export ====
  observeEvent(input$exportSseaTable, {
    if (is.null(global$sseaResults))
    {
      js$alertMessage("No results available to export.")
      return (NULL)
    }
    
    global$exportTable <- global$sseaResults$enrichmentTable
    
    # Open modal
    toggleModal(session, "exportTableModal", toggle = "open")
  })
    
  observeEvent(input$exportSseaSetContent, {
    if (is.null(global$sseaSetContent))
    {
      js$alertMessage("No results available to export.")
      return (NULL)
    }
    
    global$exportTable <- global$sseaSetContent
    
    # Open modal
    toggleModal(session, "sseaSetContentModal", toggle = "close")
    toggleModal(session, "exportTableModal", toggle = "open")
  })
}
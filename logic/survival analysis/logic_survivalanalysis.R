#' The logic of the survival analysis tab.
#' 
#' @param input The shiny input object.
#' @param output The shiny output object. 
#' @param session The shiny session object.
#' @param global The reactive list of global variables.
logicSurvivalAnalysis <- function(input, output, session, global)
{
  # **** Design of experiment ====
  biomex.designOfExperimentLogic(input, output, session, global, id = "sa", 
    allowObservations = TRUE, allowFeatures = FALSE , allowSets = FALSE, allowAdditionalFiltering = FALSE, allowGrouping = FALSE)
  
  # **** UI ====
  observeEvent(c(global$saFinalSettings, global$saDoeTrigger), {
    updateTextInput(session, "saEventTarget", value = global$saFinalSettings$eventTarget)
    updateSliderInput(session, "saQuantiles", value = global$saFinalSettings$quantiles)
    updateCheckboxInput(session, "saExcludeMedium", value = global$saFinalSettings$excludeMedium)
    updateCheckboxInput(session, "saDivideTime", value = global$saFinalSettings$divideTime)
  })
  
  # Features ====
  observeEvent(c(global$metadataMatrix, global$saDoeTrigger, global$feTrigger), {
    # Get correct metadata subset
    originalFeatures <- biomex.getFeatureNames(dataMatrix = global$dataMatrixNormalized, mappingTable = global$mappingTable, toType = "Name")
    engineeredFeatures <- biomex.getFeatureNames(dataMatrix = global$feResults$dataMatrixEngineered)
    variables <- c("Not selected", biomex.getMetadataNames(metadataMatrix = global$metadataMatrix, ignoreFirstColumn = FALSE))
    
    # If the matrices are not computed yet, do nothing
    if (is.null(originalFeatures) || is.null(engineeredFeatures)) return (NULL)
    
    # Method settings
    # Target column
    selected <- biomex.getValidChoice(choices = variables, selected = global$saFinalSettings$eventVariable)
    updateSelectInput(session, "saEventVariable", choices = variables, selected = selected)
    
    # Time column
    selected <- biomex.getValidChoice(choices = variables, selected = global$saFinalSettings$timeVariable)
    updateSelectInput(session, "saTimeVariable", choices = variables, selected = selected)
    
    # Features
    # Original feature
    selected <- biomex.getValidChoice(choices = originalFeatures, selected = input$saOriginalFeature)
    updateSelectizeInput(session, "saOriginalFeature", choices = originalFeatures, selected = selected, server = TRUE)
    
    # Engineered feature
    selected <- biomex.getValidChoice(choices = engineeredFeatures, selected = input$saEngineeredFeature)
    updateSelectizeInput(session, "saEngineeredFeature", choices = engineeredFeatures, selected = selected, server = TRUE)
    
    # Stratification
    selected <- biomex.getValidChoice(choices = variables, selected = global$saFinalSettings$stratificationVariable)
    updateSelectInput(session, "saStratificationVariable", choices = variables, selected = selected)
  })
  
  # Stratification to visualize
  observeEvent(global$saResults, {
    if (is.null(global$saResults)) return (NULL)
    
    stratifications <- levels(factor(global$saResults$metadataMatrix$Stratification))
    if (length(stratifications) == 1) stratifications <- NULL # In this case, only "All" will be displayed
    
    updateSelectInput(session, "saStratificationToVisualize", choices = c("All", stratifications), selected = "All")
  })
  
  # **** Assign to global variables ====
  # Method settings
  observeEvent(input$saEventVariable, {
    global$saSettings$eventVariable <- input$saEventVariable
  })
  
  observeEvent(input$saEventTarget, {
    global$saSettings$eventTarget <- input$saEventTarget
  })  
  
  observeEvent(input$saTimeVariable, {
    global$saSettings$timeVariable <- input$saTimeVariable
  })
  
  observeEvent(input$saQuantiles, {
    global$saSettings$quantiles <- input$saQuantiles
  })
  
  observeEvent(input$saExcludeMedium, {
    global$saSettings$excludeMedium <- input$saExcludeMedium
  })
  
  observeEvent(input$saDivideTime, {
    global$saSettings$divideTime <- input$saDivideTime
  })
  
  observeEvent(input$saStratificationVariable, {
    global$saSettings$stratificationVariable <- input$saStratificationVariable
  })
  
  # **** Setup survival analysis ====
  observeEvent(input$updateSurvivalAnalysis, {
    if (is.null(global$saDoeResults)) return (NULL)
    
    # Set up progress bar
    progress <- biomex.createProgressObject(message = "Performing survival analysis...", style = global$progressStyle)
    on.exit(progress$close())
    
    # Call garbage collector
    biomex.callGC()
    
    # Perform survival analysis
    progress$set(message = "Performing survival analysis...", detail = "Setting up", value = 1)
    global$saResults <- biomex.setupSurvivalAnalysis(metadataMatrix = global$metadataMatrix, observationsSelected = global$saDoeResults$observationsSelected,
      eventVariable = global$saSettings$eventVariable, targetEvent = global$saSettings$eventTarget, timeVariable = global$saSettings$timeVariable,
      divideTime = global$saSettings$divideTime, stratificationVariable = global$saSettings$stratificationVariable)
    
    # If it fails, stop and reset results
    if (!is.null(global$saResults$errorMessage)) 
    {
      js$alertMessage(global$saResults$errorMessage)
      global$saResults <- NULL
      global$saSurvivalResults <- NULL
      global$saPlot <- NULL
      return (NULL)
    }
    
    # Save settings
    global$saFinalSettings <- global$saSettings
    
    # Call garbage collector
    biomex.callGC()
  })
  
  # **** Perform survival analysis ====
  observeEvent(c(global$saResults, input$saFeatures, input$saOriginalFeature, input$saEngineeredFeature, global$saFinalSettings), {
    # If survival analysis was not set up, do nothing
    if (is.null(global$saResults) || is.null(global$saDoeResults)) return (NULL)
    # If the experiment is switching, do nothing
    if (global$switchingExperiment == TRUE) return (NULL)
      
    # Set up progress bar
    progress <- biomex.createProgressObject(message = "Performing survival analysis...", style = global$progressStyle)
    on.exit(progress$close())

    # Perform survival analysis
    progress$set(message = "Performing survival analysis...", detail = "Computing", value = 1)
    if (input$saFeatures == "Original feature") 
    {
      feature <- input$saOriginalFeature
      dataMatrix <- global$dataMatrixNormalized
    }
    if (input$saFeatures == "Engineered feature")
    {
      feature <- input$saEngineeredFeature
      dataMatrix <- global$feResults$dataMatrixEngineered
    }
  
    global$saSurvivalResults <- biomex.performSurvivalAnalysis(dataMatrix = dataMatrix, metadataMatrix = global$saResults$metadataMatrix, mappingTable = global$mappingTable,
      observationsSelected = global$saDoeResults$observationsSelected, featureType = input$saFeatures, feature = feature, quantiles = global$saFinalSettings$quantiles, excludeMedium = global$saFinalSettings$excludeMedium)
    
    # If it fails, stop and reset results
    if (is.null(global$saSurvivalResults)) 
    {
      global$saPlot <- NULL
      return (NULL)
    }
  })
  
  # **** Plots and tables ====
  # Survival plot ====
  output$saPlot <- renderPlotly({
    if (is.null(global$saSurvivalResults))
    {
      global$saPlot <- NULL
      return (biomex.blankPlot(text = "No results available."))
    }
    
    # Get stratification
    stratifications <- names(global$saSurvivalResults)
    stratificationToVisualize <- input$saStratificationToVisualize
    
    survivalPlots <- list()
    for (stratification in stratifications)
    {
      # Get results
      survivalResults <- global$saSurvivalResults[[stratification]]
      survivalFit <- survivalResults$survivalFit
      pvalue <- survivalResults$pvalue
      
      # Get parameters
      lineThickness <- input$saLineThickness
      lineColorLower <- input$saLineColorLow
      lineColorHigher <- input$saLineColorHigh
      lineColorMedium <- input$saLineColorMedium
      showLegend <- !input$saLegendHide
      showGrid <- !input$saGridHide
      
      # Set the colors
      if (length(survivalFit$strata) == 1) lineColors <- c(lineColorMedium)
      else if (length(survivalFit$strata) == 2) lineColors <- c(lineColorHigher, lineColorLower)
      else if (length(survivalFit$strata) == 3) lineColors <- c(lineColorHigher, lineColorLower, lineColorMedium)
        
      # Check colors validity
      if (biomex.areColors(colors = lineColors) == FALSE)
      {
        global$saPlot <- NULL
        return (biomex.blankPlot(text = "Invalid color detected."))
      }
      
      # Create annotation
      annotation <- list(text = paste(stratification, " - ", round(pvalue, 4)), xref = "paper", yref = "paper", yanchor = "bottom", xanchor = "center",
        align = "center", x = 0.5, y = 1, showarrow = FALSE)
      
      p <- ggplotly(biomex.ggsurv(survivalFit, plot.cens = FALSE, back.white = TRUE, size = lineThickness, surv.col = lineColors)) %>%
        layout(annotations = annotation, showlegend = showLegend, xaxis = list(showgrid = showGrid, title = "Time"), yaxis = list(showgrid = showGrid, title = "Survival fraction"))
      
      survivalPlots[[stratification]] <- p
    }
    
    # Select the right plot to visualize
    if (stratificationToVisualize == "All" && length(survivalPlots) > 1) p <- plotly::subplot(survivalPlots, nrows = length(survivalPlots), margin = c(0.02, 0.02, 0.03, 0.03), shareX = FALSE)
    else if (stratificationToVisualize != "All") p <- survivalPlots[[stratificationToVisualize]]
    else p <- survivalPlots[["No stratification"]]
    
    global$saPlot <- p
    
    p
  })
  
  # Information table ====
  output$saInformationTable <- shiny::renderDataTable({
    if (is.null(global$saSurvivalResults)) return (biomex.blankTable(text = "You need to perform the survival analysis first."))

    # Get stratification
    stratifications <- names(global$saSurvivalResults)
    
    # Create empty data.table
    informationTable <- data.table()
    
    for (stratification in stratifications)
    {
      # Get results
      survivalResults <- global$saSurvivalResults[[stratification]]
      feature <- survivalResults$feature
      pvalue <- survivalResults$pvalue
      group <- as.data.table(table(survivalResults$group))
      colnames(group) <- c("Variable", "Value")
    
      informationTable <- rbind(informationTable, data.table(Variable = "Stratification", Value = stratification))
      informationTable <- rbind(informationTable, data.table(Variable = "Feature", Value = feature))
      informationTable <- rbind(informationTable, data.table(Variable = "P-value", Value = pvalue))
      informationTable <- rbind(informationTable, group)
      informationTable <- rbind(informationTable, data.table(Variable = "", Value = ""))
    }
    
    informationTable
  }, options = list(scrollX = TRUE, paging = FALSE, searching = FALSE, info = FALSE, autoWidth = FALSE))
  
  
  # **** Export ====
  # Plot export ====
  observeEvent(input$exportSaPlot, {
    if (is.null(global$saPlot))
    {
      js$alertMessage("No results available to export.")
      return (NULL)
    }
    
    global$exportPlot <- global$saPlot
    
    # Open modal
    toggleModal(session, "exportPlotModal", toggle = "open")
  })
}
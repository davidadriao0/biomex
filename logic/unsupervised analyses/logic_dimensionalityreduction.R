#' The logic of the Design of Experiment tab.
#' 
#' @param input The shiny input object.
#' @param output The shiny output object. 
#' @param session The shiny session object.
#' @param global The reactive list of global variables.
logicDimensionalityReduction <- function(input, output, session, global)
{
  # **** Design of experiment ====
  biomex.designOfExperimentLogic(input, output, session, global, id = "dr", 
    allowObservations = TRUE, allowFeatures = TRUE , allowSets = FALSE, allowAdditionalFiltering = TRUE, allowGrouping = FALSE)
  
  # **** Store and load results ====
  biomex.storeAndLoadResultsLogic(input, output, session, global, id = "dr")
  
  # **** UI ====
  # Update widgets when switching experiments and when performing the analysis ====
  observeEvent(c(global$drFinalSettings, global$drDoeTrigger), {
    # Method settings
    updateSelectInput(session, "drDataMatrixAsInput", selected = global$drFinalSettings$dataMatrixAsInput)
    updateSelectInput(session, "drScaling", selected = global$drFinalSettings$scaling)
    updateSelectInput(session, "drMethod", selected = global$drFinalSettings$method)
    updateNumericInput(session, "drDimensions", value = global$drFinalSettings$dimensions)
    updateCheckboxInput(session, "drAccurateModePCA", value = global$drFinalSettings$pcaAccurateMode)
    
    updateNumericInput(session, "tsnePcaDimensions", value = global$drFinalSettings$tsnePcaDimensions)
    updateNumericInput(session, "tsnePerplexity", value = global$drFinalSettings$tsnePerplexity)
    updateNumericInput(session, "tsneLearningRate", value = global$drFinalSettings$tsneLearningRate)
    updateNumericInput(session, "tsneIterations", value = global$drFinalSettings$tsneIterations)
    updateNumericInput(session, "tsneRandomSeed", value = global$drFinalSettings$tsneRandomSeed)
    updateNumericInput(session, "tsneCores", value = global$drFinalSettings$tsneCores)
    
    updateNumericInput(session, "umapPcaDimensions", value = global$drFinalSettings$umapPcaDimensions)
    updateNumericInput(session, "umapNeighbours", value = global$drFinalSettings$umapNeighbours)
    updateNumericInput(session, "umapMinimumDistance", value = global$drFinalSettings$umapMinimumDistance)
    updateNumericInput(session, "umapAlpha", value = global$drFinalSettings$umapAlpha)
    updateNumericInput(session, "umapEpochs", value = global$drFinalSettings$umapEpochs)
    updateNumericInput(session, "umapRandomSeed", value = global$drFinalSettings$umapRandomSeed)
  })
  
  # Color coding ====
  observeEvent(c(global$drDoePerformed, global$drDoeTrigger, global$feTrigger), {
    originalFeatures <- biomex.getFeatureNames(dataMatrix = global$dataMatrixNormalized, mappingTable = global$mappingTable, toType = "Name")
    engineeredFeatures <- global$feResults$dataMatrixEngineered$Feature
    
    # If the matrices are not computed yet, do nothing
    if (is.null(originalFeatures) || is.null(engineeredFeatures)) return (NULL)
    
    # Original feature
    selected <- biomex.getValidChoice(choices = originalFeatures, selected = input$drOriginalFeature)
    updateSelectizeInput(session, "drOriginalFeature", choices = originalFeatures, selected = selected, server = TRUE)
    
    # Engineered feature
    selected <- biomex.getValidChoice(choices = engineeredFeatures, selected = input$drEngineeredFeature)
    updateSelectizeInput(session, "drEngineeredFeature", choices = engineeredFeatures, selected = selected, server = TRUE)
  })
  
  observeEvent(global$metadataMatrix, {
    # Metadata
    choices <- biomex.getMetadataNames(metadataMatrix = global$metadataMatrix, ignoreFirstColumn = TRUE)
    selected <- biomex.getValidChoice(choices = choices, selected = input$drMetadata)
    updateSelectInput(session, "drMetadata", choices = choices, selected = selected)
  })
  
  # Dimensions ====
  observeEvent(global$drResults, {
    scoresMatrix <- global$drResults$scoresMatrix
    dimensions <- ncol(scoresMatrix) - 1
    
    updateSelectInput(session, "drXAxis", choices = 1:dimensions, selected = 1)
    updateSelectInput(session, "drYAxis", choices = 1:dimensions, selected = if (dimensions >= 2) 2 else 1)
    updateSelectInput(session, "drZAxis", choices = 1:dimensions, selected = if (dimensions >= 3) 3 else 1)
  })
  
  # **** Assign to global variables ====
  # Method settings
  observeEvent(input$drDataMatrixAsInput, {
    global$drSettings$dataMatrixAsInput <- input$drDataMatrixAsInput
  })
  
  observeEvent(input$drScaling, {
    global$drSettings$scaling <- input$drScaling
  })
  
  observeEvent(input$drMethod, {
    global$drSettings$method <- input$drMethod
  })
  
  observeEvent(input$drDimensions, {
    global$drSettings$dimensions <- input$drDimensions
  })
  
  observeEvent(input$drAccurateModePCA, {
    global$drSettings$pcaAccurateMode <- input$drAccurateModePCA
  })
  
  observeEvent(input$tsnePcaDimensions, {
    global$drSettings$tsnePcaDimensions <- biomex.validateNumericInput(value = input$tsnePcaDimensions, defaultTo = globalOriginal[["drSettings"]][["tsnePcaDimensions"]], min = 0, asInteger = TRUE)
  })
  
  observeEvent(input$tsnePerplexity, {
    global$drSettings$tsnePerplexity <- biomex.validateNumericInput(value = input$tsnePerplexity, defaultTo = globalOriginal[["drSettings"]][["tsnePerplexity"]], min = 1, asInteger = TRUE)
  })
  
  observeEvent(input$tsneLearningRate, {
    global$drSettings$tsneLearningRate <- biomex.validateNumericInput(value = input$tsneLearningRate, defaultTo = globalOriginal[["drSettings"]][["tsneLearningRate"]], min = 1)
  })
  
  observeEvent(input$tsneIterations, {
    global$drSettings$tsneIterations <- biomex.validateNumericInput(value = input$tsneIterations, defaultTo = globalOriginal[["drSettings"]][["tsneIterations"]], min = 1, asInteger = TRUE)
  })
  
  observeEvent(input$tsneRandomSeed, {
    global$drSettings$tsneRandomSeed <- biomex.validateNumericInput(value = input$tsneRandomSeed, defaultTo = globalOriginal[["drSettings"]][["tsneRandomSeed"]], min = 0, asInteger = TRUE)
  })
    
  observeEvent(input$tsneCores, {
    global$drSettings$tsneCores <- biomex.validateNumericInput(value = input$tsneCores, defaultTo = globalOriginal[["drSettings"]][["tsneCores"]], min = 1, asInteger = TRUE)
  })
  
  observeEvent(input$umapPcaDimensions, {
    global$drSettings$umapPcaDimensions <- biomex.validateNumericInput(value = input$umapPcaDimensions, defaultTo = globalOriginal[["drSettings"]][["umapPcaDimensions"]], min = 0, asInteger = TRUE)
  })
  
  observeEvent(input$umapNeighbours, {
    global$drSettings$umapNeighbours <- biomex.validateNumericInput(value = input$umapNeighbours, defaultTo = globalOriginal[["drSettings"]][["umapNeighbours"]], min = 1, asInteger = TRUE)
  })
  
  observeEvent(input$umapMinimumDistance, {
    global$drSettings$umapMinimumDistance <- biomex.validateNumericInput(value = input$umapMinimumDistance, defaultTo = globalOriginal[["drSettings"]][["umapMinimumDistance"]], min = 0)
  })
  
  observeEvent(input$umapAlpha, {
    global$drSettings$umapAlpha <- biomex.validateNumericInput(value = input$umapAlpha, defaultTo = globalOriginal[["drSettings"]][["umapAlpha"]], min = 1)
  })
  
  observeEvent(input$umapEpochs, {
    global$drSettings$umapEpochs <- biomex.validateNumericInput(value = input$umapEpochs, defaultTo = globalOriginal[["drSettings"]][["umapEpochs"]], min = 1, asInteger = TRUE)
  })
  
  observeEvent(input$umapRandomSeed, {
    global$drSettings$umapRandomSeed <- biomex.validateNumericInput(value = input$umapRandomSeed, defaultTo = globalOriginal[["drSettings"]][["umapRandomSeed"]], min = 0, asInteger = TRUE)
  })
  
  # **** Perform dimensionality reduction ====
  observeEvent(input$updateDimensionalityReduction, {
    if (is.null(global$drDoeResults)) return (NULL)
    
    # Set up progress bar
    progress <- biomex.createProgressObject(message = "Performing dimensionality reduction...", style = global$progressStyle)
    on.exit(progress$close())
    
    # Call garbage collector
    biomex.callGC()
    
    # Get the right data matrix
    progress$set(message = "Performing dimensionality reduction...", detail = "Preparing data", value = 1)
    dataMatrix <- biomex.obtainCorrectDataMatrix(dataMatrixNormalized = global$dataMatrixNormalized, dataMatrixEngineered = global$feResults$dataMatrixEngineered, 
      dataMatrixNormalizedGrouped = global$drDoeResults$dataMatrixNormalizedGrouped, dataMatrixEngineeredGrouped = global$drDoeResults$dataMatrixEngineeredGrouped,
      dataMatrixAsInput = global$drSettings$dataMatrixAsInput, observationsSelected = global$drDoeResults$observationsSelected,
      mappingTable = global$mappingTable, mappingType = "Name",
      performAdditionalFiltering = FALSE, featuresSelected = global$drDoeResults$featuresSelected, additionalFilteringFeaturesSelected = NULL)

    # Scale the data
    progress$set(message = "Performing dimensionality reduction...", detail = "Scaling data", value = 1)
    dataMatrix <- biomex.scaleData(dataMatrix = dataMatrix, type = global$drSettings$scaling, returnAsTransposed = TRUE) # The data is partially modified by reference
    
    # Call garbage collector
    biomex.callGC()
    
    # Perform dimensionality reduction
    progress$set(message = "Performing dimensionality reduction...", detail = "Reducing data", value = 1)
    global$drResults <- biomex.dimensionalityReduction(dataMatrix = dataMatrix, isTransposed = TRUE,
     type = global$drSettings$method, dimensions = global$drSettings$dimensions, accurateMode = global$drSettings$pcaAccurateMode, 
     tsnePcaDimensions = global$drSettings$tsnePcaDimensions, tsnePerplexity = global$drSettings$tsnePerplexity, tsneIterations = global$drSettings$tsneIterations, 
     tsneLearningRate = global$drSettings$tsneLearningRate, tsneRandomSeed = global$drSettings$tsneRandomSeed, tsneCores = global$drSettings$tsneCores,
     umapPcaDimensions = global$drSettings$umapPcaDimensions, umapNeighbours = global$drSettings$umapNeighbours,  umapMinimumDistance = global$drSettings$umapMinimumDistance,
     umapEpochs = global$drSettings$umapEpochs, umapAlpha = global$drSettings$umapAlpha, umapRandomSeed = global$drSettings$umapRandomSeed)  # The data is partially modified by reference

    # Display the error message and reset the results
    if (!is.null(global$drResults$errorMessage))
    {
      js$alertMessage(global$drResults$errorMessage)
      global$drResults <- NULL
      global$drPlot <- NULL
      global$drLoadingsPlot <- NULL
      return (NULL)
    }
    
    # Remove unwanted variables
    biomex.removeVariables(variables = "dataMatrix")

    # Save settings
    global$drFinalSettings <- global$drSettings
    
    # Call garbage collector
    biomex.callGC()
  })
  
  # Async version of the dimensionality reduction (MUST BE UPDATED)
  # observeEvent(input$updateDimensionalityReduction, {
  #   if (is.null(global$drDoeResults)) return (NULL)
  #   
  #   # Set up progress bar
  #   progress <- biomex.createProgressObject(message = "Performing dimensionality reduction...", style = global$progressStyle)
  #   # on.exit(progress$close()) # on.exit cannot be used when future is used, since it would block the async computation
  #   # We need to close the progress manually in the promise statements (not in the future statement).
  #       
  #   # Get the right data matrix
  #   progress$set(message = "Performing dimensionality reduction...", detail = "Preparing data", value = 1)
  #   dataMatrix <- biomex.obtainCorrectDataMatrix(dataMatrixNormalized = global$dataMatrixNormalized, dataMatrixEngineered = global$feResults$dataMatrixEngineered, 
  #     dataMatrixNormalizedGrouped = global$drDoeResults$dataMatrixNormalizedGrouped, dataMatrixEngineeredGrouped = global$drDoeResults$dataMatrixEngineeredGrouped,
  #     dataMatrixAsInput = global$drSettings$dataMatrixAsInput, observationsSelected = global$drDoeResults$observationsSelected,
  #     mappingTable = global$mappingTable, mappingType = "Name",
  #     performAdditionalFiltering = FALSE, featuresSelected = global$drDoeResults$featuresSelected, additionalFilteringFeaturesSelected = NULL)
  # 
  #   # In async computation, we cannot read/write any reactive values in the future statement.
  #   # So we need to put them in a normal variable before hand.
  #   # What is passed to the future statement is passed through sockets, so it can be slow to create the child if you pass too much data.
  #   dataMatrixNormalized <- global$dataMatrixNormalized
  #   dataMatrixEngineered <- global$feResults$dataMatrixEngineered
  #   mappingTable <- global$mappingTable
  #   drDoeResults <- global$drDoeResults
  #   drSettings <- global$drSettings 
  #   
  #   # Set the progress bar
  #   progress$set(message = "Performing dimensionality reduction...", detail = "Computing (background processing)", value = 1)
  #   
  #   future({
  #     # Scale the data
  #     dataMatrix <- biomex.scaleData(dataMatrix = dataMatrix, type = drSettings$scaling)
  #     
  #     # Perform dimensionality reduction
  #     resultList <- biomex.dimensionalityReduction(dataMatrix = dataMatrix, type = drSettings$method, dimensions = drSettings$dimensions, 
  #      accurateMode = drSettings$pcaAccurateMode, pcaDimensions = drSettings$tsnePcaDimensions, perplexity = drSettings$tsnePerplexity, 
  #      iterations = drSettings$tsneIterations, learningRate = drSettings$tsneLearningRate, randomSeed = drSettings$tsneRandomSeed)
  #     
  #     return (resultList)
  #   }) %...>% (function(results) 
  #   {
  #     progress$close() # Close the progress bar
  #     
  #     global$drResults <- results
  #     
  #     # Display the error message and reset the results
  #     if (!is.null(global$drResults$errorMessage))
  #     {
  #       js$alertMessage(global$drResults$errorMessage)
  #       global$drResults <- NULL
  #       global$drPlot <- NULL
  #       global$drLoadingsPlot <- NULL
  #       return (NULL)
  #     }
  #     
  #     # Save settings
  #     global$drFinalSettings <- global$drSettings
  #   }) %...!% (function(error) 
  #   {
  #     progress$close() # Close the progress bar
  #     
  #     js$alertMessage("Dimensionality reduction child process failed, computation stopped.")
  #     global$drResults <- NULL
  #     global$drPlot <- NULL
  #     global$drLoadingsPlot <- NULL
  #     print(error)
  #   })
  #   NULL # Necessary to make the future statement a fire-and-forget statement. Without it async doesn't work intra-user.
  # })
  
  # **** Manual group selection ====
  observeEvent(input$drPerformManualClustering, {
    # If there are no results, don't do anything
    if (is.null(global$drResults)) return(NULL)
    
    eventData <- event_data("plotly_selected", source = "drPlotSource")
    
    # If no points were selected, do nothing
    if (is.null(eventData) || nrow(eventData) == 0)
    {
      js$alertMessage("No points selected.")
      return (NULL)
    }
    
    scoresMatrix <- global$drResults$scoresMatrix
    groupName <- input$drManualClusteringGroup
    
    # Get the selected observations
    scoresMatrix <- scoresMatrix[Observation %in% eventData$key]
    observations <- scoresMatrix$Observation
    
    # If the ManualClustering column is not present, create it
    if (is.null(global$metadataMatrix[["ManualClustering"]])) global$metadataMatrix$ManualClustering <- "Not selected"
    
    global$metadataMatrix[Observation %in% observations, "ManualClustering"] <- groupName
  })
  
  observeEvent(input$drResetManualClustering, {
     global$metadataMatrix$ManualClustering <- "Not selected"
  })
  
  # **** Plots and tables ====
  # Scores ====
  output$drPlot <- renderPlotly({
    # If there are no results, don't do anything
    if (is.null(global$drResults)) 
    {
      global$drPlot <- NULL
      return (biomex.blankPlot(text = "No results available."))
    }
    
    scoresMatrix <- global$drResults$scoresMatrix
    varianceExplained <- global$drResults$varianceExplained
    observations <- scoresMatrix$Observation
    dataMatrixAsInput <- global$drFinalSettings$dataMatrixAsInput
    
    # Obtain all the variables for convenience
    colorCoding <- input$drColorCoding
    metadataSelection <- input$drMetadata
    featureSelection <- input$drOriginalFeature
    engineeredSelection <- input$drEngineeredFeature
    reverseColors <- input$drReverseColors
    metadataColor <- input$drMetadataColor
    forceToCharacter <- input$drMetadataForceNotNumeric
    featureColor <- input$drFeatureColor
    forcePositive <- input$drMetadataForcePositive
    gradientBegin <- input$drColorGradient[1]
    gradientEnd <- input$drColorGradient[2]
    show3DPlot <- input$drShow3DPlot
    dotSize <- input$drDotSize
    bottomLegend <- input$drLegendBottom
    hideLegend <- input$drLegendHide
    sizeLegend <- input$drLegendSize
    hideColorBar <- input$drColorBarHide
    showGrid <- input$drShowGrid
    xSelected <- as.numeric(input$drXAxis)
    ySelected <- as.numeric(input$drYAxis)
    zSelected <- as.numeric(input$drZAxis)
    xMirror <- input$drXAxisMirror
    yMirror <- input$drYAxisMirror
    zMirror <- input$drZAxisMirror
    showFeatureName <- input$drShowFeatureName
    featureThresholdCheckbox <- input$drFeatureThresholdCheckbox
    featureThreshold <- biomex.validateNumericInput(value = input$drFeatureThreshold, defaultTo = 0)
    aboveThresholdColor <- input$drFeatureThresholdAboveColor
    belowThresholdColor <- input$drFeatureThresholdBelowColor
    customColors <- NULL
    predefinedColors <- NULL

    # Select the right feature to plot
    if (colorCoding == "Metadata") 
    {
      if (metadataSelection == "")
      {
        return (biomex.blankPlot(text = "No feature selected."))
      }
      else
      {
        if (dataMatrixAsInput %in% c("Normal", "Engineered"))
        {
          metadataMatrix <- biomex.getDataTableSubset(dataTable = global$metadataMatrix, rows = observations)
          values <- metadataMatrix[[metadataSelection]]
          variable <- metadataSelection
          
          # Get the custom colors and the predefined colors
          customColors <- biomex.getCustomColors(colorCustomTable = input$drMetadataColorCustomTable)
          predefinedColors <- biomex.getPredefinedColors(metadataColors = global$metadataColors, name = variable, values = values, useFactorOrder = TRUE)
        }
        else
        {
          values <- observations
          variable <- "Grouped observations"
        }
      }
    }
    else if (colorCoding == "Original feature")
    {
      if (featureSelection == "")
      {
        return (biomex.blankPlot(text = "No feature selected."))
      }
      else
      {
        featureConverted <- biomex.mapFeatures(features = featureSelection, from = "Name", to = "Original", mappingTable = global$mappingTable)
        if (dataMatrixAsInput %in% c("Normal", "Engineered")) 
          dataMatrix <- biomex.getDataTableSubset(dataTable = global$dataMatrixNormalized, columns = observations, rows = featureConverted)
        else
          dataMatrix <- biomex.getDataTableSubset(dataTable = global$drDoeResults$dataMatrixNormalizedGrouped, columns = NULL, rows = featureConverted)
    
        values <- transpose(dataMatrix)[[1]] # Doing this is much faster then doing as.numeric(dataMatrix[1, -"Feature"])
        variable <- featureSelection
      }
    }
    else if (colorCoding == "Engineered feature") 
    {
      if (engineeredSelection == "")
      {
        return (biomex.blankPlot(text = "No feature selected."))
      }
      else
      {
        if (dataMatrixAsInput %in% c("Normal", "Engineered")) 
          dataMatrix <- biomex.getDataTableSubset(dataTable = global$feResults$dataMatrixEngineered, columns = observations, rows = engineeredSelection)
        else
          dataMatrix <- biomex.getDataTableSubset(dataTable = global$drDoeResults$dataMatrixEngineeredGrouped, columns = NULL, rows = engineeredSelection)
       
        values <- transpose(dataMatrix)[[1]] # Doing this is much faster then doing as.numeric(dataMatrix[1, -"Feature"])
        variable <- engineeredSelection
      }
    }
    
    # When switching experiments, widgets gets updated in different order. The metadata widget gets updated later, so we need to check for it.
    # No metadata column present || # No feature present
    if (is.null(values) || any(is.na(values)))
    {
      global$drPlot <- NULL
      return (biomex.blankPlot())
    }
    
    # Special option for the metadata
    if (colorCoding == "Metadata")
    {
      if (all(check.numeric(values, exceptions = NULL) == TRUE) && forceToCharacter == FALSE) values <- as.numeric(values)
      if (forceToCharacter == TRUE) values <- as.character(values)
    }
    
    # If there are too many factors, don't plot them
    if (!is.numeric(values) && length(unique(values)) > global$plotLimit)
    {
      global$drPlot <- NULL
      return (biomex.blankPlot(text = "Too many factors to create the plot."))
    }
    
    # Special option for original features and engineered features
    if (forcePositive == TRUE) values[values < 0 ] <- 0
    
    # Select the right colors
    if (!is.numeric(values)) colors <- biomex.getColorPalette(palette = metadataColor, number = length(unique(values)), reverse = reverseColors, customColors = customColors, predefinedColors = predefinedColors)
    else colors <- biomex.getColorPalette(palette = featureColor, number = 10, begin = gradientBegin, end = gradientEnd, reverse = reverseColors)
    
    # Special option for the feature threshold coloring
    if (featureThresholdCheckbox == TRUE && is.numeric(values))
    {
      featureAboveThreshold <- values > featureThreshold
      values[featureAboveThreshold] <- "Above threshold"
      values[!featureAboveThreshold] <- "Below threshold"
      values <- factor(values, levels = c("Below threshold", "Above threshold"))
      colors <- c()
      colors[1] <- belowThresholdColor
      colors[2] <- aboveThresholdColor
    }
    
    # Check colors validity
    if (biomex.areColors(colors = colors) == FALSE)
    {
      global$drPlot <- NULL
      return (biomex.blankPlot(text = "Invalid color detected."))
    }
    
    # Select the right data to show
    xValues <- scoresMatrix[[xSelected + 1]]
    yValues <- scoresMatrix[[ySelected + 1]]
    zValues <- scoresMatrix[[zSelected + 1]]
    
    # Set up the axes
    xTitle <- colnames(scoresMatrix)[[xSelected + 1]]
    yTitle <- colnames(scoresMatrix)[[ySelected + 1]]
    zTitle <- colnames(scoresMatrix)[[zSelected + 1]]
    
    if (!is.null(varianceExplained))
    {
      xTitle <- paste0(xTitle, " (", varianceExplained[xSelected], "%)")
      yTitle <- paste0(yTitle, " (", varianceExplained[ySelected], "%)")
      zTitle <- paste0(zTitle, " (", varianceExplained[zSelected], "%)")
    }
    
    xAxis <- list(linecolor = "black", linewidth = 4, mirror = TRUE, zeroline = FALSE, title = xTitle, showgrid = showGrid)
    yAxis <- list(linecolor = "black", linewidth = 4, mirror = TRUE, zeroline = FALSE, title = yTitle, showgrid = showGrid)
    zAxis <- list(linecolor = "black", linewidth = 4, mirror = TRUE, zeroline = FALSE, title = zTitle, showgrid = showGrid)
   
    # Mirror data
    if (xMirror == TRUE) xValues <- -xValues
    if (yMirror == TRUE) yValues <- -yValues
    if (zMirror == TRUE) zValues <- -zValues
    
    # 2D plot
    if (show3DPlot == FALSE)
    {
      p <- plot_ly(x = xValues, y = yValues, type = "scatter", mode = "markers", source = "drPlotSource",
        key = scoresMatrix$Observation, marker = list(size = dotSize), text = scoresMatrix$Observation,
        color = values, colors = colors) %>% layout(xaxis = xAxis, yaxis = yAxis, legend = list(font = list(size = sizeLegend))) 
    }
    
    # 3D plot
    if (show3DPlot == TRUE)
    {
      p <- plot_ly(x = xValues, y = yValues, z = zValues, type = "scatter3d", mode = "markers", source = "drPlotSource",
        key = scoresMatrix$Observation, marker = list(size = dotSize), text = scoresMatrix$Observation,
        color = values, colors = colors) %>% layout(scene = list(xaxis = xAxis, yaxis = yAxis, zaxis = zAxis), legend = list(font = list(size = sizeLegend))) 
    }
    
    if (showFeatureName == TRUE) p <- p %>% layout(title = variable)
    
    if (bottomLegend == TRUE) p <- p %>% layout(legend = list(orientation = "h"))
      
    if (hideLegend == TRUE) p <- p %>% layout(showlegend = FALSE)
    
    if (hideColorBar == TRUE) p <- hide_colorbar(p)
      
    global$drPlot <- p
    
    if (global$doNotUseWebGL == TRUE) p else toWebGL(p)
  })
  
  # Loadings ====
  output$drLoadingsPlot <- renderPlotly({
    # If there are no results, don't do anything
    if (is.null(global$drResults))
    {
      global$drLoadingsPlot <- NULL
      return (biomex.blankPlot(text = "No results available."))
    }
    
    loadingsMatrix <- global$drResults$loadingsMatrix
    varianceExplained <- global$drResults$varianceExplained
    
    # Only PCA has loadings. If some other dimensionality reduction technique is used, do nothing.
    if (is.null(loadingsMatrix))
    {
      global$drLoadingsPlot <- NULL
      return (biomex.blankPlot(text = "No loadings available."))
    }
    
    # Obtain all the variables for convenience
    show3DPlot <- input$drShow3DPlot
    dotSize <- input$drDotSize
    bottomLegend <- input$drLegendBottom
    hideLegend <- input$drLegendHide
    showGrid <- input$drShowGrid
    xSelected <- as.numeric(input$drXAxis)
    ySelected <- as.numeric(input$drYAxis)
    zSelected <- as.numeric(input$drZAxis)
    xMirror <- input$drXAxisMirror
    yMirror <- input$drYAxisMirror
    zMirror <- input$drZAxisMirror
    sizeLegend <- input$drLegendSize
  
    # Select the right data to show
    xValues <- loadingsMatrix[[xSelected + 1]]
    yValues <- loadingsMatrix[[ySelected + 1]]
    zValues <- loadingsMatrix[[zSelected + 1]]
    
    # Set up the axes
    xTitle <- colnames(loadingsMatrix)[[xSelected + 1]]
    yTitle <- colnames(loadingsMatrix)[[ySelected + 1]]
    zTitle <- colnames(loadingsMatrix)[[zSelected + 1]]
    
    if (!is.null(varianceExplained))
    {
      xTitle <- paste0(xTitle, " (", varianceExplained[xSelected], "%)")
      yTitle <- paste0(yTitle, " (", varianceExplained[ySelected], "%)")
      zTitle <- paste0(zTitle, " (", varianceExplained[zSelected], "%)")
    }
    
    xAxis <- list(linecolor = "black", linewidth = 4, mirror = TRUE, zeroline = FALSE, title = xTitle, showgrid = showGrid)
    yAxis <- list(linecolor = "black", linewidth = 4, mirror = TRUE, zeroline = FALSE, title = yTitle, showgrid = showGrid)
    zAxis <- list(linecolor = "black", linewidth = 4, mirror = TRUE, zeroline = FALSE, title = zTitle, showgrid = showGrid)
   
    # Mirror data
    if (xMirror == TRUE) xValues <- -xValues
    if (yMirror == TRUE) yValues <- -yValues
    if (zMirror == TRUE) zValues <- -zValues

    # 2D plot
    if (show3DPlot == FALSE)
    {
      p <- plot_ly(x = xValues, y = yValues, type = "scatter", mode = "markers",
         marker = list(size = dotSize), text = loadingsMatrix$Feature) %>% layout(xaxis = xAxis, yaxis = yAxis, legend = list(font = list(size = sizeLegend)))
    }

    # 3D plot
    if (show3DPlot == TRUE)
    {
      p <- plot_ly(x = xValues, y = yValues, z = zValues, type = "scatter3d", mode = "markers",
        marker = list(size = dotSize), text = loadingsMatrix$Feature) %>% layout(scene = list(xaxis = xAxis, yaxis = yAxis, zaxis = zAxis), legend = list(font = list(size = sizeLegend)))
    }

    if (bottomLegend == TRUE) p <- p %>% layout(legend = list(orientation = "h"))

    if (hideLegend == TRUE) p <- p %>% layout(showlegend = FALSE)

    global$drLoadingsPlot <- p
    
    if (global$doNotUseWebGL == TRUE) p else toWebGL(p)
  })
  
  # Custom colors table ====
  output$drMetadataColorCustomTable <- renderRHandsontable({
    metadataSelection <- input$drMetadata
    if (metadataSelection == "" || is.null(global$drResults$scoresMatrix)) return (NULL)
    
    # Get values in the correct order (the factor)
    scoresMatrix <- global$drResults$scoresMatrix
    observations <- scoresMatrix$Observation
    metadataMatrix <- biomex.getDataTableSubset(dataTable = global$metadataMatrix, rows = observations)
    values <- metadataMatrix[[metadataSelection]]
    values <- levels(factor(values))
    
    # Create the data frame
    df <- data.table(Value = values, Color = rep("black", length(values)))
    
    # If there are too many rows, don't create the table
    if (nrow(df) > global$tableLimit) return (biomex.blankHandsontable(text = "Too many factors to create the table.", header = "Warning"))
    
    # Create rhandsontable
    rhandsontable(df[, -"Value"], rowHeaders = df$Value, stretchH = "all", rowHeaderWidth = 250, 
                  fillHandle = list(direction = 'vertical', autoInsertRow = FALSE), maxRows  = nrow(df)) %>%
      hot_rows(rowHeights = 25, fixedRowsTop = 1) %>%
      hot_cols(columnSorting = FALSE, manualColumnResize = FALSE, halign = "htCenter") %>%
      hot_context_menu(allowRowEdit = FALSE, allowColEdit = FALSE)
  })
  
  # **** Export ====
  # Plot export ====
  observeEvent(input$exportDrPlot, {
    if (input$drMain == "Scores") exportPlot <- global$drPlot
    if (input$drMain == "Loadings") exportPlot <- global$drLoadingsPlot
    
    if (is.null(exportPlot))
    {
      js$alertMessage("No results available to export.")
      return (NULL)
    }
    
    global$exportPlot <- exportPlot
    
    # Open modal
    toggleModal(session, "exportPlotModal", toggle = "open")
  })
  
  # Table export ====
  observeEvent(input$exportDrTable, {
    if (input$drMain == "Scores") exportTable <- global$drResults$scoresMatrix
    if (input$drMain == "Loadings") exportTable <- global$drResults$loadingsMatrix
    
    if (is.null(exportTable))
    {
      js$alertMessage("No results available to export.")
      return (NULL)
    }
    
    global$exportTable <- exportTable
    
    # Open modal
    toggleModal(session, "exportTableModal", toggle = "open")
  })
}
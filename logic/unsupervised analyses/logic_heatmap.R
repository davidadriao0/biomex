#' The logic of the Heatmap tab.
#' 
#' @param input The shiny input object.
#' @param output The shiny output object. 
#' @param session The shiny session object.
#' @param global The reactive list of global variables.
logicHeatmap <- function(input, output, session, global)
{
  # **** Design of experiment ====
  biomex.designOfExperimentLogic(input, output, session, global, id = "heatmap", 
    allowObservations = TRUE, allowFeatures = TRUE , allowSets = TRUE, allowAdditionalFiltering = TRUE, allowGrouping = TRUE)
  
  # **** Store and load results ====
  biomex.storeAndLoadResultsLogic(input, output, session, global, id = "heatmap")
  
  # **** UI ====
  # Update widgets when switching experiments and when performing the analysis ====
  observeEvent(c(global$heatmapFinalSettings, global$heatmapDoeTrigger), {
    # Method settings
    updateSelectInput(session, "heatmapDataMatrixAsInput", selected = global$heatmapFinalSettings$dataMatrixAsInput)
    updateCheckboxInput(session, "heatmapForcePositive", value = global$heatmapFinalSettings$forcePositive)
    updateSelectInput(session, "heatmapScaling", selected = global$heatmapFinalSettings$scaling)
    updateSelectInput(session, "heatmapType", selected = global$heatmapFinalSettings$type)
    
    # Clustering settings
    updateRadioButtons(session, "heatmapClusteringLocation", selected = global$heatmapFinalSettings$clusteringLocation)
    updateRadioButtons(session, "heatmapClusteringType", selected = global$heatmapFinalSettings$clusteringType)
    updateSelectInput(session, "heatmapClusteringDistance", selected = global$heatmapFinalSettings$distance)
    updateSelectInput(session, "heatmapClusteringAgglomeration", selected = global$heatmapFinalSettings$agglomeration)
    updateCheckboxInput(session, "heatmapClusteringUseSquaredDistance", value = global$heatmapFinalSettings$useSquaredDistance)
    updateSliderInput(session, "heatmapPvalue", value = global$heatmapFinalSettings$pvalueThreshold)
    updateNumericInput(session, "heatmapBootstrap", value = global$heatmapFinalSettings$bootstrapNumber)
  })
  
  # I use the input variable for the triggering because the global one triggers when just one variable in the list is modified
  observeEvent(c(input$heatmapDataMatrixAsInput, global$heatmapDoeResults, global$heatmapDoeTrigger, global$feTrigger), {
    # If the matrices are not computed yet, do nothing
    if (is.null(global$dataMatrixNormalized) || is.null(global$feResults$dataMatrixEngineered) || is.null(global$heatmapDoeResults)) return (NULL)
    
    # Get relevant variables
    sets <- biomex.getSetNames(featureSets = global$heatmapDoeResults$featureSets)
    originalFeatures <- global$heatmapDoeResults$featuresSelected
    engineeredFeatures <- biomex.getFeatureNames(dataMatrix = global$feResults$dataMatrixEngineered)
    originalObservations <- global$heatmapDoeResults$observationsSelected
    groupedObservations <- biomex.getObservationNames(dataMatrix = global$heatmapDoeResults$dataMatrixNormalizedGrouped)
    
    # Get the right features
    originalFeatures <- biomex.mapFeatures(originalFeatures, from = "Original", to = "Name", mappingTable = global$mappingTable)

    # Get the correct features
    if (input$heatmapDataMatrixAsInput %in% c("Normal", "Normal grouped")) featureChoices <- originalFeatures
    else featureChoices <- engineeredFeatures
    
    # Get the correct observations
    if (input$heatmapDataMatrixAsInput %in% c("Normal", "Engineered")) observationChoices <- originalObservations
    else observationChoices <- groupedObservations
    
    # Don't reset it when switching experiment, just show what was there before
    if (global$switchingExperiment == FALSE && global$loadStoredResults == FALSE)
    {
      featuresSelected <- biomex.getValidChoice(choices = featureChoices, selected = input$heatmapFeatures, defaultAsEmptyString = TRUE)
      observationsSelected <- biomex.getValidChoice(choices = observationChoices, selected = input$heatmapObservations, defaultAsEmptyString = TRUE)
      updateSelectizeInput(session, "heatmapFeatures", choices = featureChoices, selected = featuresSelected, server = TRUE)
      updateSelectizeInput(session, "heatmapObservations", choices = observationChoices, selected = observationsSelected, server = TRUE)
      
      if(input$heatmapDataMatrixAsInput %in% c("Normal", "Normal grouped")) selected <- biomex.getValidChoice(choices = sets, selected = input$heatmapSets, defaultAsEmptyString = TRUE)
      else selected <- ""
      updateSelectizeInput(session, "heatmapSets", choices = sets, selected = selected, server = TRUE) # Reset the sets, cannot use them with the engineered features
    }
    else
    {
      updateSelectizeInput(session, "heatmapFeatures", choices = featureChoices, selected = global$heatmapFinalSettings$featuresSelected, server = TRUE)
      updateSelectizeInput(session, "heatmapSets", choices = sets, selected = global$heatmapFinalSettings$setsSelected, server = TRUE)
      updateSelectizeInput(session, "heatmapObservations", choices = observationChoices, selected = global$heatmapFinalSettings$observationsSelected, server = TRUE)
    }
  }, ignoreInit = TRUE) # FIX: It crashes if ignoreInit is set to FALSE because there are NULL values in the variables
  
  # Column coloring ====
  observeEvent(global$metadataMatrix,{
    variables <- biomex.getMetadataNames(metadataMatrix = global$metadataMatrix, ignoreFirstColumn = TRUE) 
    
    selected <- biomex.getValidChoice(choices = c("Not selected", variables), selected = input$heatmapColumnSideColors)
    updateSelectInput(session, "heatmapColumnSideColors", choices = c("Not selected", variables), selected = selected)
  })
  
  # Show/hide upload button based on whether BIOMEX is offline or online
  observeEvent(global$online, {
    shinyjs::hide("heatmapFileSelectionDiv")
    shinyjs::hide("heatmapFileSelectionOnlineDiv")
    
    if (global$online == TRUE) shinyjs::show("heatmapFileSelectionOnlineDiv")
    else if (global$online == FALSE) shinyjs::show("heatmapFileSelectionDiv")
  })
  
  # Upload sets file offline ====
  observeEvent(input$heatmapSetsFileSelection, {
    if (is.null(global$heatmapDoeResults)) 
    {
      js$alertMessage("You need to perform the design of experiment first.")
      return (NULL)
    }
    
    # If the matrices are not computed yet, do nothing
    sets <- names(global$heatmapDoeResults$featureSets)
    if (is.null(sets)) return (NULL)
    
    # Select file
    filePath <- biomex.chooseFile()
    if (is.null(filePath)) return (NULL)
    
    # Read file
    customSets <- biomex.readFile(filePath = filePath, sep = ",")[[1]]
    
    # If the file is not valid, send a warning and do nothing
    if (length(customSets) == 0) 
    {
      js$alertMessage("The file is not valid.")
      return (NULL)
    }
    
    # Fill the widget with the correct values
    selected <- biomex.getValidChoice(choices = sets, selected = customSets, defaultAsEmptyString = TRUE, allowSubset = TRUE)
    updateSelectizeInput(session, "heatmapSets", choices = sets, selected = selected, server = TRUE)
  })
  
  # Upload sets file online ====
  observeEvent(input$heatmapSetsFileSelectionOnline, {
    if (is.null(global$heatmapDoeResults)) 
    {
      js$alertMessage("You need to perform the design of experiment first.")
      return (NULL)
    }
    
    # If the matrices are not computed yet, do nothing
    sets <- names(global$heatmapDoeResults$featureSets)
    if (is.null(sets)) return (NULL)
    
    # Select file
    filePath <- input$heatmapSetsFileSelectionOnline$datapath
    if (is.null(filePath)) return (NULL)
    
    # Read file
    customSets <- biomex.readFile(filePath = filePath, sep = ",")[[1]]
    
    # If the file is not valid, send a warning and do nothing
    if (length(customSets) == 0) 
    {
      js$alertMessage("The file is not valid.")
      return (NULL)
    }
    
    # Fill the widget with the correct values
    selected <- biomex.getValidChoice(choices = sets, selected = customSets, defaultAsEmptyString = TRUE, allowSubset = TRUE)
    updateSelectizeInput(session, "heatmapSets", choices = sets, selected = selected, server = TRUE)
    
    # Reset the upload button
    shinyjs::reset("heatmapSetsFileSelectionOnline")
  })
  
  
  # Upload feature file offline ====
  observeEvent(input$heatmapFeaturesFileSelection, {
    if (is.null(global$heatmapDoeResults)) 
    {
      js$alertMessage("You need to perform the design of experiment first.")
      return (NULL)
    }
    
    # If the matrices are not computed yet, do nothing
    originalFeatures <- global$heatmapDoeResults$featuresSelected
    engineeredFeatures <- biomex.getFeatureNames(dataMatrix = global$feResults$dataMatrixEngineered)
    if (is.null(originalFeatures) || is.null(engineeredFeatures)) return (NULL)
    
    # Select file
    filePath <- biomex.chooseFile()
    if (is.null(filePath)) return (NULL)
    
    # Read file
    customFeatures <- biomex.readFile(filePath = filePath, sep = ",")[[1]]
    
    # If the file is not valid, send a warning and do nothing
    if (length(customFeatures) == 0) 
    {
      js$alertMessage("The file is not valid.")
      return (NULL)
    }
    
    # Get the right features
    originalFeatures <- biomex.mapFeatures(originalFeatures, from = "Original", to = "Name", mappingTable = global$mappingTable)

    # Get the correct choices
    if (input$heatmapDataMatrixAsInput %in% c("Normal", "Normal grouped")) choices <- originalFeatures
    else choices <- engineeredFeatures
    
    # Fill the widget with the correct values
    selected <- biomex.getValidChoice(choices = choices, selected = customFeatures, defaultAsEmptyString = TRUE, allowSubset = TRUE)
    updateSelectizeInput(session, "heatmapFeatures", choices = choices, selected = selected, server = TRUE)
  })
  
   # Upload feature file online ====
  observeEvent(input$heatmapFeaturesFileSelectionOnline, {
    if (is.null(global$heatmapDoeResults)) 
    {
      js$alertMessage("You need to perform the design of experiment first.")
      return (NULL)
    }
    
    # If the matrices are not computed yet, do nothing
    originalFeatures <- global$heatmapDoeResults$featuresSelected
    engineeredFeatures <- biomex.getFeatureNames(dataMatrix = global$feResults$dataMatrixEngineered)
    if (is.null(originalFeatures) || is.null(engineeredFeatures)) return (NULL)
    
    # Select file
    filePath <- input$heatmapFeaturesFileSelectionOnline$datapath
    if (is.null(filePath)) return (NULL)
    
    # Read file
    customFeatures <- biomex.readFile(filePath = filePath, sep = ",")[[1]]
    
    # If the file is not valid, send a warning and do nothing
    if (length(customFeatures) == 0) 
    {
      js$alertMessage("The file is not valid.")
      return (NULL)
    }
    
    # Get the right features
    originalFeatures <- biomex.mapFeatures(originalFeatures, from = "Original", to = "Name", mappingTable = global$mappingTable)

    # Get the correct choices
    if (input$heatmapDataMatrixAsInput %in% c("Normal", "Normal grouped")) choices <- originalFeatures
    else choices <- engineeredFeatures
    
    # Fill the widget with the correct values
    selected <- biomex.getValidChoice(choices = choices, selected = customFeatures, defaultAsEmptyString = TRUE, allowSubset = TRUE)
    updateSelectizeInput(session, "heatmapFeatures", choices = choices, selected = selected, server = TRUE)
    
    # Reset the upload button
    shinyjs::reset("heatmapFeaturesFileSelectionOnline")
  })
  
  # Upload observation file offline ====
  observeEvent(input$heatmapObservationsFileSelection, {
    if (is.null(global$heatmapDoeResults)) 
    {
      js$alertMessage("You need to perform the design of experiment first.")
      return (NULL)
    }
    
    # Get observations
    originalObservations <- global$heatmapDoeResults$observationsSelected
    groupedObservations <- biomex.getObservationNames(dataMatrix = global$heatmapDoeResults$dataMatrixNormalizedGrouped)
    
    # Get the correct observations
    if (input$heatmapDataMatrixAsInput %in% c("Normal", "Engineered")) choices <- originalObservations
    else choices <- groupedObservations
    
    # Select file
    filePath <- biomex.chooseFile()
    if (is.null(filePath)) return (NULL)
    
    # Read file
    customObservations <- biomex.readFile(filePath = filePath, sep = ",")[[1]]
    
    # If the file is not valid, send a warning and do nothing
    if (length(customObservations) == 0) 
    {
      js$alertMessage("The file is not valid.")
      return (NULL)
    }
    
    selected <- biomex.getValidChoice(choices = choices, selected = customObservations, defaultAsEmptyString = TRUE, allowSubset = TRUE)
    updateSelectizeInput(session, "heatmapObservations", choices = choices, selected = selected, server = TRUE)
  })
  
  # Upload observation file online ====
  observeEvent(input$heatmapObservationsFileSelectionOnline, {
    if (is.null(global$heatmapDoeResults)) 
    {
      js$alertMessage("You need to perform the design of experiment first.")
      return (NULL)
    }
    
    # Get observations
    originalObservations <- global$heatmapDoeResults$observationsSelected
    groupedObservations <- biomex.getObservationNames(dataMatrix = global$heatmapDoeResults$dataMatrixNormalizedGrouped)
    
    # Get the correct observations
    if (input$heatmapDataMatrixAsInput %in% c("Normal", "Engineered")) choices <- originalObservations
    else choices <- groupedObservations
    
    # Select file
    filePath <- input$heatmapObservationsFileSelectionOnline$datapath
    if (is.null(filePath)) return (NULL)
    
    # Read file
    customObservations <- biomex.readFile(filePath = filePath, sep = ",")[[1]]
    
    # If the file is not valid, send a warning and do nothing
    if (length(customObservations) == 0) 
    {
      js$alertMessage("The file is not valid.")
      return (NULL)
    }
    
    selected <- biomex.getValidChoice(choices = choices, selected = customObservations, defaultAsEmptyString = TRUE, allowSubset = TRUE)
    updateSelectizeInput(session, "heatmapObservations", choices = choices, selected = selected, server = TRUE)
    
    # Reset the upload button
    shinyjs::reset("heatmapObservationsFileSelectionOnline")
  })
  
  # *** Assign to global variables ====
  # Method settings ====
  observeEvent(input$heatmapDataMatrixAsInput, {
    global$heatmapSettings$dataMatrixAsInput <- input$heatmapDataMatrixAsInput
  })
  
  # observeEvent(input$heatmapForcePositive, {
  #   global$heatmapSettings$forcePositive <- input$heatmapForcePositive
  # })
  
  observeEvent(input$heatmapScaling, {
    global$heatmapSettings$scaling <- input$heatmapScaling
  })
  
  observeEvent(input$heatmapType, {
    global$heatmapSettings$type <- input$heatmapType
  })
  
  observeEvent(input$heatmapSets, {
    global$heatmapSettings$setsSelected <- biomex.validateSelectizeInputMultiple(value = input$heatmapSets)
  }, ignoreNULL = FALSE)
  
  observeEvent(input$heatmapFeatures, {
    global$heatmapSettings$featuresSelected <- biomex.validateSelectizeInputMultiple(value = input$heatmapFeatures)
  }, ignoreNULL = FALSE)
    
  observeEvent(input$heatmapObservations, {
    global$heatmapSettings$observationsSelected <- biomex.validateSelectizeInputMultiple(value = input$heatmapObservations)
  }, ignoreNULL = FALSE)
  
  # Clustering settings ====
  observeEvent(input$heatmapClusteringLocation, {
    global$heatmapSettings$clusteringLocation <- input$heatmapClusteringLocation
  })
   
  observeEvent(input$heatmapClusteringDistance, {
    global$heatmapSettings$distance <- input$heatmapClusteringDistance
  })
  
  observeEvent(input$heatmapClusteringAgglomeration, {
    global$heatmapSettings$agglomeration <- input$heatmapClusteringAgglomeration
  })
  
  observeEvent(input$heatmapClusteringUseSquaredDistance, {
    global$heatmapSettings$useSquaredDistance <- input$heatmapClusteringUseSquaredDistance
  })
   
  observeEvent(input$heatmapClusteringType, {
    global$heatmapSettings$clusteringType <- input$heatmapClusteringType
  })
  
  observeEvent(input$heatmapPvalue, {
    global$heatmapSettings$pvalueThreshold <- biomex.validateNumericInput(value = input$heatmapPvalue, defaultTo = globalOrigina[["heatmapSettings"]][["pvalueThreshold"]], min = 0, max = 1)
  })
  
  observeEvent(input$heatmapBootstrap, {
    global$heatmapSettings$bootstrapNumber <- input$heatmapBootstrap
  })
  
  # **** Perform heatmap ====
  observeEvent(input$updateHeatmap, {
    if (is.null(global$heatmapDoeResults)) return (NULL)
    
    # Set up progress bar
    progress <- biomex.createProgressObject(message = "Performing heatmap analysis...", style = global$progressStyle)
    on.exit(progress$close())
    
    # Call garbage collector
    biomex.callGC()
    
    # Get the subset normalized data matrix
    progress$set(message = "Performing heatmap analysis...", detail = "Preparing data", value = 1)
    dataMatrix <- biomex.obtainCorrectDataMatrix(dataMatrixNormalized = global$dataMatrixNormalized, dataMatrixEngineered = global$feResults$dataMatrixEngineered, 
      dataMatrixNormalizedGrouped = global$heatmapDoeResults$dataMatrixNormalizedGrouped, dataMatrixEngineeredGrouped = global$heatmapDoeResults$dataMatrixEngineeredGrouped,
      dataMatrixAsInput = global$heatmapSettings$dataMatrixAsInput, observationsSelected = global$heatmapDoeResults$observationsSelected,
      mappingTable = global$mappingTable, mappingType = "Name",
      performAdditionalFiltering = FALSE, featuresSelected = global$heatmapDoeResults$featuresSelected, additionalFilteringFeaturesSelected = NULL)
    
    # Subset if the user made the choice to subset the data
    if (!"" %in% global$heatmapSettings$observationsSelected) dataMatrix <- biomex.getDataMatrixSubset(dataMatrix = dataMatrix, columns = global$heatmapSettings$observationsSelected)
    
    # # Force positive values only (if needed)
    # if (global$heatmapSettings$forcePositive == TRUE)
    # {
    #   index <- dataMatrix[, -"Feature"] < 0
    #   index <- cbind(Feature = FALSE, index) # We need to add the index for the Feature, which is FALSE
    #   dataMatrix[index] <- 0
    # }
    
    # Save the original data matrix
    dataMatrixNotScaled <- copy(dataMatrix) # We need to copy, since the data will be partially modified by reference in the scaling function
    
    # Scale the data
    progress$set(message = "Performing heatmap analysis...", detail = "Scaling data", value = 1)
    dataMatrix <- biomex.scaleData(dataMatrix = dataMatrix, type = global$heatmapSettings$scaling) # The data is partially modified by reference
    
    # Perform heatmap analysis
    progress$set(message = "Performing heatmap analysis...", detail = "Creating heatmap", value = 1)
    heatmapResults <- biomex.createHeatmap(dataMatrix = dataMatrix, dataMatrixNotScaled = dataMatrixNotScaled, mappingTable = global$mappingTable, 
      featureSets = global$heatmapDoeResults$featureSets, type = global$heatmapSettings$type, sets = global$heatmapSettings$setsSelected, 
      features = global$heatmapSettings$featuresSelected) # The data is partially modified by reference
    
    # Remove unwanted variables
    biomex.removeVariables(variables = c("dataMatrix", "dataMatrixNotScaled"))
    
    # If it fails, stop and reset results
    if (!is.null(heatmapResults$errorMessage)) 
    {
      js$alertMessage(heatmapResults$errorMessage)
      global$heatmapResults <- NULL
      global$heatmapPlot <- NULL
      return (NULL)
    }
    
    # Perform clustering
    progress$set(message = "Performing heatmap analysis...", detail = "Clustering", value = 1)
    clusteringResults <- biomex.performHeatmapClustering(dataMatrix = heatmapResults$dataMatrix, clusteringLocation = global$heatmapSettings$clusteringLocation, 
                          clusteringType = global$heatmapSettings$clusteringType, distance = global$heatmapSettings$distance, agglomeration = global$heatmapSettings$agglomeration,
                          useSquaredDistance = global$heatmapSettings$useSquaredDistance,
                          pvalueThreshold = global$heatmapSettings$pvalueThreshold, bootstrapNumber = global$heatmapSettings$bootstrapNumber)
    
    # If it fails, stop and reset results
    if (!is.null(clusteringResults$errorMessage)) 
    {
      js$alertMessage(clusteringResults$errorMessage)
      global$heatmapResults <- NULL
      global$heatmapPlot <- NULL
      return (NULL)
    }
    
    # Create result list
    global$heatmapResults <- NULL
    global$heatmapResults$dataMatrix <- heatmapResults$dataMatrix
    global$heatmapResults$dataMatrixNotScaled <- heatmapResults$dataMatrixNotScaled
    global$heatmapResults$rowClustering <- clusteringResults$rowClustering
    global$heatmapResults$columnClustering <- clusteringResults$columnClustering
    global$heatmapResults$alreadyColored <- clusteringResults$alreadyColored
    
    # Remove unwanted variables
    biomex.removeVariables(variables = c("heatmapResults", "clusteringResults"))
    
    # Save settings
    global$heatmapFinalSettings <- global$heatmapSettings
    
    # Call garbage collector
    biomex.callGC()
  })
  
  # **** Plots and tables ====
  output$heatmapPlot <- renderPlotly({
    # If there are no results, don't do anything
    if (is.null(global$heatmapResults))
    {
      global$heatmapPlot <- NULL
      return (biomex.blankPlot(text = "No results available."))
    }
    
    # Get results
    dataMatrix <- global$heatmapResults$dataMatrix
    observations <- biomex.getObservationNames(dataMatrix = dataMatrix)
    rowClustering <- global$heatmapResults$rowClustering
    columnClustering <- global$heatmapResults$columnClustering
    alreadyColored <- global$heatmapResults$alreadyColored
    heatmapType <- global$heatmapFinalSettings$type
    metadataMatrix <- biomex.getDataTableSubset(global$metadataMatrix, rows = observations)
    dataMatrixAsInput <- global$heatmapFinalSettings$dataMatrixAsInput
    
    # Get settings
    colorType <- input$heatmapColors
    colorNumber <- biomex.validateNumericInput(value = input$heatmapColorNumber, defaultTo = 5, min = 2)
    reverseColors <- input$heatmapReverseColors
    gridSize <- biomex.validateNumericInput(value = input$heatmapGridSize, defaultTo = 1, min = 0)
    columnFontSize <- biomex.validateNumericInput(value = input$heatmapFontSizeColumn, defaultTo = 8, min = 0)
    rowFontSize <- biomex.validateNumericInput(value = input$heatmapFontSizeRow, defaultTo = 8, min = 0)
    rowMargin <- biomex.validateNumericInput(value = input$heatmapRowMargin, defaultTo = 100, min = 0)
    columnMargin <- biomex.validateNumericInput(value = input$heatmapColumnMargin, defaultTo = 100, min = 0)
    rowDendrogramColorNumber <- biomex.validateNumericInput(value = input$heatmapRowDendrogramColor, defaultTo = 1, min = 1)
    columnDendrogramColorNumber <- biomex.validateNumericInput(value = input$heatmapColumnDendrogramColor, defaultTo = 1, min = 1)
    dendrogramLineWidth <- biomex.validateNumericInput(value = input$heatmapWidthDendrogram, defaultTo = 1, min = 1)
    reverseDendrogram <- input$heatmapReverseDendrogram
    showColorBar <- input$heatmapShowColorBar
    columnSideColors <- input$heatmapColumnSideColors
    rendering <- input$heatmapRendering
    ladderize <- input$heatmapLadderize
    showLegend <- input$heatmapLegendShow
    sizeLegend <- input$heatmapLegendSize
    hideObservationNames <- input$heatmapHideObservationNames
    hideFeatureNames <- input$heatmapHideFeatureNames
    overrideColumnOrder <- input$heatmapOverrideColumnOrder
    
    # Subset data matrix (showing just one observation is not allowed)
    if (length(observations) == 1) 
    {
      global$heatmapPlot <- NULL
      return (biomex.blankPlot(text = "Showing just one observation is not allowed."))
    }
    
    # Column colors
    if (is.null(columnSideColors) || columnSideColors == "" || columnSideColors == "Not selected" || dataMatrixAsInput %in% c("Normal grouped", "Engineered grouped")) 
    {
      colSideColors <- NULL
    }
    else 
    {
      metadataMatrix <- biomex.getDataTableSubset(metadataMatrix, rows = observations)
      colSideColors <- data.table(Groups = metadataMatrix[[columnSideColors]])
    }
      
    # Ordering
    if (is.null(rowClustering) && is.null(columnClustering)) 
    {
      # If the override is selected, override the current ordering. Otherwise keep the ordering as in observations.
      if (overrideColumnOrder == TRUE && !is.null(colSideColors)) 
      {
        indexOrdering <- order(colSideColors)
        observations <- observations[indexOrdering]
        colSideColors <- colSideColors[indexOrdering]
        
        if (heatmapType == "Correlation") dataMatrix <- biomex.getDataTableSubset(dataMatrix, columns = c("Variable", observations), rows =  observations)
        if (heatmapType == "Features") dataMatrix <- biomex.getDataTableSubset(dataMatrix, columns = c("Variable", observations))
        metadataMatrix <- biomex.getDataTableSubset(metadataMatrix, rows = observations)
      }
    }
    
    # Colors
    colors <- biomex.getColorPalette(palette = colorType, number = colorNumber, reverse = reverseColors)
    
    # Check colors validity
    if (biomex.areColors(colors = colors) == FALSE)
    {
      global$heatmapPlot <- NULL
      return (biomex.blankPlot(text = "Invalid color detected."))
    }
    
    # Color the dendrogram
    if (alreadyColored == FALSE)
    {
      rowDendrogramColor <- rainbow
      columnDendrogramColor <- rainbow
      
      if (rowDendrogramColorNumber == 1) rowDendrogramColor <- "black"
      if (columnDendrogramColorNumber == 1) columnDendrogramColor <- "black"
      
       if (!is.null(rowClustering)) rowClustering <- color_branches(rowClustering, k = rowDendrogramColorNumber, col = rowDendrogramColor)
       if (!is.null(columnClustering)) columnClustering <- color_branches(columnClustering, k = columnDendrogramColorNumber, col = columnDendrogramColor)
    }
    
    # Dendrogram width (only with ggplot rendering)
    if (!is.null(rowClustering)) rowClustering <- assign_values_to_branches_edgePar(rowClustering, value = dendrogramLineWidth, edgePar = "lwd")
    if (!is.null(columnClustering)) columnClustering <- assign_values_to_branches_edgePar(columnClustering, value = dendrogramLineWidth, edgePar = "lwd")
    
    # Reverse dendrogram
    if (!is.null(rowClustering) && reverseDendrogram == TRUE) rowClustering <- rev(rowClustering)
    if (!is.null(columnClustering) && reverseDendrogram == TRUE) columnClustering <- rev(columnClustering)
    
    # Ladderize the clustering
    if (ladderize == TRUE)
    {
      if (!is.null(rowClustering)) rowClustering <- ladderize(rowClustering)
      if (!is.null(columnClustering)) columnClustering <- ladderize(columnClustering)
    }
    
    # Get the right labels
    labels <- c(TRUE, TRUE)
    if (hideObservationNames == TRUE) labels[1] <- FALSE
    if (hideFeatureNames == TRUE) labels[2] <- FALSE
    
    # We need to transform the data matrix to a data.frame to make it work properly with heatmaply
    variables <- dataMatrix$Variable
    dataMatrix <- dataMatrix[, -"Variable"] 
    dataMatrix <- setDF(dataMatrix, rownames = variables)
    
    # If there are too many factors, don't plot them
    if (nrow(dataMatrix) > global$plotLimit || ncol(dataMatrix) > global$plotLimit)
    {
      global$heatmapPlot <- NULL
      return (biomex.blankPlot(text = "Too many factors to create the plot."))
    }
    
    
    # Use tryCatch, since for some features the coloring can fail (e.g. when you have one feature and all the values are the same)
    p <- tryCatch({
      R.devices::suppressGraphics({
        heatmaply(dataMatrix, scale = "none", Rowv = rowClustering, Colv = columnClustering,
          colors = colors, fontsize_row = rowFontSize, fontsize_col = columnFontSize,  grid_gap = gridSize,
          plot_method = rendering, margins = c(columnMargin, rowMargin, NA, NA), hide_colorbar = !showColorBar,
          ColSideColors = colSideColors, col_side_palette = rainbow, showticklabels = labels, file = NULL)
      })
    }, error = function(err) {
      print(err)
      return (NULL)
    })
    
    if (is.null(p)) 
    {
      global$heatmapPlot <- NULL
      return (biomex.blankPlot())
    }
    
    # Legend
    p <- p %>% layout(showlegend = showLegend, legend = list(orientation = "h", xanchor = "left", yanchor = "top", y = 0, x = 0, font = list(size = sizeLegend)))
    
    # Remove the black border from the heatmap in case the rending is ggplot, to be more consistent with the plotly rendering
    if (rendering == "ggplot")
    {
      if (!is.null(p$x$layout$xaxis)) p$x$layout$xaxis$showline <- FALSE
      if (!is.null(p$x$layout$xaxis2)) p$x$layout$xaxis2$showline <- FALSE
      if (!is.null(p$x$layout$yaxis)) p$x$layout$yaxis$showline <- FALSE
      if (!is.null(p$x$layout$yaxis2)) p$x$layout$yaxis2$showline <- FALSE
    }
      
    global$heatmapPlot <- p
  })
  
  # **** Export ====
  # Plot export ====
  observeEvent(input$exportHeatmapPlot, {
    if (is.null(global$heatmapPlot))
    {
      js$alertMessage("No results available to export.")
      return (NULL)
    }
    
    global$exportPlot <- global$heatmapPlot
    
    # Open modal
    toggleModal(session, "exportPlotModal", toggle = "open")
  })
  
  # Table export ====
  observeEvent(input$exportHeatmapTable, {
    if (is.null(global$heatmapResults))
    {
      js$alertMessage("No results available to export.")
      return (NULL)
    }
    
     # Get results
    dataMatrix <- global$heatmapResults$dataMatrix
    rowClustering <- global$heatmapResults$rowClustering
    columnClustering <- global$heatmapResults$columnClustering
    
    # If the heatmap was clustered, the data must be ordered accordingly
    if (!is.null(rowClustering)) dataMatrix <- dataMatrix[order.dendrogram(rowClustering)]
    if (!is.null(columnClustering)) dataMatrix <- dataMatrix[, c(1, 1 + order.dendrogram(columnClustering)), with = FALSE]
    
    global$exportTable <- dataMatrix
    
    # Open modal
    toggleModal(session, "exportTableModal", toggle = "open")
  })
  
  # Table export (not scaled) ====
  observeEvent(input$exportHeatmapTableNotScaled, {
    if (is.null(global$heatmapResults))
    {
      js$alertMessage("No results available to export.")
      return (NULL)
    }
    
     # Get results
    dataMatrix <- global$heatmapResults$dataMatrixNotScaled
    rowClustering <- global$heatmapResults$rowClustering
    columnClustering <- global$heatmapResults$columnClustering
    
    # If the heatmap was clustered, the data must be ordered accordingly
    if (!is.null(rowClustering)) dataMatrix <- dataMatrix[order.dendrogram(rowClustering)]
    if (!is.null(columnClustering)) dataMatrix <- dataMatrix[, c(1, 1 + order.dendrogram(columnClustering)), with = FALSE]
    
    global$exportTable <- dataMatrix
    
    # Open modal
    toggleModal(session, "exportTableModal", toggle = "open")
  })
}
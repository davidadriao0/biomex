shinyjs.alertMessage = function(params)
{
	var defaultParams = {
		message : null,
	};
  
	params = shinyjs.getParams(params, defaultParams);
	
	alert(params.message);
}

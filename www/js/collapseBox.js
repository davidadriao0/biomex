shinyjs.collapseBox = function(name)
{
	// In this way it clicks only on non-collapsed boxes (taking into account the different attributes). Collapsed boxes will not be clicked.
	if ($('#' + name).closest("div[class='box']").length > 0) $('#' + name).closest("div[class='box']").find('[data-widget=collapse]').click();
	else if ($('#' + name).closest("div[class='box box-primary']").length > 0) $('#' + name).closest("div[class='box box-primary']").find('[data-widget=collapse]').click();
	else if ($('#' + name).closest("div[class='box box-warning']").length > 0) $('#' + name).closest("div[class='box box-warning']").find('[data-widget=collapse]').click();
	else if ($('#' + name).closest("div[class='box box-info']").length > 0) $('#' + name).closest("div[class='box box-info']").find('[data-widget=collapse]').click();
	else if ($('#' + name).closest("div[class='box box-success']").length > 0) $('#' + name).closest("div[class='box box-success']").find('[data-widget=collapse]').click();
	else if ($('#' + name).closest("div[class='box box-danger']").length > 0) $('#' + name).closest("div[class='box box-danger']").find('[data-widget=collapse]').click();
	
	// Old way, it click on collapsed and non-collapsed boxes alike.
	// $('#' + name).closest('.box').find('[data-widget=collapse]').click();
}
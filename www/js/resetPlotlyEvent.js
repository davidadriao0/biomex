shinyjs.resetPlotlyEvent = function(params)
{  
	var defaultParams = {
		type : null,
		source: null
	};
  
	params = shinyjs.getParams(params, defaultParams);
	
	Shiny.onInputChange('.clientValue-' + params.type + '-' + params.source, 'null')
}
shinyjs.resetWidget = function(params)
{  
	var defaultParams = {
		id : null,
		value: null
	};
  
	params = shinyjs.getParams(params, defaultParams);
	
	Shiny.onInputChange(params.id,  params.value)
}
shinyjs.uncollapseBox = function(name)
{
	// In this way it clicks only on non-collapsed boxes (taking into account the different attributes). Collapsed boxes will not be clicked.
	if ($('#' + name).closest("div[class='box collapsed-box']").length > 0) $('#' + name).closest("div[class='box collapsed-box']").find('[data-widget=collapse]').click();
	else if ($('#' + name).closest("div[class='box box-primary collapsed-box']").length > 0) $('#' + name).closest("div[class='box box-primary collapsed-box']").find('[data-widget=collapse]').click();
	else if ($('#' + name).closest("div[class='box box-warning collapsed-box']").length > 0) $('#' + name).closest("div[class='box box-warning collapsed-box']").find('[data-widget=collapse]').click();
	else if ($('#' + name).closest("div[class='box box-info collapsed-box']").length > 0) $('#' + name).closest("div[class='box box-info collapsed-box']").find('[data-widget=collapse]').click();
	else if ($('#' + name).closest("div[class='box box-success collapsed-box']").length > 0) $('#' + name).closest("div[class='box box-success collapsed-box']").find('[data-widget=collapse]').click();
	else if ($('#' + name).closest("div[class='box box-danger collapsed-box']").length > 0) $('#' + name).closest("div[class='box box-danger collapsed-box']").find('[data-widget=collapse]').click();
	
	// Old way, it click on collapsed and non-collapsed boxes alike.
	// $('#' + name).closest('.collapsed-box').find('[data-widget=collapse]').click();
}